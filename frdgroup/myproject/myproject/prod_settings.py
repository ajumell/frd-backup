#noinspection PyUnresolvedReferences
from base_settings import *

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['frdapp.xeoscript.com', 'frdgroup.co.in', 'www.frdgroup.co.in']

STATIC_ROOT = '/home/ajumell/webapps/frd_app_static'

MEDIA_ROOT = os.path.join(STATIC_ROOT, 'media')

MEDIA_URL = STATIC_URL + 'media/'

ADMINS = (
    ('Muhammed K K', 'ajumell@gmail.com'),
)
