#noinspection PyUnresolvedReferences
from base_settings import *

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['alameergroupindia.com']

STATIC_ROOT = '/home/ajumell/webfaction/al_ameer_static'

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(STATIC_ROOT, 'media')

MEDIA_URL = STATIC_URL + 'media/'

STATIC_URL = '/static/'
