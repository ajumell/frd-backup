def _add(path):
    import sys
    from os.path import abspath

    if path not in sys.path:
        sys.path.append(abspath(path))


def setup():
    from os.path import join, dirname

    cur_dir = dirname(__file__)
    libs_dir = join(dirname(cur_dir), 'libs')

    _add(join(libs_dir, 'django-helpers'))