#noinspection PyUnresolvedReferences
from base_settings import *

INSTALLED_APPS += (

)

MEDIA_ROOT = os.path.join(BASE_DIR, 'static', 'media')

MEDIA_URL = STATIC_URL + 'media/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
