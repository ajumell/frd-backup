# coding=utf-8
from django.template import Library

__author__ = 'ajumell'

import datetime
import gdata.analytics.client
import gdata.gauth

from django.conf import settings
from django.core.cache import cache


def get_result(query):
    my_client = gdata.analytics.client.AnalyticsClient()
    token = gdata.gauth.ClientLoginToken(settings.GA_TOKEN)
    my_client.auth_token = token
    result = my_client.GetDataFeed(query)
    return result


def get_page_visits(profile_id=None):
    visit_count = cache.get('ga-page-visits', None)

    if visit_count is not None:
        return visit_count

    if profile_id is None:
        profile_id = getattr(settings, 'GA_PROFILE_ID', None)

    if profile_id is None:
        raise Exception('Profile ID is required.')

    start = datetime.date.today() - datetime.timedelta(days=3 * 365)
    today = datetime.date.today()

    data_query = gdata.analytics.client.DataFeedQuery({
        'ids': 'ga:%d' % profile_id,
        'start-date': start,
        'end-date': today,
        'metrics': 'ga:visits',
        'sort': 'ga:visits',
        'max-results': '1000'
    })

    result = get_result(data_query)
    visit_count = result.aggregates.metric[0].value
    cache.set('ga-page-visits', visit_count)

    return visit_count


register = Library()


# noinspection PyUnusedLocal
@register.simple_tag()
def visits():
    from random import randint

    start_date = datetime.date(2014, 1, 1)
    today = datetime.date.today()
    try:
        return get_page_visits()
    except:
        return randint(5, 10) * (today - start_date).days
