#import "../3d-slider/js/cute.slider.js"
#import "../3d-slider/js/cute.transitions.all.js"
#import "../3d-slider/js/cute.css3d.module.js"
#import "../3d-slider/js/cute.2d.module.js"

jQuery(document).ready ->
    jQuery(".br-slidecontrol li span").css "display", "none"
    jQuery(".br-slidecontrol li:first-child span").css "display", "block"

slider = new Cute.Slider()
slider.setup "cute-slider", "bar-timer"
slider.play()