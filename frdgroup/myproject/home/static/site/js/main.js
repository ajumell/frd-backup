(function() {
    var templates = {};
    UI = this.UI || {};
    UI.Templates = templates;
}).call(this);
(function() {
    (function(jQuery) {
        jQuery.transit = {
            version: "0.9.9",
            propertyMap: {
                marginLeft: "margin",
                marginRight: "margin",
                marginBottom: "margin",
                marginTop: "margin",
                paddingLeft: "padding",
                paddingRight: "padding",
                paddingBottom: "padding",
                paddingTop: "padding"
            },
            enabled: true,
            useTransitionEnd: false
        };
        var div = document.createElement("div");
        var support = {};
        function getVendorPropertyName(prop) {
            if (prop in div.style) return prop;
            var prefixes = [ "Moz", "Webkit", "O", "ms" ];
            var prop_ = prop.charAt(0).toUpperCase() + prop.substr(1);
            if (prop in div.style) {
                return prop;
            }
            for (var i = 0; i < prefixes.length; ++i) {
                var vendorProp = prefixes[i] + prop_;
                if (vendorProp in div.style) {
                    return vendorProp;
                }
            }
        }
        function checkTransform3dSupport() {
            div.style[support.transform] = "";
            div.style[support.transform] = "rotateY(90deg)";
            return div.style[support.transform] !== "";
        }
        var isChrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
        support.transition = getVendorPropertyName("transition");
        support.transitionDelay = getVendorPropertyName("transitionDelay");
        support.transform = getVendorPropertyName("transform");
        support.transformOrigin = getVendorPropertyName("transformOrigin");
        support.transform3d = checkTransform3dSupport();
        var eventNames = {
            transition: "transitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            WebkitTransition: "webkitTransitionEnd",
            msTransition: "MSTransitionEnd"
        };
        var transitionEnd = support.transitionEnd = eventNames[support.transition] || null;
        for (var key in support) {
            if (support.hasOwnProperty(key) && typeof jQuery.support[key] === "undefined") {
                jQuery.support[key] = support[key];
            }
        }
        div = null;
        jQuery.cssEase = {
            _default: "ease",
            "in": "ease-in",
            out: "ease-out",
            "in-out": "ease-in-out",
            snap: "cubic-bezier(0,1,.5,1)",
            easeInCubic: "cubic-bezier(.55, .055, .675, .19)",
            easeOutCubic: "cubic-bezier(.215,.61,.355,1)",
            easeInOutCubic: "cubic-bezier(.645,.045,.355,1)",
            easeInCirc: "cubic-bezier(.6,.04,.98,.335)",
            easeOutCirc: "cubic-bezier(.075,.82,.165,1)",
            easeInOutCirc: "cubic-bezier(.785,.135,.15,.86)",
            easeInExpo: "cubic-bezier(.95,.05,.795,.035)",
            easeOutExpo: "cubic-bezier(.19,1,.22,1)",
            easeInOutExpo: "cubic-bezier(1,0,0,1)",
            easeInQuad: "cubic-bezier(.55,.085,.68,.53)",
            easeOutQuad: "cubic-bezier(.25,.46,.45,.94)",
            easeInOutQuad: "cubic-bezier(.455,.03,.515,.955)",
            easeInQuart: "cubic-bezier(.895,.03,.685,.22)",
            easeOutQuart: "cubic-bezier(.165,.84,.44,1)",
            easeInOutQuart: "cubic-bezier(.77,0,.175,1)",
            easeInQuint: "cubic-bezier(.755,.05,.855,.06)",
            easeOutQuint: "cubic-bezier(.23,1,.32,1)",
            easeInOutQuint: "cubic-bezier(.86,0,.07,1)",
            easeInSine: "cubic-bezier(.47,0,.745,.715)",
            easeOutSine: "cubic-bezier(.39,.575,.565,1)",
            easeInOutSine: "cubic-bezier(.445,.05,.55,.95)",
            easeInBack: "cubic-bezier(.6,-.28,.735,.045)",
            easeOutBack: "cubic-bezier(.175, .885,.32,1.275)",
            easeInOutBack: "cubic-bezier(.68,-.55,.265,1.55)"
        };
        jQuery.cssHooks["transit:transform"] = {
            get: function(elem) {
                return jQuery(elem).data("transform") || new Transform();
            },
            set: function(elem, v) {
                var value = v;
                if (!(value instanceof Transform)) {
                    value = new Transform(value);
                }
                if (support.transform === "WebkitTransform" && !isChrome) {
                    elem.style[support.transform] = value.toString(true);
                } else {
                    elem.style[support.transform] = value.toString();
                }
                jQuery(elem).data("transform", value);
            }
        };
        jQuery.cssHooks.transform = {
            set: jQuery.cssHooks["transit:transform"].set
        };
        if (jQuery.fn.jquery < "1.8") {
            jQuery.cssHooks.transformOrigin = {
                get: function(elem) {
                    return elem.style[support.transformOrigin];
                },
                set: function(elem, value) {
                    elem.style[support.transformOrigin] = value;
                }
            };
            jQuery.cssHooks.transition = {
                get: function(elem) {
                    return elem.style[support.transition];
                },
                set: function(elem, value) {
                    elem.style[support.transition] = value;
                }
            };
        }
        registerCssHook("scale");
        registerCssHook("translate");
        registerCssHook("rotate");
        registerCssHook("rotateX");
        registerCssHook("rotateY");
        registerCssHook("rotate3d");
        registerCssHook("perspective");
        registerCssHook("skewX");
        registerCssHook("skewY");
        registerCssHook("x", true);
        registerCssHook("y", true);
        function Transform(str) {
            if (typeof str === "string") {
                this.parse(str);
            }
            return this;
        }
        Transform.prototype = {
            setFromString: function(prop, val) {
                var args = typeof val === "string" ? val.split(",") : val.constructor === Array ? val : [ val ];
                args.unshift(prop);
                Transform.prototype.set.apply(this, args);
            },
            set: function(prop) {
                var args = Array.prototype.slice.apply(arguments, [ 1 ]);
                if (this.setter[prop]) {
                    this.setter[prop].apply(this, args);
                } else {
                    this[prop] = args.join(",");
                }
            },
            get: function(prop) {
                if (this.getter[prop]) {
                    return this.getter[prop].apply(this);
                } else {
                    return this[prop] || 0;
                }
            },
            setter: {
                rotate: function(theta) {
                    this.rotate = unit(theta, "deg");
                },
                rotateX: function(theta) {
                    this.rotateX = unit(theta, "deg");
                },
                rotateY: function(theta) {
                    this.rotateY = unit(theta, "deg");
                },
                scale: function(x, y) {
                    if (y === undefined) {
                        y = x;
                    }
                    this.scale = x + "," + y;
                },
                skewX: function(x) {
                    this.skewX = unit(x, "deg");
                },
                skewY: function(y) {
                    this.skewY = unit(y, "deg");
                },
                perspective: function(dist) {
                    this.perspective = unit(dist, "px");
                },
                x: function(x) {
                    this.set("translate", x, null);
                },
                y: function(y) {
                    this.set("translate", null, y);
                },
                translate: function(x, y) {
                    if (this._translateX === undefined) {
                        this._translateX = 0;
                    }
                    if (this._translateY === undefined) {
                        this._translateY = 0;
                    }
                    if (x !== null && x !== undefined) {
                        this._translateX = unit(x, "px");
                    }
                    if (y !== null && y !== undefined) {
                        this._translateY = unit(y, "px");
                    }
                    this.translate = this._translateX + "," + this._translateY;
                }
            },
            getter: {
                x: function() {
                    return this._translateX || 0;
                },
                y: function() {
                    return this._translateY || 0;
                },
                scale: function() {
                    var s = (this.scale || "1,1").split(",");
                    if (s[0]) {
                        s[0] = parseFloat(s[0]);
                    }
                    if (s[1]) {
                        s[1] = parseFloat(s[1]);
                    }
                    return s[0] === s[1] ? s[0] : s;
                },
                rotate3d: function() {
                    var s = (this.rotate3d || "0,0,0,0deg").split(",");
                    for (var i = 0; i <= 3; ++i) {
                        if (s[i]) {
                            s[i] = parseFloat(s[i]);
                        }
                    }
                    if (s[3]) {
                        s[3] = unit(s[3], "deg");
                    }
                    return s;
                }
            },
            parse: function(str) {
                var self = this;
                str.replace(/([a-zA-Z0-9]+)\((.*?)\)/g, function(x, prop, val) {
                    self.setFromString(prop, val);
                });
            },
            toString: function(use3d) {
                var re = [];
                for (var i in this) {
                    if (this.hasOwnProperty(i)) {
                        if (!support.transform3d && (i === "rotateX" || i === "rotateY" || i === "perspective" || i === "transformOrigin")) {
                            continue;
                        }
                        if (i[0] !== "_") {
                            if (use3d && i === "scale") {
                                re.push(i + "3d(" + this[i] + ",1)");
                            } else if (use3d && i === "translate") {
                                re.push(i + "3d(" + this[i] + ",0)");
                            } else {
                                re.push(i + "(" + this[i] + ")");
                            }
                        }
                    }
                }
                return re.join(" ");
            }
        };
        function callOrQueue(self, queue, fn) {
            if (queue === true) {
                self.queue(fn);
            } else if (queue) {
                self.queue(queue, fn);
            } else {
                fn();
            }
        }
        function getProperties(props) {
            var re = [];
            jQuery.each(props, function(key) {
                key = jQuery.camelCase(key);
                key = jQuery.transit.propertyMap[key] || jQuery.cssProps[key] || key;
                key = uncamel(key);
                if (jQuery.inArray(key, re) === -1) {
                    re.push(key);
                }
            });
            return re;
        }
        function getTransition(properties, duration, easing, delay) {
            var props = getProperties(properties);
            if (jQuery.cssEase[easing]) {
                easing = jQuery.cssEase[easing];
            }
            var attribs = "" + toMS(duration) + " " + easing;
            if (parseInt(delay, 10) > 0) {
                attribs += " " + toMS(delay);
            }
            var transitions = [];
            jQuery.each(props, function(i, name) {
                transitions.push(name + " " + attribs);
            });
            return transitions.join(", ");
        }
        jQuery.fn.transition = jQuery.fn.transit = function(properties, duration, easing, callback) {
            var self = this;
            var delay = 0;
            var queue = true;
            var theseProperties = jQuery.extend(true, {}, properties);
            if (typeof duration === "function") {
                callback = duration;
                duration = undefined;
            }
            if (typeof duration === "object") {
                easing = duration.easing;
                delay = duration.delay || 0;
                queue = duration.queue || true;
                callback = duration.complete;
                duration = duration.duration;
            }
            if (typeof easing === "function") {
                callback = easing;
                easing = undefined;
            }
            if (typeof theseProperties.easing !== "undefined") {
                easing = theseProperties.easing;
                delete theseProperties.easing;
            }
            if (typeof theseProperties.duration !== "undefined") {
                duration = theseProperties.duration;
                delete theseProperties.duration;
            }
            if (typeof theseProperties.complete !== "undefined") {
                callback = theseProperties.complete;
                delete theseProperties.complete;
            }
            if (typeof theseProperties.queue !== "undefined") {
                queue = theseProperties.queue;
                delete theseProperties.queue;
            }
            if (typeof theseProperties.delay !== "undefined") {
                delay = theseProperties.delay;
                delete theseProperties.delay;
            }
            if (typeof duration === "undefined") {
                duration = jQuery.fx.speeds._default;
            }
            if (typeof easing === "undefined") {
                easing = jQuery.cssEase._default;
            }
            duration = toMS(duration);
            var transitionValue = getTransition(theseProperties, duration, easing, delay);
            var work = jQuery.transit.enabled && support.transition;
            var i = work ? parseInt(duration, 10) + parseInt(delay, 10) : 0;
            if (i === 0) {
                var fn = function(next) {
                    self.css(theseProperties);
                    if (callback) {
                        callback.apply(self);
                    }
                    if (next) {
                        next();
                    }
                };
                callOrQueue(self, queue, fn);
                return self;
            }
            var oldTransitions = {};
            var run = function(nextCall) {
                var bound = false;
                var cb = function() {
                    if (bound) {
                        self.unbind(transitionEnd, cb);
                    }
                    if (i > 0) {
                        self.each(function() {
                            this.style[support.transition] = oldTransitions[this] || null;
                        });
                    }
                    if (typeof callback === "function") {
                        callback.apply(self);
                    }
                    if (typeof nextCall === "function") {
                        nextCall();
                    }
                };
                if (i > 0 && transitionEnd && jQuery.transit.useTransitionEnd) {
                    bound = true;
                    self.bind(transitionEnd, cb);
                } else {
                    window.setTimeout(cb, i);
                }
                self.each(function() {
                    if (i > 0) {
                        this.style[support.transition] = transitionValue;
                    }
                    jQuery(this).css(properties);
                });
            };
            var deferredRun = function(next) {
                this.offsetWidth;
                run(next);
            };
            callOrQueue(self, queue, deferredRun);
            return this;
        };
        function registerCssHook(prop, isPixels) {
            if (!isPixels) {
                jQuery.cssNumber[prop] = true;
            }
            jQuery.transit.propertyMap[prop] = support.transform;
            jQuery.cssHooks[prop] = {
                get: function(elem) {
                    var t = jQuery(elem).css("transit:transform");
                    return t.get(prop);
                },
                set: function(elem, value) {
                    var t = jQuery(elem).css("transit:transform");
                    t.setFromString(prop, value);
                    jQuery(elem).css({
                        "transit:transform": t
                    });
                }
            };
        }
        function uncamel(str) {
            return str.replace(/([A-Z])/g, function(letter) {
                return "-" + letter.toLowerCase();
            });
        }
        function unit(i, units) {
            if (typeof i === "string" && !i.match(/^[\-0-9\.]+$/)) {
                return i;
            } else {
                return "" + i + units;
            }
        }
        function toMS(duration) {
            var i = duration;
            if (typeof i === "string" && !i.match(/^[\-0-9\.]+/)) {
                i = jQuery.fx.speeds[i] || jQuery.fx.speeds._default;
            }
            return unit(i, "ms");
        }
        jQuery.transit.getTransitionValue = getTransition;
    })(jQuery);
    (function(e, t) {
        jQuery.easing["jswing"] = jQuery.easing["swing"];
        jQuery.extend(jQuery.easing, {
            def: "easeOutQuad",
            swing: function(e, t, n, r, i) {
                return jQuery.easing[jQuery.easing.def](e, t, n, r, i);
            },
            easeInQuad: function(e, t, n, r, i) {
                return r * (t /= i) * t + n;
            },
            easeOutQuad: function(e, t, n, r, i) {
                return -r * (t /= i) * (t - 2) + n;
            },
            easeInOutQuad: function(e, t, n, r, i) {
                if ((t /= i / 2) < 1) return r / 2 * t * t + n;
                return -r / 2 * (--t * (t - 2) - 1) + n;
            },
            easeInCubic: function(e, t, n, r, i) {
                return r * (t /= i) * t * t + n;
            },
            easeOutCubic: function(e, t, n, r, i) {
                return r * ((t = t / i - 1) * t * t + 1) + n;
            },
            easeInOutCubic: function(e, t, n, r, i) {
                if ((t /= i / 2) < 1) return r / 2 * t * t * t + n;
                return r / 2 * ((t -= 2) * t * t + 2) + n;
            },
            easeInQuart: function(e, t, n, r, i) {
                return r * (t /= i) * t * t * t + n;
            },
            easeOutQuart: function(e, t, n, r, i) {
                return -r * ((t = t / i - 1) * t * t * t - 1) + n;
            },
            easeInOutQuart: function(e, t, n, r, i) {
                if ((t /= i / 2) < 1) return r / 2 * t * t * t * t + n;
                return -r / 2 * ((t -= 2) * t * t * t - 2) + n;
            },
            easeInQuint: function(e, t, n, r, i) {
                return r * (t /= i) * t * t * t * t + n;
            },
            easeOutQuint: function(e, t, n, r, i) {
                return r * ((t = t / i - 1) * t * t * t * t + 1) + n;
            },
            easeInOutQuint: function(e, t, n, r, i) {
                if ((t /= i / 2) < 1) return r / 2 * t * t * t * t * t + n;
                return r / 2 * ((t -= 2) * t * t * t * t + 2) + n;
            },
            easeInSine: function(e, t, n, r, i) {
                return -r * Math.cos(t / i * (Math.PI / 2)) + r + n;
            },
            easeOutSine: function(e, t, n, r, i) {
                return r * Math.sin(t / i * (Math.PI / 2)) + n;
            },
            easeInOutSine: function(e, t, n, r, i) {
                return -r / 2 * (Math.cos(Math.PI * t / i) - 1) + n;
            },
            easeInExpo: function(e, t, n, r, i) {
                return t == 0 ? n : r * Math.pow(2, 10 * (t / i - 1)) + n;
            },
            easeOutExpo: function(e, t, n, r, i) {
                return t == i ? n + r : r * (-Math.pow(2, -10 * t / i) + 1) + n;
            },
            easeInOutExpo: function(e, t, n, r, i) {
                if (t == 0) return n;
                if (t == i) return n + r;
                if ((t /= i / 2) < 1) return r / 2 * Math.pow(2, 10 * (t - 1)) + n;
                return r / 2 * (-Math.pow(2, -10 * --t) + 2) + n;
            },
            easeInCirc: function(e, t, n, r, i) {
                return -r * (Math.sqrt(1 - (t /= i) * t) - 1) + n;
            },
            easeOutCirc: function(e, t, n, r, i) {
                return r * Math.sqrt(1 - (t = t / i - 1) * t) + n;
            },
            easeInOutCirc: function(e, t, n, r, i) {
                if ((t /= i / 2) < 1) return -r / 2 * (Math.sqrt(1 - t * t) - 1) + n;
                return r / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + n;
            },
            easeInElastic: function(e, t, n, r, i) {
                var s = 1.70158;
                var o = 0;
                var u = r;
                if (t == 0) return n;
                if ((t /= i) == 1) return n + r;
                if (!o) o = i * .3;
                if (u < Math.abs(r)) {
                    u = r;
                    var s = o / 4;
                } else var s = o / (2 * Math.PI) * Math.asin(r / u);
                return -(u * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * i - s) * 2 * Math.PI / o)) + n;
            },
            easeOutElastic: function(e, t, n, r, i) {
                var s = 1.70158;
                var o = 0;
                var u = r;
                if (t == 0) return n;
                if ((t /= i) == 1) return n + r;
                if (!o) o = i * .3;
                if (u < Math.abs(r)) {
                    u = r;
                    var s = o / 4;
                } else var s = o / (2 * Math.PI) * Math.asin(r / u);
                return u * Math.pow(2, -10 * t) * Math.sin((t * i - s) * 2 * Math.PI / o) + r + n;
            },
            easeInOutElastic: function(e, t, n, r, i) {
                var s = 1.70158;
                var o = 0;
                var u = r;
                if (t == 0) return n;
                if ((t /= i / 2) == 2) return n + r;
                if (!o) o = i * .3 * 1.5;
                if (u < Math.abs(r)) {
                    u = r;
                    var s = o / 4;
                } else var s = o / (2 * Math.PI) * Math.asin(r / u);
                if (t < 1) return -.5 * u * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * i - s) * 2 * Math.PI / o) + n;
                return u * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * i - s) * 2 * Math.PI / o) * .5 + r + n;
            },
            easeInBack: function(e, t, n, r, i, s) {
                if (s == undefined) s = 1.70158;
                return r * (t /= i) * t * ((s + 1) * t - s) + n;
            },
            easeOutBack: function(e, t, n, r, i, s) {
                if (s == undefined) s = 1.70158;
                return r * ((t = t / i - 1) * t * ((s + 1) * t + s) + 1) + n;
            },
            easeInOutBack: function(e, t, n, r, i, s) {
                if (s == undefined) s = 1.70158;
                if ((t /= i / 2) < 1) return r / 2 * t * t * (((s *= 1.525) + 1) * t - s) + n;
                return r / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + n;
            },
            easeInBounce: function(e, t, n, r, i) {
                return r - jQuery.easing.easeOutBounce(e, i - t, 0, r, i) + n;
            },
            easeOutBounce: function(e, t, n, r, i) {
                if ((t /= i) < 1 / 2.75) {
                    return r * 7.5625 * t * t + n;
                } else if (t < 2 / 2.75) {
                    return r * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + n;
                } else if (t < 2.5 / 2.75) {
                    return r * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + n;
                } else {
                    return r * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + n;
                }
            },
            easeInOutBounce: function(e, t, n, r, i) {
                if (t < i / 2) return jQuery.easing.easeInBounce(e, t * 2, 0, r, i) * .5 + n;
                return jQuery.easing.easeOutBounce(e, t * 2 - i, 0, r, i) * .5 + r * .5 + n;
            }
        });
        e.waitForImages = {
            hasImageProperties: [ "backgroundImage", "listStyleImage", "borderImage", "borderCornerImage" ]
        };
        e.expr[":"].uncached = function(t) {
            var n = document.createElement("img");
            n.src = t.src;
            return e(t).is('img[src!=""]') && !n.complete;
        };
        e.fn.waitForImages = function(t, n, r) {
            if (e.isPlainObject(arguments[0])) {
                n = t.each;
                r = t.waitForAll;
                t = t.finished;
            }
            t = t || e.noop;
            n = n || e.noop;
            r = !!r;
            if (!e.isFunction(t) || !e.isFunction(n)) {
                throw new TypeError("An invalid callback was supplied.");
            }
            return this.each(function() {
                var i = e(this), s = [];
                if (r) {
                    var o = e.waitForImages.hasImageProperties || [], u = /url\((['"]?)(.*?)\1\)/g;
                    i.find("*").each(function() {
                        var t = e(this);
                        if (t.is("img:uncached")) {
                            s.push({
                                src: t.attr("src"),
                                element: t[0]
                            });
                        }
                        e.each(o, function(e, n) {
                            var r = t.css(n);
                            if (!r) {
                                return true;
                            }
                            var i;
                            while (i = u.exec(r)) {
                                s.push({
                                    src: i[2],
                                    element: t[0]
                                });
                            }
                        });
                    });
                } else {
                    i.find("img:uncached").each(function() {
                        s.push({
                            src: this.src,
                            element: this
                        });
                    });
                }
                var f = s.length, l = 0;
                if (f == 0) {
                    t.call(i[0]);
                }
                e.each(s, function(r, s) {
                    var o = new Image();
                    e(o).bind("load error", function(e) {
                        l++;
                        n.call(s.element, l, f, e.type == "load");
                        if (l == f) {
                            t.call(i[0]);
                            return false;
                        }
                    });
                    o.src = s.src;
                });
            });
        };
        e.fn.swipe = function(t) {
            if (!this) return false;
            var n = {
                fingers: 1,
                threshold: 75,
                swipe: null,
                swipeLeft: null,
                swipeRight: null,
                swipeUp: null,
                swipeDown: null,
                swipeStatus: null,
                click: null,
                triggerOnTouchEnd: true,
                allowPageScroll: "auto"
            };
            var r = "left";
            var i = "right";
            var s = "up";
            var o = "down";
            var u = "none";
            var f = "horizontal";
            var l = "vertical";
            var c = "auto";
            var h = "start";
            var p = "move";
            var d = "end";
            var v = "cancel";
            var m = "ontouchstart" in window, g = m ? "touchstart" : "mousedown", y = m ? "touchmove" : "mousemove", b = m ? "touchend" : "mouseup", w = "touchcancel";
            var E = "start";
            if (t.allowPageScroll == undefined && (t.swipe != undefined || t.swipeStatus != undefined)) t.allowPageScroll = u;
            if (t) e.extend(n, t);
            return this.each(function() {
                function t() {
                    var e = S();
                    if (e <= 45 && e >= 0) return r; else if (e <= 360 && e >= 315) return r; else if (e >= 135 && e <= 225) return i; else if (e > 45 && e < 135) return o; else return s;
                }
                function S() {
                    var e = H.x - B.x;
                    var t = B.y - H.y;
                    var n = Math.atan2(t, e);
                    var r = Math.round(n * 180 / Math.PI);
                    if (r < 0) r = 360 - Math.abs(r);
                    return r;
                }
                function x() {
                    return Math.round(Math.sqrt(Math.pow(B.x - H.x, 2) + Math.pow(B.y - H.y, 2)));
                }
                function T(e, t) {
                    if (n.allowPageScroll == u) {
                        e.preventDefault();
                    } else {
                        var a = n.allowPageScroll == c;
                        switch (t) {
                          case r:
                            if (n.swipeLeft && a || !a && n.allowPageScroll != f) e.preventDefault();
                            break;

                          case i:
                            if (n.swipeRight && a || !a && n.allowPageScroll != f) e.preventDefault();
                            break;

                          case s:
                            if (n.swipeUp && a || !a && n.allowPageScroll != l) e.preventDefault();
                            break;

                          case o:
                            if (n.swipeDown && a || !a && n.allowPageScroll != l) e.preventDefault();
                            break;
                        }
                    }
                }
                function N(e, t) {
                    if (n.swipeStatus) n.swipeStatus.call(_, e, t, direction || null, distance || 0);
                    if (t == v) {
                        if (n.click && (P == 1 || !m) && (isNaN(distance) || distance == 0)) n.click.call(_, e, e.target);
                    }
                    if (t == d) {
                        if (n.swipe) {
                            n.swipe.call(_, e, direction, distance);
                        }
                        switch (direction) {
                          case r:
                            if (n.swipeLeft) n.swipeLeft.call(_, e, direction, distance);
                            break;

                          case i:
                            if (n.swipeRight) n.swipeRight.call(_, e, direction, distance);
                            break;

                          case s:
                            if (n.swipeUp) n.swipeUp.call(_, e, direction, distance);
                            break;

                          case o:
                            if (n.swipeDown) n.swipeDown.call(_, e, direction, distance);
                            break;
                        }
                    }
                }
                function C(e) {
                    P = 0;
                    H.x = 0;
                    H.y = 0;
                    B.x = 0;
                    B.y = 0;
                    F.x = 0;
                    F.y = 0;
                }
                function L(e) {
                    e.preventDefault();
                    distance = x();
                    direction = t();
                    if (n.triggerOnTouchEnd) {
                        E = d;
                        if ((P == n.fingers || !m) && B.x != 0) {
                            if (distance >= n.threshold) {
                                N(e, E);
                                C(e);
                            } else {
                                E = v;
                                N(e, E);
                                C(e);
                            }
                        } else {
                            E = v;
                            N(e, E);
                            C(e);
                        }
                    } else if (E == p) {
                        E = v;
                        N(e, E);
                        C(e);
                    }
                    M.removeEventListener(y, A, false);
                    M.removeEventListener(b, L, false);
                }
                function A(e) {
                    if (E == d || E == v) return;
                    var r = m ? e.touches[0] : e;
                    B.x = r.pageX;
                    B.y = r.pageY;
                    direction = t();
                    if (m) {
                        P = e.touches.length;
                    }
                    E = p;
                    T(e, direction);
                    if (P == n.fingers || !m) {
                        distance = x();
                        if (n.swipeStatus) N(e, E, direction, distance);
                        if (!n.triggerOnTouchEnd) {
                            if (distance >= n.threshold) {
                                E = d;
                                N(e, E);
                                C(e);
                            }
                        }
                    } else {
                        E = v;
                        N(e, E);
                        C(e);
                    }
                }
                function O(e) {
                    var t = m ? e.touches[0] : e;
                    E = h;
                    if (m) {
                        P = e.touches.length;
                    }
                    distance = 0;
                    direction = null;
                    if (P == n.fingers || !m) {
                        H.x = B.x = t.pageX;
                        H.y = B.y = t.pageY;
                        if (n.swipeStatus) N(e, E);
                    } else {
                        C(e);
                    }
                    M.addEventListener(y, A, false);
                    M.addEventListener(b, L, false);
                }
                var M = this;
                var _ = e(this);
                var D = null;
                var P = 0;
                var H = {
                    x: 0,
                    y: 0
                };
                var B = {
                    x: 0,
                    y: 0
                };
                var F = {
                    x: 0,
                    y: 0
                };
                try {
                    this.addEventListener(g, O, false);
                    this.addEventListener(w, C);
                } catch (I) {}
            });
        };
    })(jQuery);
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
    (function($) {
        $.fn.superfish = function(op) {
            var sf = $.fn.superfish, c = sf.c, $arrow = $([ '<span class="', c.arrowClass, '"> &#187;</span>' ].join("")), over = function() {
                var $$ = $(this), menu = getMenu($$);
                clearTimeout(menu.sfTimer);
                $$.showSuperfishUl().siblings().hideSuperfishUl();
            }, out = function() {
                var $$ = $(this), menu = getMenu($$), o = sf.op;
                clearTimeout(menu.sfTimer);
                menu.sfTimer = setTimeout(function() {
                    o.retainPath = $.inArray($$[0], o.$path) > -1;
                    $$.hideSuperfishUl();
                    if (o.$path.length && $$.parents([ "li.", o.hoverClass ].join("")).length < 1) {
                        over.call(o.$path);
                    }
                }, o.delay);
            }, getMenu = function($menu) {
                var menu = $menu.parents([ "ul.", c.menuClass, ":first" ].join(""))[0];
                sf.op = sf.o[menu.serial];
                return menu;
            }, addArrow = function($a) {
                $a.addClass(c.anchorClass).append($arrow.clone());
            };
            return this.each(function() {
                var s = this.serial = sf.o.length;
                var o = $.extend({}, sf.defaults, op);
                o.$path = $("li." + o.pathClass, this).slice(0, o.pathLevels).each(function() {
                    $(this).addClass([ o.hoverClass, c.bcClass ].join(" ")).filter("li:has(ul)").removeClass(o.pathClass);
                });
                sf.o[s] = sf.op = o;
                $("li:has(ul)", this)[$.fn.hoverIntent && !o.disableHI ? "hoverIntent" : "hover"](over, out).each(function() {
                    if (o.autoArrows) addArrow($(">a:first-child", this));
                }).not("." + c.bcClass).hideSuperfishUl();
                var $a = $("a", this);
                $a.each(function(i) {
                    var $li = $a.eq(i).parents("li");
                    $a.eq(i).focus(function() {
                        over.call($li);
                    }).blur(function() {
                        out.call($li);
                    });
                });
                o.onInit.call(this);
            }).each(function() {
                var menuClasses = [ c.menuClass ];
                if (sf.op.dropShadows && !($.browser.msie && $.browser.version < 7)) menuClasses.push(c.shadowClass);
                $(this).addClass(menuClasses.join(" "));
            });
        };
        var sf = $.fn.superfish;
        sf.o = [];
        sf.op = {};
        sf.IE7fix = function() {
            var o = sf.op;
            if ($.browser.msie && $.browser.version > 6 && o.dropShadows && o.animation.opacity != undefined) this.toggleClass(sf.c.shadowClass + "-off");
        };
        sf.c = {
            bcClass: "sf-breadcrumb",
            menuClass: "sf-js-enabled",
            anchorClass: "sf-with-ul",
            arrowClass: "sf-sub-indicator",
            shadowClass: "sf-shadow"
        };
        sf.defaults = {
            hoverClass: "sfHover",
            pathClass: "overideThisToUse",
            pathLevels: 1,
            delay: 800,
            animation: {
                opacity: "show"
            },
            speed: "normal",
            autoArrows: true,
            dropShadows: true,
            disableHI: false,
            onInit: function() {},
            onBeforeShow: function() {},
            onShow: function() {},
            onHide: function() {}
        };
        $.fn.extend({
            hideSuperfishUl: function() {
                var o = sf.op, not = o.retainPath === true ? o.$path : "";
                o.retainPath = false;
                var $ul = $([ "li.", o.hoverClass ].join(""), this).add(this).not(not).removeClass(o.hoverClass).find(">ul").hide().css("visibility", "hidden");
                o.onHide.call($ul);
                return this;
            },
            showSuperfishUl: function() {
                var o = sf.op, sh = sf.c.shadowClass + "-off", $ul = this.addClass(o.hoverClass).find(">ul:hidden").css("visibility", "visible");
                sf.IE7fix.call($ul);
                o.onBeforeShow.call($ul);
                $ul.animate(o.animation, o.speed, function() {
                    sf.IE7fix.call($ul);
                    o.onShow.call($ul);
                });
                return this;
            }
        });
    })(jQuery);
    (function(jQuery) {
        jQuery.fn.lavaLamp = function(o) {
            o = jQuery.extend({
                fx: "swing",
                speed: 500,
                click: function() {
                    return true;
                },
                startItem: "no",
                autoReturn: true,
                returnDelay: 0,
                setOnClick: true,
                homeTop: 0,
                homeLeft: 0,
                homeWidth: 0,
                homeHeight: 0,
                returnHome: false
            }, o || {});
            var $home;
            if (o.homeTop || o.homeLeft) {
                $home = jQuery('<li class="current"></li>').css({
                    left: o.homeLeft,
                    top: o.homeTop,
                    width: o.homeWidth,
                    height: o.homeHeight,
                    position: "absolute"
                });
                jQuery(this).prepend($home);
            }
            return this.each(function() {
                var path = location.pathname + location.search + location.hash;
                var $selected = new Object();
                var delayTimer;
                var $back;
                var ce;
                jQuery.expr[":"].parents = function(a, i, m) {
                    return jQuery(a).parents(m[3]).length < 1;
                };
                var $li = jQuery("li", this).filter(":parents(ul ul, .sub-menu)");
                if (o.startItem == "no") $selected = jQuery('li a[href$="' + path + '"]', this).parent("li");
                if ($selected.length == 0 && o.startItem == "no") $selected = jQuery('li a[href$="' + location.pathname.substring(location.pathname.lastIndexOf("/") + 1) + location.search + location.hash + '"]', this).parent("li");
                if ($selected.length == 0 || o.startItem != "no") {
                    if (o.startItem == "no") o.startItem = 0;
                    $selected = jQuery($li[o.startItem]);
                }
                ce = jQuery("li.selectedLava", this)[0] || jQuery($selected).addClass("selectedLava")[0];
                $li.mouseenter(function() {
                    if (jQuery(this).hasClass("homeLava")) {
                        ce = jQuery(this)[0];
                    }
                    move(this);
                });
                $back = jQuery('<li class="back"><div class="left"></div></li>').appendTo(this);
                jQuery(this).mouseleave(function() {
                    if (o.autoReturn) {
                        if (o.returnHome && $home) {
                            move($home[0]);
                        } else if (o.returnDelay) {
                            if (delayTimer) clearTimeout(delayTimer);
                            delayTimer = setTimeout(function() {
                                move(null);
                            }, o.returnDelay + o.speed);
                        } else {
                            move(null);
                        }
                    }
                });
                $li.click(function(e) {
                    if (o.setOnClick) {
                        jQuery(ce).removeClass("selectedLava");
                        jQuery(this).addClass("selectedLava");
                        ce = this;
                    }
                    return o.click.apply(this, [ e, this ]);
                });
                if (o.homeTop || o.homeLeft) $back.css({
                    left: o.homeLeft,
                    width: o.homeWidth,
                    height: o.homeHeight
                }); else $back.css({
                    left: ce.offsetLeft,
                    width: ce.offsetWidth,
                    height: ce.offsetHeight
                });
                function move(el) {
                    if (!el) el = ce;
                    var bx = 0, by = 0;
                    if (!jQuery.browser.msie) {
                        bx = ($back.outerWidth() - $back.innerWidth()) / 2;
                        by = ($back.outerHeight() - $back.innerHeight()) / 2;
                    }
                    $back.stop().animate({
                        left: el.offsetLeft - bx,
                        width: el.offsetWidth,
                        height: el.offsetHeight
                    }, o.speed, o.fx);
                }
            });
        };
    })(jQuery);
    jQuery.easing["jswing"] = jQuery.easing["swing"];
    jQuery.extend(jQuery.easing, {
        def: "easeOutQuad",
        swing: function(x, t, b, c, d) {
            return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
        },
        easeInQuad: function(x, t, b, c, d) {
            return c * (t /= d) * t + b;
        },
        easeOutQuad: function(x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b;
        },
        easeInOutQuad: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t + b;
            return -c / 2 * (--t * (t - 2) - 1) + b;
        },
        easeInCubic: function(x, t, b, c, d) {
            return c * (t /= d) * t * t + b;
        },
        easeOutCubic: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        },
        easeInOutCubic: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        },
        easeInQuart: function(x, t, b, c, d) {
            return c * (t /= d) * t * t * t + b;
        },
        easeOutQuart: function(x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        },
        easeInOutQuart: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
            return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
        },
        easeInQuint: function(x, t, b, c, d) {
            return c * (t /= d) * t * t * t * t + b;
        },
        easeOutQuint: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
        },
        easeInOutQuint: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
        },
        easeInSine: function(x, t, b, c, d) {
            return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
        },
        easeOutSine: function(x, t, b, c, d) {
            return c * Math.sin(t / d * (Math.PI / 2)) + b;
        },
        easeInOutSine: function(x, t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
        },
        easeInExpo: function(x, t, b, c, d) {
            return t == 0 ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
        },
        easeOutExpo: function(x, t, b, c, d) {
            return t == d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        },
        easeInOutExpo: function(x, t, b, c, d) {
            if (t == 0) return b;
            if (t == d) return b + c;
            if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
        easeInCirc: function(x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
        },
        easeOutCirc: function(x, t, b, c, d) {
            return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
        },
        easeInOutCirc: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
            return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
        },
        easeInElastic: function(x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) return b;
            if ((t /= d) == 1) return b + c;
            if (!p) p = d * .3;
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        },
        easeOutElastic: function(x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) return b;
            if ((t /= d) == 1) return b + c;
            if (!p) p = d * .3;
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
        },
        easeInOutElastic: function(x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) return b;
            if ((t /= d / 2) == 2) return b + c;
            if (!p) p = d * (.3 * 1.5);
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
            return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
        },
        easeInBack: function(x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c * (t /= d) * t * ((s + 1) * t - s) + b;
        },
        easeOutBack: function(x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        },
        easeInOutBack: function(x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
            return c / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b;
        },
        easeInBounce: function(x, t, b, c, d) {
            return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
        },
        easeOutBounce: function(x, t, b, c, d) {
            if ((t /= d) < 1 / 2.75) {
                return c * (7.5625 * t * t) + b;
            } else if (t < 2 / 2.75) {
                return c * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + b;
            } else if (t < 2.5 / 2.75) {
                return c * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + b;
            } else {
                return c * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + b;
            }
        },
        easeInOutBounce: function(x, t, b, c, d) {
            if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
            return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
        }
    });
    (function($) {
        $.fn.jflickrfeed = function(settings, callback) {
            settings = $.extend(true, {
                flickrbase: "http://api.flickr.com/services/feeds/",
                feedapi: "photos_public.gne",
                limit: 20,
                qstrings: {
                    lang: "en-us",
                    format: "json",
                    jsoncallback: "?"
                },
                cleanDescription: true,
                useTemplate: true,
                itemTemplate: "",
                itemCallback: function() {}
            }, settings);
            var url = settings.flickrbase + settings.feedapi + "?";
            var first = true;
            for (var key in settings.qstrings) {
                if (!first) url += "&";
                url += key + "=" + settings.qstrings[key];
                first = false;
            }
            return $(this).each(function() {
                var $container = $(this);
                var container = this;
                $.getJSON(url, function(data) {
                    $.each(data.items, function(i, item) {
                        if (i < settings.limit) {
                            if (settings.cleanDescription) {
                                var regex = /<p>(.*?)<\/p>/g;
                                var input = item.description;
                                if (regex.test(input)) {
                                    item.description = input.match(regex)[2];
                                    if (item.description != undefined) item.description = item.description.replace("<p>", "").replace("</p>", "");
                                }
                            }
                            item["image_s"] = item.media.m.replace("_m", "_s");
                            item["image_t"] = item.media.m.replace("_m", "_t");
                            item["image_m"] = item.media.m.replace("_m", "_m");
                            item["image"] = item.media.m.replace("_m", "");
                            item["image_b"] = item.media.m.replace("_m", "_b");
                            delete item.media;
                            if (settings.useTemplate) {
                                var template = settings.itemTemplate;
                                for (var key in item) {
                                    var rgx = new RegExp("{{" + key + "}}", "g");
                                    template = template.replace(rgx, item[key]);
                                }
                                $container.append(template);
                            }
                            settings.itemCallback.call(container, item);
                        }
                    });
                    if ($.isFunction(callback)) {
                        callback.call(container, data);
                    }
                });
            });
        };
    })(jQuery);
    (function(e) {
        e.fn.superfish = function(t) {
            var n = e.fn.superfish, r = n.c, i = e([ '<span class="', r.arrowClass, '"> &#187;</span>' ].join("")), s = function() {
                var t = e(this), n = u(t);
                clearTimeout(n.sfTimer);
                t.showSuperfishUl().siblings().hideSuperfishUl();
            }, o = function() {
                var t = e(this), r = u(t), i = n.op;
                clearTimeout(r.sfTimer);
                r.sfTimer = setTimeout(function() {
                    i.retainPath = e.inArray(t[0], i.$path) > -1;
                    t.hideSuperfishUl();
                    if (i.$path.length && t.parents([ "li.", i.hoverClass ].join("")).length < 1) {
                        s.call(i.$path);
                    }
                }, i.delay);
            }, u = function(e) {
                var t = e.parents([ "ul.", r.menuClass, ":first" ].join(""))[0];
                n.op = n.o[t.serial];
                return t;
            }, a = function(e) {
                e.addClass(r.anchorClass).append(i.clone());
            };
            return this.each(function() {
                var i = this.serial = n.o.length;
                var u = e.extend({}, n.defaults, t);
                u.$path = e("li." + u.pathClass, this).slice(0, u.pathLevels).each(function() {
                    e(this).addClass([ u.hoverClass, r.bcClass ].join(" ")).filter("li:has(ul)").removeClass(u.pathClass);
                });
                n.o[i] = n.op = u;
                e("li:has(ul)", this)[e.fn.hoverIntent && !u.disableHI ? "hoverIntent" : "hover"](s, o).each(function() {
                    if (u.autoArrows) a(e(">a:first-child", this));
                }).not("." + r.bcClass).hideSuperfishUl();
                var f = e("a", this);
                f.each(function(e) {
                    var t = f.eq(e).parents("li");
                    f.eq(e).focus(function() {
                        s.call(t);
                    }).blur(function() {
                        o.call(t);
                    });
                });
                u.onInit.call(this);
            }).each(function() {
                var t = [ r.menuClass ];
                if (n.op.dropShadows && !(e.browser.msie && e.browser.version < 7)) t.push(r.shadowClass);
                e(this).addClass(t.join(" "));
            });
        };
        var t = e.fn.superfish;
        t.o = [];
        t.op = {};
        t.IE7fix = function() {
            var n = t.op;
            if (e.browser.msie && e.browser.version > 6 && n.dropShadows && n.animation.opacity != undefined) this.toggleClass(t.c.shadowClass + "-off");
        };
        t.c = {
            bcClass: "sf-breadcrumb",
            menuClass: "sf-js-enabled",
            anchorClass: "sf-with-ul",
            arrowClass: "sf-sub-indicator",
            shadowClass: "sf-shadow"
        };
        t.defaults = {
            hoverClass: "sfHover",
            pathClass: "overideThisToUse",
            pathLevels: 1,
            delay: 800,
            animation: {
                opacity: "show"
            },
            speed: "normal",
            autoArrows: true,
            dropShadows: true,
            disableHI: false,
            onInit: function() {},
            onBeforeShow: function() {},
            onShow: function() {},
            onHide: function() {}
        };
        e.fn.extend({
            hideSuperfishUl: function() {
                var n = t.op, r = n.retainPath === true ? n.$path : "";
                n.retainPath = false;
                var i = e([ "li.", n.hoverClass ].join(""), this).add(this).not(r).removeClass(n.hoverClass).find(">ul").hide().css("visibility", "hidden");
                n.onHide.call(i);
                return this;
            },
            showSuperfishUl: function() {
                var e = t.op, n = t.c.shadowClass + "-off", r = this.addClass(e.hoverClass).find(">ul:hidden").css("visibility", "visible");
                t.IE7fix.call(r);
                e.onBeforeShow.call(r);
                r.animate(e.animation, e.speed, function() {
                    t.IE7fix.call(r);
                    e.onShow.call(r);
                });
                return this;
            }
        });
    })(jQuery);
    (function(e) {
        e.fn.lavaLamp = function(t) {
            t = e.extend({
                fx: "swing",
                speed: 500,
                click: function() {
                    return true;
                },
                startItem: "no",
                autoReturn: true,
                returnDelay: 0,
                setOnClick: true,
                homeTop: 0,
                homeLeft: 0,
                homeWidth: 0,
                homeHeight: 0,
                returnHome: false
            }, t || {});
            var n;
            if (t.homeTop || t.homeLeft) {
                n = e('<li class="current"></li>').css({
                    left: t.homeLeft,
                    top: t.homeTop,
                    width: t.homeWidth,
                    height: t.homeHeight,
                    position: "absolute"
                });
                e(this).prepend(n);
            }
            return this.each(function() {
                function l(n) {
                    if (!n) n = a;
                    var r = 0, i = 0;
                    if (!e.browser.msie) {
                        r = (u.outerWidth() - u.innerWidth()) / 2;
                        i = (u.outerHeight() - u.innerHeight()) / 2;
                    }
                    u.stop().animate({
                        left: n.offsetLeft - r,
                        width: n.offsetWidth,
                        height: n.offsetHeight
                    }, t.speed, t.fx);
                }
                var r = location.pathname + location.search + location.hash;
                var i = new Object();
                var s;
                var u;
                var a;
                e.expr[":"].parents = function(t, n, r) {
                    return e(t).parents(r[3]).length < 1;
                };
                var f = e("li", this).filter(":parents(ul ul, .sub-menu)");
                if (t.startItem == "no") i = e('li a[href$="' + r + '"]', this).parent("li");
                if (i.length == 0 && t.startItem == "no") i = e('li a[href$="' + location.pathname.substring(location.pathname.lastIndexOf("/") + 1) + location.search + location.hash + '"]', this).parent("li");
                if (i.length == 0 || t.startItem != "no") {
                    if (t.startItem == "no") t.startItem = 0;
                    i = e(f[t.startItem]);
                }
                a = e("li.selectedLava", this)[0] || e(i).addClass("selectedLava")[0];
                f.mouseenter(function() {
                    if (e(this).hasClass("homeLava")) {
                        a = e(this)[0];
                    }
                    l(this);
                });
                u = e('<li class="back"><div class="left"></div></li>').appendTo(this);
                e(this).mouseleave(function() {
                    if (t.autoReturn) {
                        if (t.returnHome && n) {
                            l(n[0]);
                        } else if (t.returnDelay) {
                            if (s) clearTimeout(s);
                            s = setTimeout(function() {
                                l(null);
                            }, t.returnDelay + t.speed);
                        } else {
                            l(null);
                        }
                    }
                });
                f.click(function(n) {
                    if (t.setOnClick) {
                        e(a).removeClass("selectedLava");
                        e(this).addClass("selectedLava");
                        a = this;
                    }
                    return t.click.apply(this, [ n, this ]);
                });
                if (t.homeTop || t.homeLeft) u.css({
                    left: t.homeLeft,
                    width: t.homeWidth,
                    height: t.homeHeight
                }); else u.css({
                    left: a.offsetLeft,
                    width: a.offsetWidth,
                    height: a.offsetHeight
                });
            });
        };
    })(jQuery);
    jQuery.easing["jswing"] = jQuery.easing["swing"];
    jQuery.extend(jQuery.easing, {
        def: "easeOutQuad",
        swing: function(e, t, n, r, i) {
            return jQuery.easing[jQuery.easing.def](e, t, n, r, i);
        },
        easeInQuad: function(e, t, n, r, i) {
            return r * (t /= i) * t + n;
        },
        easeOutQuad: function(e, t, n, r, i) {
            return -r * (t /= i) * (t - 2) + n;
        },
        easeInOutQuad: function(e, t, n, r, i) {
            if ((t /= i / 2) < 1) return r / 2 * t * t + n;
            return -r / 2 * (--t * (t - 2) - 1) + n;
        },
        easeInCubic: function(e, t, n, r, i) {
            return r * (t /= i) * t * t + n;
        },
        easeOutCubic: function(e, t, n, r, i) {
            return r * ((t = t / i - 1) * t * t + 1) + n;
        },
        easeInOutCubic: function(e, t, n, r, i) {
            if ((t /= i / 2) < 1) return r / 2 * t * t * t + n;
            return r / 2 * ((t -= 2) * t * t + 2) + n;
        },
        easeInQuart: function(e, t, n, r, i) {
            return r * (t /= i) * t * t * t + n;
        },
        easeOutQuart: function(e, t, n, r, i) {
            return -r * ((t = t / i - 1) * t * t * t - 1) + n;
        },
        easeInOutQuart: function(e, t, n, r, i) {
            if ((t /= i / 2) < 1) return r / 2 * t * t * t * t + n;
            return -r / 2 * ((t -= 2) * t * t * t - 2) + n;
        },
        easeInQuint: function(e, t, n, r, i) {
            return r * (t /= i) * t * t * t * t + n;
        },
        easeOutQuint: function(e, t, n, r, i) {
            return r * ((t = t / i - 1) * t * t * t * t + 1) + n;
        },
        easeInOutQuint: function(e, t, n, r, i) {
            if ((t /= i / 2) < 1) return r / 2 * t * t * t * t * t + n;
            return r / 2 * ((t -= 2) * t * t * t * t + 2) + n;
        },
        easeInSine: function(e, t, n, r, i) {
            return -r * Math.cos(t / i * (Math.PI / 2)) + r + n;
        },
        easeOutSine: function(e, t, n, r, i) {
            return r * Math.sin(t / i * (Math.PI / 2)) + n;
        },
        easeInOutSine: function(e, t, n, r, i) {
            return -r / 2 * (Math.cos(Math.PI * t / i) - 1) + n;
        },
        easeInExpo: function(e, t, n, r, i) {
            return t == 0 ? n : r * Math.pow(2, 10 * (t / i - 1)) + n;
        },
        easeOutExpo: function(e, t, n, r, i) {
            return t == i ? n + r : r * (-Math.pow(2, -10 * t / i) + 1) + n;
        },
        easeInOutExpo: function(e, t, n, r, i) {
            if (t == 0) return n;
            if (t == i) return n + r;
            if ((t /= i / 2) < 1) return r / 2 * Math.pow(2, 10 * (t - 1)) + n;
            return r / 2 * (-Math.pow(2, -10 * --t) + 2) + n;
        },
        easeInCirc: function(e, t, n, r, i) {
            return -r * (Math.sqrt(1 - (t /= i) * t) - 1) + n;
        },
        easeOutCirc: function(e, t, n, r, i) {
            return r * Math.sqrt(1 - (t = t / i - 1) * t) + n;
        },
        easeInOutCirc: function(e, t, n, r, i) {
            if ((t /= i / 2) < 1) return -r / 2 * (Math.sqrt(1 - t * t) - 1) + n;
            return r / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + n;
        },
        easeInElastic: function(e, t, n, r, i) {
            var s = 1.70158;
            var o = 0;
            var u = r;
            if (t == 0) return n;
            if ((t /= i) == 1) return n + r;
            if (!o) o = i * .3;
            if (u < Math.abs(r)) {
                u = r;
                var s = o / 4;
            } else var s = o / (2 * Math.PI) * Math.asin(r / u);
            return -(u * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * i - s) * 2 * Math.PI / o)) + n;
        },
        easeOutElastic: function(e, t, n, r, i) {
            var s = 1.70158;
            var o = 0;
            var u = r;
            if (t == 0) return n;
            if ((t /= i) == 1) return n + r;
            if (!o) o = i * .3;
            if (u < Math.abs(r)) {
                u = r;
                var s = o / 4;
            } else var s = o / (2 * Math.PI) * Math.asin(r / u);
            return u * Math.pow(2, -10 * t) * Math.sin((t * i - s) * 2 * Math.PI / o) + r + n;
        },
        easeInOutElastic: function(e, t, n, r, i) {
            var s = 1.70158;
            var o = 0;
            var u = r;
            if (t == 0) return n;
            if ((t /= i / 2) == 2) return n + r;
            if (!o) o = i * .3 * 1.5;
            if (u < Math.abs(r)) {
                u = r;
                var s = o / 4;
            } else var s = o / (2 * Math.PI) * Math.asin(r / u);
            if (t < 1) return -.5 * u * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * i - s) * 2 * Math.PI / o) + n;
            return u * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * i - s) * 2 * Math.PI / o) * .5 + r + n;
        },
        easeInBack: function(e, t, n, r, i, s) {
            if (s == undefined) s = 1.70158;
            return r * (t /= i) * t * ((s + 1) * t - s) + n;
        },
        easeOutBack: function(e, t, n, r, i, s) {
            if (s == undefined) s = 1.70158;
            return r * ((t = t / i - 1) * t * ((s + 1) * t + s) + 1) + n;
        },
        easeInOutBack: function(e, t, n, r, i, s) {
            if (s == undefined) s = 1.70158;
            if ((t /= i / 2) < 1) return r / 2 * t * t * (((s *= 1.525) + 1) * t - s) + n;
            return r / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + n;
        },
        easeInBounce: function(e, t, n, r, i) {
            return r - jQuery.easing.easeOutBounce(e, i - t, 0, r, i) + n;
        },
        easeOutBounce: function(e, t, n, r, i) {
            if ((t /= i) < 1 / 2.75) {
                return r * 7.5625 * t * t + n;
            } else if (t < 2 / 2.75) {
                return r * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + n;
            } else if (t < 2.5 / 2.75) {
                return r * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + n;
            } else {
                return r * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + n;
            }
        },
        easeInOutBounce: function(e, t, n, r, i) {
            if (t < i / 2) return jQuery.easing.easeInBounce(e, t * 2, 0, r, i) * .5 + n;
            return jQuery.easing.easeOutBounce(e, t * 2 - i, 0, r, i) * .5 + r * .5 + n;
        }
    });
    (function(e) {
        e.fn.jflickrfeed = function(t, n) {
            t = e.extend(true, {
                flickrbase: "http://api.flickr.com/services/feeds/",
                feedapi: "photos_public.gne",
                limit: 20,
                qstrings: {
                    lang: "en-us",
                    format: "json",
                    jsoncallback: "?"
                },
                cleanDescription: true,
                useTemplate: true,
                itemTemplate: "",
                itemCallback: function() {}
            }, t);
            var r = t.flickrbase + t.feedapi + "?";
            var i = true;
            for (var s in t.qstrings) {
                if (!i) r += "&";
                r += s + "=" + t.qstrings[s];
                i = false;
            }
            return e(this).each(function() {
                var i = e(this);
                var s = this;
                e.getJSON(r, function(r) {
                    e.each(r.items, function(e, n) {
                        if (e < t.limit) {
                            if (t.cleanDescription) {
                                var r = /<p>(.*?)<\/p>/g;
                                var o = n.description;
                                if (r.test(o)) {
                                    n.description = o.match(r)[2];
                                    if (n.description != undefined) n.description = n.description.replace("<p>", "").replace("</p>", "");
                                }
                            }
                            n["image_s"] = n.media.m.replace("_m", "_s");
                            n["image_t"] = n.media.m.replace("_m", "_t");
                            n["image_m"] = n.media.m.replace("_m", "_m");
                            n["image"] = n.media.m.replace("_m", "");
                            n["image_b"] = n.media.m.replace("_m", "_b");
                            delete n.media;
                            if (t.useTemplate) {
                                var u = t.itemTemplate;
                                for (var a in n) {
                                    var f = new RegExp("{{" + a + "}}", "g");
                                    u = u.replace(f, n[a]);
                                }
                                i.append(u);
                            }
                            t.itemCallback.call(s, n);
                        }
                    });
                    if (e.isFunction(n)) {
                        n.call(s, r);
                    }
                });
            });
        };
    })(jQuery);
    (function(e) {
        function t(t, n) {
            return parseInt(e.css(t[0], n)) || 0;
        }
        function n(e) {
            return e[0].offsetWidth + t(e, "marginLeft") + t(e, "marginRight");
        }
        function r(e) {
            return e[0].offsetHeight + t(e, "marginTop") + t(e, "marginBottom");
        }
        e.fn.jCarouselLite = function(t) {
            t = e.extend({
                btnPrev: null,
                btnNext: null,
                btnGo: null,
                mouseWheel: false,
                auto: null,
                hoverPause: false,
                speed: 200,
                easing: null,
                vertical: false,
                circular: true,
                visible: 3,
                start: 0,
                scroll: 1,
                beforeStart: null,
                afterEnd: null
            }, t || {});
            return this.each(function() {
                function w() {
                    E();
                    b = setInterval(function() {
                        x(v + t.scroll);
                    }, t.auto + t.speed);
                }
                function E() {
                    clearInterval(b);
                }
                function S() {
                    return p.slice(v).slice(0, h);
                }
                function x(n) {
                    if (!i) {
                        if (t.beforeStart) t.beforeStart.call(this, S());
                        if (t.circular) {
                            if (n < 0) {
                                f.css(s, -((v + c) * m) + "px");
                                v = n + c;
                            } else if (n > d - h) {
                                f.css(s, -((v - c) * m) + "px");
                                v = n - c;
                            } else v = n;
                        } else {
                            if (n < 0 || n > d - h) return; else v = n;
                        }
                        i = true;
                        f.animate(s == "left" ? {
                            left: -(v * m)
                        } : {
                            top: -(v * m)
                        }, t.speed, t.easing, function() {
                            if (t.afterEnd) t.afterEnd.call(this, S());
                            i = false;
                        });
                        if (!t.circular) {
                            e(t.btnPrev + "," + t.btnNext).removeClass("disabled");
                            e(v - t.scroll < 0 && t.btnPrev || v + t.scroll > d - h && t.btnNext || []).addClass("disabled");
                        }
                    }
                    return false;
                }
                var i = false, s = t.vertical ? "top" : "left", u = t.vertical ? "height" : "width";
                var a = e(this), f = e("ul", a), l = e("li", f), c = l.size(), h = t.visible;
                if (t.circular) {
                    f.prepend(l.slice(c - h + 1).clone()).append(l.slice(0, t.scroll).clone());
                    t.start += h - 1;
                }
                var p = e("li", f), d = p.size(), v = t.start;
                a.css("visibility", "visible");
                p.css({
                    overflow: "hidden",
                    "float": t.vertical ? "none" : "left"
                });
                f.css({
                    margin: "0",
                    padding: "0",
                    position: "relative",
                    "list-style-type": "none",
                    "z-index": "1"
                });
                a.css({
                    overflow: "hidden",
                    position: "relative",
                    "z-index": "2",
                    left: "0px"
                });
                var m = t.vertical ? r(p) : n(p);
                var g = m * d;
                var y = m * h;
                p.css({
                    width: p.width(),
                    height: p.height()
                });
                f.css(u, g + "px").css(s, -(v * m));
                a.css(u, y + "px");
                if (t.btnPrev) {
                    e(t.btnPrev).click(function() {
                        return x(v - t.scroll);
                    });
                    if (t.hoverPause) {
                        e(t.btnPrev).hover(function() {
                            E();
                        }, function() {
                            w();
                        });
                    }
                }
                if (t.btnNext) {
                    e(t.btnNext).click(function() {
                        return x(v + t.scroll);
                    });
                    if (t.hoverPause) {
                        e(t.btnNext).hover(function() {
                            E();
                        }, function() {
                            w();
                        });
                    }
                }
                if (t.btnGo) e.each(t.btnGo, function(n, r) {
                    e(r).click(function() {
                        return x(t.circular ? t.visible + n : n);
                    });
                });
                if (t.mouseWheel && a.mousewheel) a.mousewheel(function(e, n) {
                    return n > 0 ? x(v - t.scroll) : x(v + t.scroll);
                });
                var b;
                if (t.auto) {
                    if (t.hoverPause) {
                        a.hover(function() {
                            E();
                        }, function() {
                            w();
                        });
                    }
                    w();
                }
            });
        };
    })(jQuery);
    (function(e) {
        function t(e) {
            if (e.attr("title") || typeof e.attr("original-title") != "string") {
                e.attr("original-title", e.attr("title") || "").removeAttr("title");
            }
        }
        function n(n, r) {
            this.$element = e(n);
            this.options = r;
            this.enabled = true;
            t(this.$element);
        }
        n.prototype = {
            show: function() {
                var t = this.getTitle();
                if (t && this.enabled) {
                    var n = this.tip();
                    n.find(".tipsy-inner")[this.options.html ? "html" : "text"](t);
                    n[0].className = "tipsy";
                    n.remove().css({
                        top: 0,
                        left: 0,
                        visibility: "hidden",
                        display: "block"
                    }).appendTo(document.body);
                    var r = e.extend({}, this.$element.offset(), {
                        width: this.$element[0].offsetWidth,
                        height: this.$element[0].offsetHeight
                    });
                    var i = n[0].offsetWidth, s = n[0].offsetHeight;
                    var o = typeof this.options.gravity == "function" ? this.options.gravity.call(this.$element[0]) : this.options.gravity;
                    var u;
                    switch (o.charAt(0)) {
                      case "n":
                        u = {
                            top: r.top + r.height + this.options.offset,
                            left: r.left + r.width / 2 - i / 2
                        };
                        break;

                      case "s":
                        u = {
                            top: r.top - s - this.options.offset,
                            left: r.left + r.width / 2 - i / 2
                        };
                        break;

                      case "e":
                        u = {
                            top: r.top + r.height / 2 - s / 2,
                            left: r.left - i - this.options.offset
                        };
                        break;

                      case "w":
                        u = {
                            top: r.top + r.height / 2 - s / 2,
                            left: r.left + r.width + this.options.offset
                        };
                        break;
                    }
                    if (o.length == 2) {
                        if (o.charAt(1) == "w") {
                            u.left = r.left + r.width / 2 - 15;
                        } else {
                            u.left = r.left + r.width / 2 - i + 15;
                        }
                    }
                    n.css(u).addClass("tipsy-" + o);
                    if (this.options.fade) {
                        n.stop().css({
                            opacity: 0,
                            display: "block",
                            visibility: "visible"
                        }).animate({
                            opacity: this.options.opacity
                        });
                    } else {
                        n.css({
                            visibility: "visible",
                            opacity: this.options.opacity
                        });
                    }
                }
            },
            hide: function() {
                if (this.options.fade) {
                    this.tip().stop().fadeOut(function() {
                        e(this).remove();
                    });
                } else {
                    this.tip().remove();
                }
            },
            getTitle: function() {
                var e, n = this.$element, r = this.options;
                t(n);
                var e, r = this.options;
                if (typeof r.title == "string") {
                    e = n.attr(r.title == "title" ? "original-title" : r.title);
                } else if (typeof r.title == "function") {
                    e = r.title.call(n[0]);
                }
                e = ("" + e).replace(/(^\s*|\s*$)/, "");
                return e || r.fallback;
            },
            tip: function() {
                if (!this.$tip) {
                    this.$tip = e('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"/></div>');
                }
                return this.$tip;
            },
            validate: function() {
                if (!this.$element[0].parentNode) {
                    this.hide();
                    this.$element = null;
                    this.options = null;
                }
            },
            enable: function() {
                this.enabled = true;
            },
            disable: function() {
                this.enabled = false;
            },
            toggleEnabled: function() {
                this.enabled = !this.enabled;
            }
        };
        e.fn.tipsy = function(t) {
            function r(r) {
                var i = e.data(r, "tipsy");
                if (!i) {
                    i = new n(r, e.fn.tipsy.elementOptions(r, t));
                    e.data(r, "tipsy", i);
                }
                return i;
            }
            function i() {
                var e = r(this);
                e.hoverState = "in";
                if (t.delayIn == 0) {
                    e.show();
                } else {
                    setTimeout(function() {
                        if (e.hoverState == "in") e.show();
                    }, t.delayIn);
                }
            }
            function s() {
                var e = r(this);
                e.hoverState = "out";
                if (t.delayOut == 0) {
                    e.hide();
                } else {
                    setTimeout(function() {
                        if (e.hoverState == "out") e.hide();
                    }, t.delayOut);
                }
            }
            if (t === true) {
                return this.data("tipsy");
            } else if (typeof t == "string") {
                return this.data("tipsy")[t]();
            }
            t = e.extend({}, e.fn.tipsy.defaults, t);
            if (!t.live) this.each(function() {
                r(this);
            });
            if (t.trigger != "manual") {
                var o = t.live ? "live" : "bind", u = t.trigger == "hover" ? "mouseenter" : "focus", a = t.trigger == "hover" ? "mouseleave" : "blur";
                this[o](u, i)[o](a, s);
            }
            return this;
        };
        e.fn.tipsy.defaults = {
            delayIn: 0,
            delayOut: 0,
            fade: false,
            fallback: "",
            gravity: "n",
            html: false,
            live: false,
            offset: 0,
            opacity: .8,
            title: "title",
            trigger: "hover"
        };
        e.fn.tipsy.elementOptions = function(t, n) {
            return e.metadata ? e.extend({}, n, e(t).metadata()) : n;
        };
        e.fn.tipsy.autoNS = function() {
            return e(this).offset().top > e(document).scrollTop() + e(window).height() / 2 ? "s" : "n";
        };
        e.fn.tipsy.autoWE = function() {
            return e(this).offset().left > e(document).scrollLeft() + e(window).width() / 2 ? "e" : "w";
        };
    })(jQuery);
    (function(e, t, n, r) {
        function d(t, n) {
            this.element = t;
            this.options = e.extend({}, s, n);
            this._defaults = s;
            this._name = i;
            this.init();
        }
        var i = "stellar", s = {
            scrollProperty: "scroll",
            positionProperty: "position",
            horizontalScrolling: true,
            verticalScrolling: true,
            horizontalOffset: 0,
            verticalOffset: 0,
            responsive: true,
            parallaxBackgrounds: true,
            parallaxElements: true,
            hideDistantElements: true,
            hideElement: function(e) {
                e.hide();
            },
            showElement: function(e) {
                e.show();
            }
        }, o = {
            scroll: {
                getLeft: function(e) {
                    return e.scrollLeft();
                },
                setLeft: function(e, t) {
                    e.scrollLeft(t);
                },
                getTop: function(e) {
                    return e.scrollTop();
                },
                setTop: function(e, t) {
                    e.scrollTop(t);
                }
            },
            position: {
                getLeft: function(e) {
                    return parseInt(e.css("left"), 10) * -1;
                },
                getTop: function(e) {
                    return parseInt(e.css("top"), 10) * -1;
                }
            },
            margin: {
                getLeft: function(e) {
                    return parseInt(e.css("margin-left"), 10) * -1;
                },
                getTop: function(e) {
                    return parseInt(e.css("margin-top"), 10) * -1;
                }
            },
            transform: {
                getLeft: function(e) {
                    var t = getComputedStyle(e[0])[f];
                    return t !== "none" ? parseInt(t.match(/(-?[0-9]+)/g)[4], 10) * -1 : 0;
                },
                getTop: function(e) {
                    var t = getComputedStyle(e[0])[f];
                    return t !== "none" ? parseInt(t.match(/(-?[0-9]+)/g)[5], 10) * -1 : 0;
                }
            }
        }, u = {
            position: {
                setLeft: function(e, t) {
                    e.css("left", t);
                },
                setTop: function(e, t) {
                    e.css("top", t);
                }
            },
            transform: {
                setPosition: function(e, t, n, r, i) {
                    e[0].style[f] = "translate3d(" + (t - n) + "px, " + (r - i) + "px, 0)";
                }
            }
        }, a = function() {
            var t = /^(Moz|Webkit|Khtml|O|ms|Icab)(?=[A-Z])/, n = e("script")[0].style, r = "", i;
            for (i in n) {
                if (t.test(i)) {
                    r = i.match(t)[0];
                    break;
                }
            }
            if ("WebkitOpacity" in n) {
                r = "Webkit";
            }
            if ("KhtmlOpacity" in n) {
                r = "Khtml";
            }
            return function(e) {
                return r + (r.length > 0 ? e.charAt(0).toUpperCase() + e.slice(1) : e);
            };
        }(), f = a("transform"), l = e("<div />", {
            style: "background:#fff"
        }).css("background-position-x") !== r, c = l ? function(e, t, n) {
            e.css({
                "background-position-x": t,
                "background-position-y": n
            });
        } : function(e, t, n) {
            e.css("background-position", t + " " + n);
        }, h = l ? function(e) {
            return [ e.css("background-position-x"), e.css("background-position-y") ];
        } : function(e) {
            return e.css("background-position").split(" ");
        }, p = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || t.msRequestAnimationFrame || function(e) {
            setTimeout(e, 1e3 / 60);
        };
        d.prototype = {
            init: function() {
                this.options.name = i + "_" + Math.floor(Math.random() * 1e9);
                this._defineElements();
                this._defineGetters();
                this._defineSetters();
                this._handleWindowLoadAndResize();
                this._detectViewport();
                this.refresh({
                    firstLoad: true
                });
                if (this.options.scrollProperty === "scroll") {
                    this._handleScrollEvent();
                } else {
                    this._startAnimationLoop();
                }
            },
            _defineElements: function() {
                if (this.element === n.body) this.element = t;
                this.$scrollElement = e(this.element);
                this.$element = this.element === t ? e("body") : this.$scrollElement;
                this.$viewportElement = this.options.viewportElement !== r ? e(this.options.viewportElement) : this.$scrollElement[0] === t || this.options.scrollProperty === "scroll" ? this.$scrollElement : this.$scrollElement.parent();
            },
            _defineGetters: function() {
                var e = this, t = o[e.options.scrollProperty];
                this._getScrollLeft = function() {
                    return t.getLeft(e.$scrollElement);
                };
                this._getScrollTop = function() {
                    return t.getTop(e.$scrollElement);
                };
            },
            _defineSetters: function() {
                var t = this, n = o[t.options.scrollProperty], r = u[t.options.positionProperty], i = n.setLeft, s = n.setTop;
                this._setScrollLeft = typeof i === "function" ? function(e) {
                    i(t.$scrollElement, e);
                } : e.noop;
                this._setScrollTop = typeof s === "function" ? function(e) {
                    s(t.$scrollElement, e);
                } : e.noop;
                this._setPosition = r.setPosition || function(e, n, i, s, o) {
                    if (t.options.horizontalScrolling) {
                        r.setLeft(e, n, i);
                    }
                    if (t.options.verticalScrolling) {
                        r.setTop(e, s, o);
                    }
                };
            },
            _handleWindowLoadAndResize: function() {
                var n = this, r = e(t);
                if (n.options.responsive) {
                    r.bind("load." + this.name, function() {
                        n.refresh();
                    });
                }
                r.bind("resize." + this.name, function() {
                    n._detectViewport();
                    if (n.options.responsive) {
                        n.refresh();
                    }
                });
            },
            refresh: function(n) {
                var r = this, i = r._getScrollLeft(), s = r._getScrollTop();
                if (!n || !n.firstLoad) {
                    this._reset();
                }
                this._setScrollLeft(0);
                this._setScrollTop(0);
                this._setOffsets();
                this._findParticles();
                this._findBackgrounds();
                if (n && n.firstLoad && /WebKit/.test(navigator.userAgent)) {
                    e(t).load(function() {
                        var e = r._getScrollLeft(), t = r._getScrollTop();
                        r._setScrollLeft(e + 1);
                        r._setScrollTop(t + 1);
                        r._setScrollLeft(e);
                        r._setScrollTop(t);
                    });
                }
                this._setScrollLeft(i);
                this._setScrollTop(s);
            },
            _detectViewport: function() {
                var e = this.$viewportElement.offset(), t = e !== null && e !== r;
                this.viewportWidth = this.$viewportElement.width();
                this.viewportHeight = this.$viewportElement.height();
                this.viewportOffsetTop = t ? e.top : 0;
                this.viewportOffsetLeft = t ? e.left : 0;
            },
            _findParticles: function() {
                var t = this, n = this._getScrollLeft(), i = this._getScrollTop();
                if (this.particles !== r) {
                    for (var s = this.particles.length - 1; s >= 0; s--) {
                        this.particles[s].$element.data("stellar-elementIsActive", r);
                    }
                }
                this.particles = [];
                if (!this.options.parallaxElements) return;
                this.$element.find("[data-stellar-ratio]").each(function(n) {
                    var i = e(this), s, o, u, a, f, l, c, h, p, d = 0, v = 0, m = 0, g = 0;
                    if (!i.data("stellar-elementIsActive")) {
                        i.data("stellar-elementIsActive", this);
                    } else if (i.data("stellar-elementIsActive") !== this) {
                        return;
                    }
                    t.options.showElement(i);
                    if (!i.data("stellar-startingLeft")) {
                        i.data("stellar-startingLeft", i.css("left"));
                        i.data("stellar-startingTop", i.css("top"));
                    } else {
                        i.css("left", i.data("stellar-startingLeft"));
                        i.css("top", i.data("stellar-startingTop"));
                    }
                    u = i.position().left;
                    a = i.position().top;
                    f = i.css("margin-left") === "auto" ? 0 : parseInt(i.css("margin-left"), 10);
                    l = i.css("margin-top") === "auto" ? 0 : parseInt(i.css("margin-top"), 10);
                    h = i.offset().left - f;
                    p = i.offset().top - l;
                    i.parents().each(function() {
                        var t = e(this);
                        if (t.data("stellar-offset-parent") === true) {
                            d = m;
                            v = g;
                            c = t;
                            return false;
                        } else {
                            m += t.position().left;
                            g += t.position().top;
                        }
                    });
                    s = i.data("stellar-horizontal-offset") !== r ? i.data("stellar-horizontal-offset") : c !== r && c.data("stellar-horizontal-offset") !== r ? c.data("stellar-horizontal-offset") : t.horizontalOffset;
                    o = i.data("stellar-vertical-offset") !== r ? i.data("stellar-vertical-offset") : c !== r && c.data("stellar-vertical-offset") !== r ? c.data("stellar-vertical-offset") : t.verticalOffset;
                    t.particles.push({
                        $element: i,
                        $offsetParent: c,
                        isFixed: i.css("position") === "fixed",
                        horizontalOffset: s,
                        verticalOffset: o,
                        startingPositionLeft: u,
                        startingPositionTop: a,
                        startingOffsetLeft: h,
                        startingOffsetTop: p,
                        parentOffsetLeft: d,
                        parentOffsetTop: v,
                        stellarRatio: i.data("stellar-ratio") !== r ? i.data("stellar-ratio") : 1,
                        width: i.outerWidth(true),
                        height: i.outerHeight(true),
                        isHidden: false
                    });
                });
            },
            _findBackgrounds: function() {
                var t = this, n = this._getScrollLeft(), i = this._getScrollTop(), s;
                this.backgrounds = [];
                if (!this.options.parallaxBackgrounds) return;
                s = this.$element.find("[data-stellar-background-ratio]");
                if (this.$element.data("stellar-background-ratio")) {
                    s = s.add(this.$element);
                }
                s.each(function() {
                    var s = e(this), o = h(s), u, a, f, l, p, d, v, m, g, y = 0, b = 0, w = 0, E = 0;
                    if (!s.data("stellar-backgroundIsActive")) {
                        s.data("stellar-backgroundIsActive", this);
                    } else if (s.data("stellar-backgroundIsActive") !== this) {
                        return;
                    }
                    if (!s.data("stellar-backgroundStartingLeft")) {
                        s.data("stellar-backgroundStartingLeft", o[0]);
                        s.data("stellar-backgroundStartingTop", o[1]);
                    } else {
                        c(s, s.data("stellar-backgroundStartingLeft"), s.data("stellar-backgroundStartingTop"));
                    }
                    p = s.css("margin-left") === "auto" ? 0 : parseInt(s.css("margin-left"), 10);
                    d = s.css("margin-top") === "auto" ? 0 : parseInt(s.css("margin-top"), 10);
                    v = s.offset().left - p - n;
                    m = s.offset().top - d - i;
                    s.parents().each(function() {
                        var t = e(this);
                        if (t.data("stellar-offset-parent") === true) {
                            y = w;
                            b = E;
                            g = t;
                            return false;
                        } else {
                            w += t.position().left;
                            E += t.position().top;
                        }
                    });
                    u = s.data("stellar-horizontal-offset") !== r ? s.data("stellar-horizontal-offset") : g !== r && g.data("stellar-horizontal-offset") !== r ? g.data("stellar-horizontal-offset") : t.horizontalOffset;
                    a = s.data("stellar-vertical-offset") !== r ? s.data("stellar-vertical-offset") : g !== r && g.data("stellar-vertical-offset") !== r ? g.data("stellar-vertical-offset") : t.verticalOffset;
                    t.backgrounds.push({
                        $element: s,
                        $offsetParent: g,
                        isFixed: s.css("background-attachment") === "fixed",
                        horizontalOffset: u,
                        verticalOffset: a,
                        startingValueLeft: o[0],
                        startingValueTop: o[1],
                        startingBackgroundPositionLeft: isNaN(parseInt(o[0], 10)) ? 0 : parseInt(o[0], 10),
                        startingBackgroundPositionTop: isNaN(parseInt(o[1], 10)) ? 0 : parseInt(o[1], 10),
                        startingPositionLeft: s.position().left,
                        startingPositionTop: s.position().top,
                        startingOffsetLeft: v,
                        startingOffsetTop: m,
                        parentOffsetLeft: y,
                        parentOffsetTop: b,
                        stellarRatio: s.data("stellar-background-ratio") === r ? 1 : s.data("stellar-background-ratio")
                    });
                });
            },
            _reset: function() {
                var e, t, n, r, i;
                for (i = this.particles.length - 1; i >= 0; i--) {
                    e = this.particles[i];
                    t = e.$element.data("stellar-startingLeft");
                    n = e.$element.data("stellar-startingTop");
                    this._setPosition(e.$element, t, t, n, n);
                    this.options.showElement(e.$element);
                    e.$element.data("stellar-startingLeft", null).data("stellar-elementIsActive", null).data("stellar-backgroundIsActive", null);
                }
                for (i = this.backgrounds.length - 1; i >= 0; i--) {
                    r = this.backgrounds[i];
                    r.$element.data("stellar-backgroundStartingLeft", null).data("stellar-backgroundStartingTop", null);
                    c(r.$element, r.startingValueLeft, r.startingValueTop);
                }
            },
            destroy: function() {
                this._reset();
                this.$scrollElement.unbind("resize." + this.name).unbind("scroll." + this.name);
                this._animationLoop = e.noop;
                e(t).unbind("load." + this.name).unbind("resize." + this.name);
            },
            _setOffsets: function() {
                var n = this, r = e(t);
                r.unbind("resize.horizontal-" + this.name).unbind("resize.vertical-" + this.name);
                if (typeof this.options.horizontalOffset === "function") {
                    this.horizontalOffset = this.options.horizontalOffset();
                    r.bind("resize.horizontal-" + this.name, function() {
                        n.horizontalOffset = n.options.horizontalOffset();
                    });
                } else {
                    this.horizontalOffset = this.options.horizontalOffset;
                }
                if (typeof this.options.verticalOffset === "function") {
                    this.verticalOffset = this.options.verticalOffset();
                    r.bind("resize.vertical-" + this.name, function() {
                        n.verticalOffset = n.options.verticalOffset();
                    });
                } else {
                    this.verticalOffset = this.options.verticalOffset;
                }
            },
            _repositionElements: function() {
                var e = this._getScrollLeft(), t = this._getScrollTop(), n, r, i, s, o, u, a, f = true, l = true, h, p, d, v, m;
                if (this.currentScrollLeft === e && this.currentScrollTop === t && this.currentWidth === this.viewportWidth && this.currentHeight === this.viewportHeight) {
                    return;
                } else {
                    this.currentScrollLeft = e;
                    this.currentScrollTop = t;
                    this.currentWidth = this.viewportWidth;
                    this.currentHeight = this.viewportHeight;
                }
                for (m = this.particles.length - 1; m >= 0; m--) {
                    i = this.particles[m];
                    s = i.isFixed ? 1 : 0;
                    if (this.options.horizontalScrolling) {
                        h = (e + i.horizontalOffset + this.viewportOffsetLeft + i.startingPositionLeft - i.startingOffsetLeft + i.parentOffsetLeft) * -(i.stellarRatio + s - 1) + i.startingPositionLeft;
                        d = h - i.startingPositionLeft + i.startingOffsetLeft;
                    } else {
                        h = i.startingPositionLeft;
                        d = i.startingOffsetLeft;
                    }
                    if (this.options.verticalScrolling) {
                        p = (t + i.verticalOffset + this.viewportOffsetTop + i.startingPositionTop - i.startingOffsetTop + i.parentOffsetTop) * -(i.stellarRatio + s - 1) + i.startingPositionTop;
                        v = p - i.startingPositionTop + i.startingOffsetTop;
                    } else {
                        p = i.startingPositionTop;
                        v = i.startingOffsetTop;
                    }
                    if (this.options.hideDistantElements) {
                        l = !this.options.horizontalScrolling || d + i.width > (i.isFixed ? 0 : e) && d < (i.isFixed ? 0 : e) + this.viewportWidth + this.viewportOffsetLeft;
                        f = !this.options.verticalScrolling || v + i.height > (i.isFixed ? 0 : t) && v < (i.isFixed ? 0 : t) + this.viewportHeight + this.viewportOffsetTop;
                    }
                    if (l && f) {
                        if (i.isHidden) {
                            this.options.showElement(i.$element);
                            i.isHidden = false;
                        }
                        this._setPosition(i.$element, h, i.startingPositionLeft, p, i.startingPositionTop);
                    } else {
                        if (!i.isHidden) {
                            this.options.hideElement(i.$element);
                            i.isHidden = true;
                        }
                    }
                }
                for (m = this.backgrounds.length - 1; m >= 0; m--) {
                    o = this.backgrounds[m];
                    s = o.isFixed ? 0 : 1;
                    u = this.options.horizontalScrolling ? (e + o.horizontalOffset - this.viewportOffsetLeft - o.startingOffsetLeft + o.parentOffsetLeft - o.startingBackgroundPositionLeft) * (s - o.stellarRatio) + "px" : o.startingValueLeft;
                    a = this.options.verticalScrolling ? (t + o.verticalOffset - this.viewportOffsetTop - o.startingOffsetTop + o.parentOffsetTop - o.startingBackgroundPositionTop) * (s - o.stellarRatio) + "px" : o.startingValueTop;
                    c(o.$element, u, a);
                }
            },
            _handleScrollEvent: function() {
                var e = this, t = false;
                var n = function() {
                    e._repositionElements();
                    t = false;
                };
                var r = function() {
                    if (!t) {
                        p(n);
                        t = true;
                    }
                };
                this.$scrollElement.bind("scroll." + this.name, r);
                r();
            },
            _startAnimationLoop: function() {
                var e = this;
                this._animationLoop = function() {
                    p(e._animationLoop);
                    e._repositionElements();
                };
                this._animationLoop();
            }
        };
        e.fn[i] = function(t) {
            var n = arguments;
            if (t === r || typeof t === "object") {
                return this.each(function() {
                    if (!e.data(this, "plugin_" + i)) {
                        e.data(this, "plugin_" + i, new d(this, t));
                    }
                });
            } else if (typeof t === "string" && t[0] !== "_" && t !== "init") {
                return this.each(function() {
                    var r = e.data(this, "plugin_" + i);
                    if (r instanceof d && typeof r[t] === "function") {
                        r[t].apply(r, Array.prototype.slice.call(n, 1));
                    }
                    if (t === "destroy") {
                        e.data(this, "plugin_" + i, null);
                    }
                });
            }
        };
        e[i] = function(n) {
            var r = e(t);
            return r.stellar.apply(r, Array.prototype.slice.call(arguments, 0));
        };
        e[i].scrollProperty = o;
        e[i].positionProperty = u;
        t.Stellar = d;
    })(jQuery, this, document);
    (function(c, b) {
        var a = a || function(k) {
            var f = {
                element: null,
                dragger: null,
                disable: "none",
                addBodyClasses: true,
                hyperextensible: true,
                resistance: .5,
                flickThreshold: 50,
                transitionSpeed: .3,
                easing: "ease",
                maxPosition: 266,
                minPosition: -266,
                tapToClose: true,
                touchToDrag: true,
                slideIntent: 40,
                minDragDistance: 5
            }, e = {
                simpleStates: {
                    opening: null,
                    towards: null,
                    hyperExtending: null,
                    halfway: null,
                    flick: null,
                    translation: {
                        absolute: 0,
                        relative: 0,
                        sinceDirectionChange: 0,
                        percentage: 0
                    }
                }
            }, h = {}, d = {
                hasTouch: b.ontouchstart === null,
                eventType: function(m) {
                    var l = {
                        down: d.hasTouch ? "touchstart" : "mousedown",
                        move: d.hasTouch ? "touchmove" : "mousemove",
                        up: d.hasTouch ? "touchend" : "mouseup",
                        out: d.hasTouch ? "touchcancel" : "mouseout"
                    };
                    return l[m];
                },
                page: function(l, m) {
                    return d.hasTouch && m.touches.length && m.touches[0] ? m.touches[0]["page" + l] : m["page" + l];
                },
                klass: {
                    has: function(m, l) {
                        return m.className.indexOf(l) !== -1;
                    },
                    add: function(m, l) {
                        if (!d.klass.has(m, l) && f.addBodyClasses) {
                            m.className += " " + l;
                        }
                    },
                    remove: function(m, l) {
                        if (f.addBodyClasses) {
                            m.className = m.className.replace(l, "").replace(/^\s+|\s+$/g, "");
                        }
                    }
                },
                dispatchEvent: function(l) {
                    if (typeof h[l] === "function") {
                        return h[l].call();
                    }
                },
                vendor: function() {
                    var m = b.createElement("div"), n = "webkit Moz O ms".split(" "), l;
                    for (l in n) {
                        if (typeof m.style[n[l] + "Transition"] !== "undefined") {
                            return n[l];
                        }
                    }
                },
                transitionCallback: function() {
                    return e.vendor === "Moz" || e.vendor === "ms" ? "transitionend" : e.vendor + "TransitionEnd";
                },
                canTransform: function() {
                    return typeof f.element.style[e.vendor + "Transform"] !== "undefined";
                },
                deepExtend: function(l, n) {
                    var m;
                    for (m in n) {
                        if (n[m] && n[m].constructor && n[m].constructor === Object) {
                            l[m] = l[m] || {};
                            d.deepExtend(l[m], n[m]);
                        } else {
                            l[m] = n[m];
                        }
                    }
                    return l;
                },
                angleOfDrag: function(l, o) {
                    var n, m;
                    m = Math.atan2(-(e.startDragY - o), e.startDragX - l);
                    if (m < 0) {
                        m += 2 * Math.PI;
                    }
                    n = Math.floor(m * (180 / Math.PI) - 180);
                    if (n < 0 && n > -180) {
                        n = 360 - Math.abs(n);
                    }
                    return Math.abs(n);
                },
                events: {
                    addEvent: function g(m, l, n) {
                        if (m.addEventListener) {
                            return m.addEventListener(l, n, false);
                        } else {
                            if (m.attachEvent) {
                                return m.attachEvent("on" + l, n);
                            }
                        }
                    },
                    removeEvent: function g(m, l, n) {
                        if (m.addEventListener) {
                            return m.removeEventListener(l, n, false);
                        } else {
                            if (m.attachEvent) {
                                return m.detachEvent("on" + l, n);
                            }
                        }
                    },
                    prevent: function(l) {
                        if (l.preventDefault) {
                            l.preventDefault();
                        } else {
                            l.returnValue = false;
                        }
                    }
                },
                parentUntil: function(n, l) {
                    var m = typeof l === "string";
                    while (n.parentNode) {
                        if (m && n.getAttribute && n.getAttribute(l)) {
                            return n;
                        } else {
                            if (!m && n === l) {
                                return n;
                            }
                        }
                        n = n.parentNode;
                    }
                    return null;
                }
            }, i = {
                translate: {
                    get: {
                        matrix: function(n) {
                            if (!d.canTransform()) {
                                return parseInt(f.element.style.left, 10);
                            } else {
                                var m = c.getComputedStyle(f.element)[e.vendor + "Transform"].match(/\((.*)\)/), l = 8;
                                if (m) {
                                    m = m[1].split(",");
                                    if (m.length === 16) {
                                        n += l;
                                    }
                                    return parseInt(m[n], 10);
                                }
                                return 0;
                            }
                        }
                    },
                    easeCallback: function() {
                        f.element.style[e.vendor + "Transition"] = "";
                        e.translation = i.translate.get.matrix(4);
                        e.easing = false;
                        clearInterval(e.animatingInterval);
                        if (e.easingTo === 0) {
                            d.klass.remove(b.body, "snapjs-right");
                            d.klass.remove(b.body, "snapjs-left");
                        }
                        d.dispatchEvent("animated");
                        d.events.removeEvent(f.element, d.transitionCallback(), i.translate.easeCallback);
                    },
                    easeTo: function(l) {
                        if (!d.canTransform()) {
                            e.translation = l;
                            i.translate.x(l);
                        } else {
                            e.easing = true;
                            e.easingTo = l;
                            f.element.style[e.vendor + "Transition"] = "all " + f.transitionSpeed + "s " + f.easing;
                            e.animatingInterval = setInterval(function() {
                                d.dispatchEvent("animating");
                            }, 1);
                            d.events.addEvent(f.element, d.transitionCallback(), i.translate.easeCallback);
                            i.translate.x(l);
                        }
                        if (l === 0) {
                            f.element.style[e.vendor + "Transform"] = "";
                        }
                    },
                    x: function(m) {
                        if (f.disable === "left" && m > 0 || f.disable === "right" && m < 0) {
                            return;
                        }
                        if (!f.hyperextensible) {
                            if (m === f.maxPosition || m > f.maxPosition) {
                                m = f.maxPosition;
                            } else {
                                if (m === f.minPosition || m < f.minPosition) {
                                    m = f.minPosition;
                                }
                            }
                        }
                        m = parseInt(m, 10);
                        if (isNaN(m)) {
                            m = 0;
                        }
                        if (d.canTransform()) {
                            var l = "translate3d(" + m + "px, 0,0)";
                            f.element.style[e.vendor + "Transform"] = l;
                        } else {
                            f.element.style.width = (c.innerWidth || b.documentElement.clientWidth) + "px";
                            f.element.style.left = m + "px";
                            f.element.style.right = "";
                        }
                    }
                },
                drag: {
                    listen: function() {
                        e.translation = 0;
                        e.easing = false;
                        d.events.addEvent(f.element, d.eventType("down"), i.drag.startDrag);
                        d.events.addEvent(f.element, d.eventType("move"), i.drag.dragging);
                        d.events.addEvent(f.element, d.eventType("up"), i.drag.endDrag);
                    },
                    stopListening: function() {
                        d.events.removeEvent(f.element, d.eventType("down"), i.drag.startDrag);
                        d.events.removeEvent(f.element, d.eventType("move"), i.drag.dragging);
                        d.events.removeEvent(f.element, d.eventType("up"), i.drag.endDrag);
                    },
                    startDrag: function(n) {
                        var m = n.target ? n.target : n.srcElement, l = d.parentUntil(m, "data-snap-ignore");
                        if (l) {
                            d.dispatchEvent("ignore");
                            return;
                        }
                        if (f.dragger) {
                            var o = d.parentUntil(m, f.dragger);
                            if (!o && (e.translation !== f.minPosition && e.translation !== f.maxPosition)) {
                                return;
                            }
                        }
                        d.dispatchEvent("start");
                        f.element.style[e.vendor + "Transition"] = "";
                        e.isDragging = true;
                        e.hasIntent = null;
                        e.intentChecked = false;
                        e.startDragX = d.page("X", n);
                        e.startDragY = d.page("Y", n);
                        e.dragWatchers = {
                            current: 0,
                            last: 0,
                            hold: 0,
                            state: ""
                        };
                        e.simpleStates = {
                            opening: null,
                            towards: null,
                            hyperExtending: null,
                            halfway: null,
                            flick: null,
                            translation: {
                                absolute: 0,
                                relative: 0,
                                sinceDirectionChange: 0,
                                percentage: 0
                            }
                        };
                    },
                    dragging: function(s) {
                        if (e.isDragging && f.touchToDrag) {
                            var v = d.page("X", s), u = d.page("Y", s), t = e.translation, o = i.translate.get.matrix(4), n = v - e.startDragX, p = o > 0, q = n, w;
                            if (e.intentChecked && !e.hasIntent) {
                                return;
                            }
                            if (f.addBodyClasses) {
                                if (o > 0) {
                                    d.klass.add(b.body, "snapjs-left");
                                    d.klass.remove(b.body, "snapjs-right");
                                } else {
                                    if (o < 0) {
                                        d.klass.add(b.body, "snapjs-right");
                                        d.klass.remove(b.body, "snapjs-left");
                                    }
                                }
                            }
                            if (e.hasIntent === false || e.hasIntent === null) {
                                var m = d.angleOfDrag(v, u), l = m >= 0 && m <= f.slideIntent || m <= 360 && m > 360 - f.slideIntent, r = m >= 180 && m <= 180 + f.slideIntent || m <= 180 && m >= 180 - f.slideIntent;
                                if (!r && !l) {
                                    e.hasIntent = false;
                                } else {
                                    e.hasIntent = true;
                                }
                                e.intentChecked = true;
                            }
                            if (f.minDragDistance >= Math.abs(v - e.startDragX) || e.hasIntent === false) {
                                return;
                            }
                            d.events.prevent(s);
                            d.dispatchEvent("drag");
                            e.dragWatchers.current = v;
                            if (e.dragWatchers.last > v) {
                                if (e.dragWatchers.state !== "left") {
                                    e.dragWatchers.state = "left";
                                    e.dragWatchers.hold = v;
                                }
                                e.dragWatchers.last = v;
                            } else {
                                if (e.dragWatchers.last < v) {
                                    if (e.dragWatchers.state !== "right") {
                                        e.dragWatchers.state = "right";
                                        e.dragWatchers.hold = v;
                                    }
                                    e.dragWatchers.last = v;
                                }
                            }
                            if (p) {
                                if (f.maxPosition < o) {
                                    w = (o - f.maxPosition) * f.resistance;
                                    q = n - w;
                                }
                                e.simpleStates = {
                                    opening: "left",
                                    towards: e.dragWatchers.state,
                                    hyperExtending: f.maxPosition < o,
                                    halfway: o > f.maxPosition / 2,
                                    flick: Math.abs(e.dragWatchers.current - e.dragWatchers.hold) > f.flickThreshold,
                                    translation: {
                                        absolute: o,
                                        relative: n,
                                        sinceDirectionChange: e.dragWatchers.current - e.dragWatchers.hold,
                                        percentage: o / f.maxPosition * 100
                                    }
                                };
                            } else {
                                if (f.minPosition > o) {
                                    w = (o - f.minPosition) * f.resistance;
                                    q = n - w;
                                }
                                e.simpleStates = {
                                    opening: "right",
                                    towards: e.dragWatchers.state,
                                    hyperExtending: f.minPosition > o,
                                    halfway: o < f.minPosition / 2,
                                    flick: Math.abs(e.dragWatchers.current - e.dragWatchers.hold) > f.flickThreshold,
                                    translation: {
                                        absolute: o,
                                        relative: n,
                                        sinceDirectionChange: e.dragWatchers.current - e.dragWatchers.hold,
                                        percentage: o / f.minPosition * 100
                                    }
                                };
                            }
                            i.translate.x(q + t);
                        }
                    },
                    endDrag: function(m) {
                        if (e.isDragging) {
                            d.dispatchEvent("end");
                            var l = i.translate.get.matrix(4);
                            if (e.dragWatchers.current === 0 && l !== 0 && f.tapToClose) {
                                d.dispatchEvent("close");
                                d.events.prevent(m);
                                i.translate.easeTo(0);
                                e.isDragging = false;
                                e.startDragX = 0;
                                return;
                            }
                            if (e.simpleStates.opening === "left") {
                                if (e.simpleStates.halfway || e.simpleStates.hyperExtending || e.simpleStates.flick) {
                                    if (e.simpleStates.flick && e.simpleStates.towards === "left") {
                                        i.translate.easeTo(0);
                                    } else {
                                        if (e.simpleStates.flick && e.simpleStates.towards === "right" || (e.simpleStates.halfway || e.simpleStates.hyperExtending)) {
                                            i.translate.easeTo(f.maxPosition);
                                        }
                                    }
                                } else {
                                    i.translate.easeTo(0);
                                }
                            } else {
                                if (e.simpleStates.opening === "right") {
                                    if (e.simpleStates.halfway || e.simpleStates.hyperExtending || e.simpleStates.flick) {
                                        if (e.simpleStates.flick && e.simpleStates.towards === "right") {
                                            i.translate.easeTo(0);
                                        } else {
                                            if (e.simpleStates.flick && e.simpleStates.towards === "left" || (e.simpleStates.halfway || e.simpleStates.hyperExtending)) {
                                                i.translate.easeTo(f.minPosition);
                                            }
                                        }
                                    } else {
                                        i.translate.easeTo(0);
                                    }
                                }
                            }
                            e.isDragging = false;
                            e.startDragX = d.page("X", m);
                        }
                    }
                }
            }, j = function(l) {
                if (l.element) {
                    d.deepExtend(f, l);
                    e.vendor = d.vendor();
                    i.drag.listen();
                }
            };
            this.open = function(l) {
                d.dispatchEvent("open");
                d.klass.remove(b.body, "snapjs-expand-left");
                d.klass.remove(b.body, "snapjs-expand-right");
                if (l === "left") {
                    e.simpleStates.opening = "left";
                    e.simpleStates.towards = "right";
                    d.klass.add(b.body, "snapjs-left");
                    d.klass.remove(b.body, "snapjs-right");
                    i.translate.easeTo(f.maxPosition);
                } else {
                    if (l === "right") {
                        e.simpleStates.opening = "right";
                        e.simpleStates.towards = "left";
                        d.klass.remove(b.body, "snapjs-left");
                        d.klass.add(b.body, "snapjs-right");
                        i.translate.easeTo(f.minPosition);
                    }
                }
            };
            this.close = function() {
                d.dispatchEvent("close");
                i.translate.easeTo(0);
            };
            this.expand = function(l) {
                var m = c.innerWidth || b.documentElement.clientWidth;
                if (l === "left") {
                    d.dispatchEvent("expandLeft");
                    d.klass.add(b.body, "snapjs-expand-left");
                    d.klass.remove(b.body, "snapjs-expand-right");
                } else {
                    d.dispatchEvent("expandRight");
                    d.klass.add(b.body, "snapjs-expand-right");
                    d.klass.remove(b.body, "snapjs-expand-left");
                    m *= -1;
                }
                i.translate.easeTo(m);
            };
            this.on = function(l, m) {
                h[l] = m;
                return this;
            };
            this.off = function(l) {
                if (h[l]) {
                    h[l] = false;
                }
            };
            this.enable = function() {
                d.dispatchEvent("enable");
                i.drag.listen();
            };
            this.disable = function() {
                d.dispatchEvent("disable");
                i.drag.stopListening();
            };
            this.settings = function(l) {
                d.deepExtend(f, l);
            };
            this.state = function() {
                var l, m = i.translate.get.matrix(4);
                if (m === f.maxPosition) {
                    l = "left";
                } else {
                    if (m === f.minPosition) {
                        l = "right";
                    } else {
                        l = "closed";
                    }
                }
                return {
                    state: l,
                    info: e.simpleStates
                };
            };
            j(k);
        };
        if (typeof module !== "undefined" && module.exports) {
            module.exports = a;
        }
        if (typeof ender === "undefined") {
            this.Snap = a;
        }
        if (typeof define === "function" && define.amd) {
            define("snap", [], function() {
                return a;
            });
        }
    }).call(this, window, document);
    (function($) {
        $.prettyPhoto = {
            version: "3.1"
        };
        $.fn.prettyPhoto = function(pp_settings) {
            pp_settings = jQuery.extend({
                animation_speed: "fast",
                slideshow: 5e3,
                autoplay_slideshow: false,
                opacity: .8,
                show_title: true,
                allow_resize: true,
                default_width: 500,
                default_height: 300,
                counter_separator_label: "/",
                theme: "pp_default",
                horizontal_padding: 20,
                hideflash: false,
                wmode: "opaque",
                autoplay: true,
                modal: false,
                overlay_gallery: true,
                keyboard_shortcuts: true,
                changepicturecallback: function() {},
                callback: function() {},
                ie6_fallback: true,
                markup: '<div class="pp_pic_holder"> 						<div class="ppt">&nbsp;</div> 						<div class="pp_top"> 							<div class="pp_left"></div> 							<div class="pp_middle"></div> 							<div class="pp_right"></div> 						</div> 						<div class="pp_content_container"> 							<div class="pp_left"> 							<div class="pp_right"> 								<div class="pp_content"> 									<div class="pp_loaderIcon"></div> 									<div class="pp_fade"> 										<a href="#" class="pp_expand" title="Expand the image">Expand</a> 										<div class="pp_hoverContainer"> 											<a class="pp_next" href="#">next</a> 											<a class="pp_previous" href="#">previous</a> 										</div> 										<div id="pp_full_res"></div> 										<div class="pp_details"> 											<div class="pp_nav"> 												<a href="#" class="pp_arrow_previous">Previous</a> 												<p class="currentTextHolder">0/0</p> 												<a href="#" class="pp_arrow_next">Next</a> 											</div> 											<p class="pp_description"></p> 											<a class="pp_close" href="#">Close</a> 										</div> 									</div> 								</div> 							</div> 							</div> 						</div> 						<div class="pp_bottom"> 							<div class="pp_left"></div> 							<div class="pp_middle"></div> 							<div class="pp_right"></div> 						</div> 					</div> 					<div class="pp_overlay"></div>',
                gallery_markup: '<div class="pp_gallery"> 								<a href="#" class="pp_arrow_previous">Previous</a> 								<div> 									<ul> 										{gallery} 									</ul> 								</div> 								<a href="#" class="pp_arrow_next">Next</a> 							</div>',
                image_markup: '<img id="fullResImage" src="{path}" />',
                flash_markup: '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{width}" height="{height}"><param name="wmode" value="{wmode}" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{path}" /><embed src="{path}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="{width}" height="{height}" wmode="{wmode}"></embed></object>',
                quicktime_markup: '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="{height}" width="{width}"><param name="src" value="{path}"><param name="autoplay" value="{autoplay}"><param name="type" value="video/quicktime"><embed src="{path}" height="{height}" width="{width}" autoplay="{autoplay}" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/"></embed></object>',
                iframe_markup: '<iframe src ="{path}" width="{width}" height="{height}" frameborder="no"></iframe>',
                inline_markup: '<div class="pp_inline">{content}</div>',
                custom_markup: ""
            }, pp_settings);
            var matchedObjects = this, percentBased = false, pp_dimensions, pp_open, pp_contentHeight, pp_contentWidth, pp_containerHeight, pp_containerWidth, windowHeight = $(window).height(), windowWidth = $(window).width(), pp_slideshow;
            doresize = true, scroll_pos = _get_scroll();
            $(window).unbind("resize.prettyphoto").bind("resize.prettyphoto", function() {
                _center_overlay();
                _resize_overlay();
            });
            if (pp_settings.keyboard_shortcuts) {
                $(document).unbind("keydown.prettyphoto").bind("keydown.prettyphoto", function(e) {
                    if (typeof $pp_pic_holder != "undefined") {
                        if ($pp_pic_holder.is(":visible")) {
                            switch (e.keyCode) {
                              case 37:
                                $.prettyPhoto.changePage("previous");
                                e.preventDefault();
                                break;

                              case 39:
                                $.prettyPhoto.changePage("next");
                                e.preventDefault();
                                break;

                              case 27:
                                if (!settings.modal) $.prettyPhoto.close();
                                e.preventDefault();
                                break;
                            }
                        }
                    }
                });
            }
            $.prettyPhoto.initialize = function() {
                settings = pp_settings;
                if (settings.theme == "pp_default") settings.horizontal_padding = 16;
                if (settings.ie6_fallback && $.browser.msie && parseInt($.browser.version) == 6) settings.theme = "light_square";
                theRel = $(this).attr("data-gal");
                galleryRegExp = /\[(?:.*)\]/;
                isSet = galleryRegExp.exec(theRel) ? true : false;
                pp_images = isSet ? jQuery.map(matchedObjects, function(n, i) {
                    if ($(n).attr("data-gal").indexOf(theRel) != -1) return $(n).attr("href");
                }) : $.makeArray($(this).attr("href"));
                pp_titles = isSet ? jQuery.map(matchedObjects, function(n, i) {
                    if ($(n).attr("data-gal").indexOf(theRel) != -1) return $(n).find("img").attr("alt") ? $(n).find("img").attr("alt") : "";
                }) : $.makeArray($(this).find("img").attr("alt"));
                pp_descriptions = isSet ? jQuery.map(matchedObjects, function(n, i) {
                    if ($(n).attr("data-gal").indexOf(theRel) != -1) return $(n).attr("title") ? $(n).attr("title") : "";
                }) : $.makeArray($(this).attr("title"));
                set_position = jQuery.inArray($(this).attr("href"), pp_images);
                _build_overlay(this);
                if (settings.allow_resize) $(window).bind("scroll.prettyphoto", function() {
                    _center_overlay();
                });
                $.prettyPhoto.open();
                return false;
            };
            $.prettyPhoto.open = function(event) {
                if (typeof settings == "undefined") {
                    settings = pp_settings;
                    if ($.browser.msie && $.browser.version == 6) settings.theme = "light_square";
                    pp_images = $.makeArray(arguments[0]);
                    pp_titles = arguments[1] ? $.makeArray(arguments[1]) : $.makeArray("");
                    pp_descriptions = arguments[2] ? $.makeArray(arguments[2]) : $.makeArray("");
                    isSet = pp_images.length > 1 ? true : false;
                    set_position = 0;
                    _build_overlay(event.target);
                }
                if ($.browser.msie && $.browser.version == 6) $("select").css("visibility", "hidden");
                if (settings.hideflash) $("object,embed").css("visibility", "hidden");
                _checkPosition($(pp_images).size());
                $(".pp_loaderIcon").show();
                if ($ppt.is(":hidden")) $ppt.css("opacity", 0).show();
                $pp_overlay.show().fadeTo(settings.animation_speed, settings.opacity);
                $pp_pic_holder.find(".currentTextHolder").text(set_position + 1 + settings.counter_separator_label + $(pp_images).size());
                $pp_pic_holder.find(".pp_description").show().html(unescape(pp_descriptions[set_position]));
                movie_width = parseFloat(grab_param("width", pp_images[set_position])) ? grab_param("width", pp_images[set_position]) : settings.default_width.toString();
                movie_height = parseFloat(grab_param("height", pp_images[set_position])) ? grab_param("height", pp_images[set_position]) : settings.default_height.toString();
                if (movie_height.indexOf("%") != -1) {
                    movie_height = parseFloat($(window).height() * parseFloat(movie_height) / 100 - 150);
                    percentBased = true;
                }
                if (movie_width.indexOf("%") != -1) {
                    movie_width = parseFloat($(window).width() * parseFloat(movie_width) / 100 - 150);
                    percentBased = true;
                }
                $pp_pic_holder.fadeIn(function() {
                    settings.show_title && pp_titles[set_position] != "" && typeof pp_titles[set_position] != "undefined" ? $ppt.html(unescape(pp_titles[set_position])) : $ppt.html("&nbsp;");
                    imgPreloader = "";
                    skipInjection = false;
                    switch (_getFileType(pp_images[set_position])) {
                      case "image":
                        imgPreloader = new Image();
                        nextImage = new Image();
                        if (isSet && set_position < $(pp_images).size() - 1) nextImage.src = pp_images[set_position + 1];
                        prevImage = new Image();
                        if (isSet && pp_images[set_position - 1]) prevImage.src = pp_images[set_position - 1];
                        $pp_pic_holder.find("#pp_full_res")[0].innerHTML = settings.image_markup.replace(/{path}/g, pp_images[set_position]);
                        imgPreloader.onload = function() {
                            pp_dimensions = _fitToViewport(imgPreloader.width, imgPreloader.height);
                            _showContent();
                        };
                        imgPreloader.onerror = function() {
                            alert("Image cannot be loaded. Make sure the path is correct and image exist.");
                            $.prettyPhoto.close();
                        };
                        imgPreloader.src = pp_images[set_position];
                        break;

                      case "youtube":
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        movie = "http://www.youtube.com/embed/" + grab_param("v", pp_images[set_position]);
                        if (settings.autoplay) movie += "?autoplay=1";
                        toInject = settings.iframe_markup.replace(/{width}/g, pp_dimensions["width"]).replace(/{height}/g, pp_dimensions["height"]).replace(/{wmode}/g, settings.wmode).replace(/{path}/g, movie);
                        break;

                      case "vimeo":
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        movie_id = pp_images[set_position];
                        var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)/;
                        var match = movie_id.match(regExp);
                        movie = "http://player.vimeo.com/video/" + match[2] + "?title=0&amp;byline=0&amp;portrait=0";
                        if (settings.autoplay) movie += "&autoplay=1;";
                        vimeo_width = pp_dimensions["width"] + "/embed/?moog_width=" + pp_dimensions["width"];
                        toInject = settings.iframe_markup.replace(/{width}/g, vimeo_width).replace(/{height}/g, pp_dimensions["height"]).replace(/{path}/g, movie);
                        break;

                      case "quicktime":
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        pp_dimensions["height"] += 15;
                        pp_dimensions["contentHeight"] += 15;
                        pp_dimensions["containerHeight"] += 15;
                        toInject = settings.quicktime_markup.replace(/{width}/g, pp_dimensions["width"]).replace(/{height}/g, pp_dimensions["height"]).replace(/{wmode}/g, settings.wmode).replace(/{path}/g, pp_images[set_position]).replace(/{autoplay}/g, settings.autoplay);
                        break;

                      case "flash":
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        flash_vars = pp_images[set_position];
                        flash_vars = flash_vars.substring(pp_images[set_position].indexOf("flashvars") + 10, pp_images[set_position].length);
                        filename = pp_images[set_position];
                        filename = filename.substring(0, filename.indexOf("?"));
                        toInject = settings.flash_markup.replace(/{width}/g, pp_dimensions["width"]).replace(/{height}/g, pp_dimensions["height"]).replace(/{wmode}/g, settings.wmode).replace(/{path}/g, filename + "?" + flash_vars);
                        break;

                      case "iframe":
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        frame_url = pp_images[set_position];
                        frame_url = frame_url.substr(0, frame_url.indexOf("iframe") - 1);
                        toInject = settings.iframe_markup.replace(/{width}/g, pp_dimensions["width"]).replace(/{height}/g, pp_dimensions["height"]).replace(/{path}/g, frame_url);
                        break;

                      case "ajax":
                        doresize = false;
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        doresize = true;
                        skipInjection = true;
                        $.get(pp_images[set_position], function(responseHTML) {
                            toInject = settings.inline_markup.replace(/{content}/g, responseHTML);
                            $pp_pic_holder.find("#pp_full_res")[0].innerHTML = toInject;
                            _showContent();
                        });
                        break;

                      case "custom":
                        pp_dimensions = _fitToViewport(movie_width, movie_height);
                        toInject = settings.custom_markup;
                        break;

                      case "inline":
                        myClone = $(pp_images[set_position]).clone().append('<br clear="all" />').css({
                            width: settings.default_width
                        }).wrapInner('<div id="pp_full_res"><div class="pp_inline"></div></div>').appendTo($("body")).show();
                        doresize = false;
                        pp_dimensions = _fitToViewport($(myClone).width(), $(myClone).height());
                        doresize = true;
                        $(myClone).remove();
                        toInject = settings.inline_markup.replace(/{content}/g, $(pp_images[set_position]).html());
                        break;
                    }
                    if (!imgPreloader && !skipInjection) {
                        $pp_pic_holder.find("#pp_full_res")[0].innerHTML = toInject;
                        _showContent();
                    }
                });
                return false;
            };
            $.prettyPhoto.changePage = function(direction) {
                currentGalleryPage = 0;
                if (direction == "previous") {
                    set_position--;
                    if (set_position < 0) {
                        set_position = $(pp_images).size() - 1;
                    }
                } else if (direction == "next") {
                    set_position++;
                    if (set_position > $(pp_images).size() - 1) {
                        set_position = 0;
                    }
                } else {
                    set_position = direction;
                }
                if (!doresize) doresize = true;
                $(".pp_contract").removeClass("pp_contract").addClass("pp_expand");
                _hideContent(function() {
                    $.prettyPhoto.open();
                });
            };
            $.prettyPhoto.changeGalleryPage = function(direction) {
                if (direction == "next") {
                    currentGalleryPage++;
                    if (currentGalleryPage > totalPage) {
                        currentGalleryPage = 0;
                    }
                    slide_speed = settings.animation_speed;
                } else if (direction == "previous") {
                    currentGalleryPage--;
                    if (currentGalleryPage < 0) {
                        currentGalleryPage = totalPage;
                    }
                    slide_speed = settings.animation_speed;
                } else {
                    currentGalleryPage = direction;
                    slide_speed = 0;
                }
                slide_to = currentGalleryPage * (itemsPerPage * itemWidth);
                itemsToSlide = currentGalleryPage == totalPage ? pp_images.length - totalPage * itemsPerPage : itemsPerPage;
                $pp_gallery.find("ul").animate({
                    left: -slide_to
                }, slide_speed);
            };
            $.prettyPhoto.startSlideshow = function() {
                if (typeof pp_slideshow == "undefined") {
                    $pp_pic_holder.find(".pp_play").unbind("click").removeClass("pp_play").addClass("pp_pause").click(function() {
                        $.prettyPhoto.stopSlideshow();
                        return false;
                    });
                    pp_slideshow = setInterval($.prettyPhoto.startSlideshow, settings.slideshow);
                } else {
                    $.prettyPhoto.changePage("next");
                }
            };
            $.prettyPhoto.stopSlideshow = function() {
                $pp_pic_holder.find(".pp_pause").unbind("click").removeClass("pp_pause").addClass("pp_play").click(function() {
                    $.prettyPhoto.startSlideshow();
                    return false;
                });
                clearInterval(pp_slideshow);
                pp_slideshow = undefined;
            };
            $.prettyPhoto.close = function() {
                if ($pp_overlay.is(":animated")) return;
                $.prettyPhoto.stopSlideshow();
                $pp_pic_holder.stop().find("object,embed").css("visibility", "hidden");
                $("div.pp_pic_holder,div.ppt,.pp_fade").fadeOut(settings.animation_speed, function() {
                    $(this).remove();
                });
                $pp_overlay.fadeOut(settings.animation_speed, function() {
                    if ($.browser.msie && $.browser.version == 6) $("select").css("visibility", "visible");
                    if (settings.hideflash) $("object,embed").css("visibility", "visible");
                    $(this).remove();
                    $(window).unbind("scroll.prettyphoto");
                    settings.callback();
                    doresize = true;
                    pp_open = false;
                    delete settings;
                });
            };
            function _showContent() {
                $(".pp_loaderIcon").hide();
                $ppt.fadeTo(settings.animation_speed, 1);
                projectedTop = scroll_pos["scrollTop"] + (windowHeight / 2 - pp_dimensions["containerHeight"] / 2);
                if (projectedTop < 0) projectedTop = 0;
                $pp_pic_holder.find(".pp_content").animate({
                    height: pp_dimensions["contentHeight"],
                    width: pp_dimensions["contentWidth"]
                }, settings.animation_speed);
                $pp_pic_holder.animate({
                    top: projectedTop,
                    left: windowWidth / 2 - pp_dimensions["containerWidth"] / 2,
                    width: pp_dimensions["containerWidth"]
                }, settings.animation_speed, function() {
                    $pp_pic_holder.find(".pp_hoverContainer,#fullResImage").height(pp_dimensions["height"]).width(pp_dimensions["width"]);
                    $pp_pic_holder.find(".pp_fade").fadeIn(settings.animation_speed);
                    if (isSet && _getFileType(pp_images[set_position]) == "image") {
                        $pp_pic_holder.find(".pp_hoverContainer").show();
                    } else {
                        $pp_pic_holder.find(".pp_hoverContainer").hide();
                    }
                    if (pp_dimensions["resized"]) {
                        $("a.pp_expand,a.pp_contract").show();
                    } else {
                        $("a.pp_expand").hide();
                    }
                    if (settings.autoplay_slideshow && !pp_slideshow && !pp_open) $.prettyPhoto.startSlideshow();
                    settings.changepicturecallback();
                    pp_open = true;
                });
                _insert_gallery();
            }
            function _hideContent(callback) {
                $pp_pic_holder.find("#pp_full_res object,#pp_full_res embed").css("visibility", "hidden");
                $pp_pic_holder.find(".pp_fade").fadeOut(settings.animation_speed, function() {
                    $(".pp_loaderIcon").show();
                    callback();
                });
            }
            function _checkPosition(setCount) {
                setCount > 1 ? $(".pp_nav").show() : $(".pp_nav").hide();
            }
            function _fitToViewport(width, height) {
                resized = false;
                _getDimensions(width, height);
                imageWidth = width, imageHeight = height;
                if ((pp_containerWidth > windowWidth || pp_containerHeight > windowHeight) && doresize && settings.allow_resize && !percentBased) {
                    resized = true, fitting = false;
                    while (!fitting) {
                        if (pp_containerWidth > windowWidth) {
                            imageWidth = windowWidth - 200;
                            imageHeight = height / width * imageWidth;
                        } else if (pp_containerHeight > windowHeight) {
                            imageHeight = windowHeight - 200;
                            imageWidth = width / height * imageHeight;
                        } else {
                            fitting = true;
                        }
                        pp_containerHeight = imageHeight, pp_containerWidth = imageWidth;
                    }
                    _getDimensions(imageWidth, imageHeight);
                    if (pp_containerWidth > windowWidth || pp_containerHeight > windowHeight) {
                        _fitToViewport(pp_containerWidth, pp_containerHeight);
                    }
                }
                return {
                    width: Math.floor(imageWidth),
                    height: Math.floor(imageHeight),
                    containerHeight: Math.floor(pp_containerHeight),
                    containerWidth: Math.floor(pp_containerWidth) + settings.horizontal_padding * 2,
                    contentHeight: Math.floor(pp_contentHeight),
                    contentWidth: Math.floor(pp_contentWidth),
                    resized: resized
                };
            }
            function _getDimensions(width, height) {
                width = parseFloat(width);
                height = parseFloat(height);
                $pp_details = $pp_pic_holder.find(".pp_details");
                $pp_details.width(width);
                detailsHeight = parseFloat($pp_details.css("marginTop")) + parseFloat($pp_details.css("marginBottom"));
                $pp_details = $pp_details.clone().addClass(settings.theme).width(width).appendTo($("body")).css({
                    position: "absolute",
                    top: -1e4
                });
                detailsHeight += $pp_details.height();
                detailsHeight = detailsHeight <= 34 ? 36 : detailsHeight;
                if ($.browser.msie && $.browser.version == 7) detailsHeight += 8;
                $pp_details.remove();
                $pp_title = $pp_pic_holder.find(".ppt");
                $pp_title.width(width);
                titleHeight = parseFloat($pp_title.css("marginTop")) + parseFloat($pp_title.css("marginBottom"));
                $pp_title = $pp_title.clone().appendTo($("body")).css({
                    position: "absolute",
                    top: -1e4
                });
                titleHeight += $pp_title.height();
                $pp_title.remove();
                pp_contentHeight = height + detailsHeight;
                pp_contentWidth = width;
                pp_containerHeight = pp_contentHeight + titleHeight + $pp_pic_holder.find(".pp_top").height() + $pp_pic_holder.find(".pp_bottom").height();
                pp_containerWidth = width;
            }
            function _getFileType(itemSrc) {
                if (itemSrc.match(/youtube\.com\/watch/i)) {
                    return "youtube";
                } else if (itemSrc.match(/vimeo\.com/i)) {
                    return "vimeo";
                } else if (itemSrc.match(/\b.mov\b/i)) {
                    return "quicktime";
                } else if (itemSrc.match(/\b.swf\b/i)) {
                    return "flash";
                } else if (itemSrc.match(/\biframe=true\b/i)) {
                    return "iframe";
                } else if (itemSrc.match(/\bajax=true\b/i)) {
                    return "ajax";
                } else if (itemSrc.match(/\bcustom=true\b/i)) {
                    return "custom";
                } else if (itemSrc.substr(0, 1) == "#") {
                    return "inline";
                } else {
                    return "image";
                }
            }
            function _center_overlay() {
                if (doresize && typeof $pp_pic_holder != "undefined") {
                    scroll_pos = _get_scroll();
                    contentHeight = $pp_pic_holder.height(), contentwidth = $pp_pic_holder.width();
                    projectedTop = windowHeight / 2 + scroll_pos["scrollTop"] - contentHeight / 2;
                    if (projectedTop < 0) projectedTop = 0;
                    if (contentHeight > windowHeight) return;
                    $pp_pic_holder.css({
                        top: projectedTop,
                        left: windowWidth / 2 + scroll_pos["scrollLeft"] - contentwidth / 2
                    });
                }
            }
            function _get_scroll() {
                if (self.pageYOffset) {
                    return {
                        scrollTop: self.pageYOffset,
                        scrollLeft: self.pageXOffset
                    };
                } else if (document.documentElement && document.documentElement.scrollTop) {
                    return {
                        scrollTop: document.documentElement.scrollTop,
                        scrollLeft: document.documentElement.scrollLeft
                    };
                } else if (document.body) {
                    return {
                        scrollTop: document.body.scrollTop,
                        scrollLeft: document.body.scrollLeft
                    };
                }
            }
            function _resize_overlay() {
                windowHeight = $(window).height(), windowWidth = $(window).width();
                if (typeof $pp_overlay != "undefined") $pp_overlay.height($(document).height()).width(windowWidth);
            }
            function _insert_gallery() {
                if (isSet && settings.overlay_gallery && _getFileType(pp_images[set_position]) == "image" && (settings.ie6_fallback && !($.browser.msie && parseInt($.browser.version) == 6))) {
                    itemWidth = 52 + 5;
                    navWidth = settings.theme == "facebook" || settings.theme == "pp_default" ? 50 : 30;
                    itemsPerPage = Math.floor((pp_dimensions["containerWidth"] - 100 - navWidth) / itemWidth);
                    itemsPerPage = itemsPerPage < pp_images.length ? itemsPerPage : pp_images.length;
                    totalPage = Math.ceil(pp_images.length / itemsPerPage) - 1;
                    if (totalPage == 0) {
                        navWidth = 0;
                        $pp_gallery.find(".pp_arrow_next,.pp_arrow_previous").hide();
                    } else {
                        $pp_gallery.find(".pp_arrow_next,.pp_arrow_previous").show();
                    }
                    galleryWidth = itemsPerPage * itemWidth;
                    fullGalleryWidth = pp_images.length * itemWidth;
                    $pp_gallery.css("margin-left", -(galleryWidth / 2 + navWidth / 2)).find("div:first").width(galleryWidth + 5).find("ul").width(fullGalleryWidth).find("li.selected").removeClass("selected");
                    goToPage = Math.floor(set_position / itemsPerPage) < totalPage ? Math.floor(set_position / itemsPerPage) : totalPage;
                    $.prettyPhoto.changeGalleryPage(goToPage);
                    $pp_gallery_li.filter(":eq(" + set_position + ")").addClass("selected");
                } else {
                    $pp_pic_holder.find(".pp_content").unbind("mouseenter mouseleave");
                }
            }
            function _build_overlay(caller) {
                $("body").append(settings.markup);
                $pp_pic_holder = $(".pp_pic_holder"), $ppt = $(".ppt"), $pp_overlay = $("div.pp_overlay");
                if (isSet && settings.overlay_gallery) {
                    currentGalleryPage = 0;
                    toInject = "";
                    for (var i = 0; i < pp_images.length; i++) {
                        if (!pp_images[i].match(/\b(jpg|jpeg|png|gif)\b/gi)) {
                            classname = "default";
                            img_src = "";
                        } else {
                            classname = "";
                            img_src = pp_images[i];
                        }
                        toInject += "<li class='" + classname + "'><a href='#'><img src='" + img_src + "' width='50' alt='' /></a></li>";
                    }
                    toInject = settings.gallery_markup.replace(/{gallery}/g, toInject);
                    $pp_pic_holder.find("#pp_full_res").after(toInject);
                    $pp_gallery = $(".pp_pic_holder .pp_gallery"), $pp_gallery_li = $pp_gallery.find("li");
                    $pp_gallery.find(".pp_arrow_next").click(function() {
                        $.prettyPhoto.changeGalleryPage("next");
                        $.prettyPhoto.stopSlideshow();
                        return false;
                    });
                    $pp_gallery.find(".pp_arrow_previous").click(function() {
                        $.prettyPhoto.changeGalleryPage("previous");
                        $.prettyPhoto.stopSlideshow();
                        return false;
                    });
                    $pp_pic_holder.find(".pp_content").hover(function() {
                        $pp_pic_holder.find(".pp_gallery:not(.disabled)").fadeIn();
                    }, function() {
                        $pp_pic_holder.find(".pp_gallery:not(.disabled)").fadeOut();
                    });
                    itemWidth = 52 + 5;
                    $pp_gallery_li.each(function(i) {
                        $(this).find("a").click(function() {
                            $.prettyPhoto.changePage(i);
                            $.prettyPhoto.stopSlideshow();
                            return false;
                        });
                    });
                }
                if (settings.slideshow) {
                    $pp_pic_holder.find(".pp_nav").prepend('<a href="#" class="pp_play">Play</a>');
                    $pp_pic_holder.find(".pp_nav .pp_play").click(function() {
                        $.prettyPhoto.startSlideshow();
                        return false;
                    });
                }
                $pp_pic_holder.attr("class", "pp_pic_holder " + settings.theme);
                $pp_overlay.css({
                    opacity: 0,
                    height: $(document).height(),
                    width: $(window).width()
                }).bind("click", function() {
                    if (!settings.modal) $.prettyPhoto.close();
                });
                $("a.pp_close").bind("click", function() {
                    $.prettyPhoto.close();
                    return false;
                });
                $("a.pp_expand").bind("click", function(e) {
                    if ($(this).hasClass("pp_expand")) {
                        $(this).removeClass("pp_expand").addClass("pp_contract");
                        doresize = false;
                    } else {
                        $(this).removeClass("pp_contract").addClass("pp_expand");
                        doresize = true;
                    }
                    _hideContent(function() {
                        $.prettyPhoto.open();
                    });
                    return false;
                });
                $pp_pic_holder.find(".pp_previous, .pp_nav .pp_arrow_previous").bind("click", function() {
                    $.prettyPhoto.changePage("previous");
                    $.prettyPhoto.stopSlideshow();
                    return false;
                });
                $pp_pic_holder.find(".pp_next, .pp_nav .pp_arrow_next").bind("click", function() {
                    $.prettyPhoto.changePage("next");
                    $.prettyPhoto.stopSlideshow();
                    return false;
                });
                _center_overlay();
            }
            return this.unbind("click.prettyphoto").bind("click.prettyphoto", $.prettyPhoto.initialize);
        };
        function grab_param(name, url) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(url);
            return results == null ? "" : results[1];
        }
    })(jQuery);
    (function(d) {
        d.flexslider = function(i, k) {
            var a = d(i), c = d.extend({}, d.flexslider.defaults, k), e = c.namespace, r = "ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch, s = r ? "touchend" : "click", l = "vertical" === c.direction, m = c.reverse, h = 0 < c.itemWidth, q = "fade" === c.animation, p = "" !== c.asNavFor, f = {};
            d.data(i, "flexslider", a);
            f = {
                init: function() {
                    a.animating = !1;
                    a.currentSlide = c.startAt;
                    a.animatingTo = a.currentSlide;
                    a.atEnd = 0 === a.currentSlide || a.currentSlide === a.last;
                    a.containerSelector = c.selector.substr(0, c.selector.search(" "));
                    a.slides = d(c.selector, a);
                    a.container = d(a.containerSelector, a);
                    a.count = a.slides.length;
                    a.syncExists = 0 < d(c.sync).length;
                    "slide" === c.animation && (c.animation = "swing");
                    a.prop = l ? "top" : "marginLeft";
                    a.args = {};
                    a.manualPause = !1;
                    var b = a, g;
                    if (g = !c.video) if (g = !q) if (g = c.useCSS) a: {
                        g = document.createElement("div");
                        var n = [ "perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective" ], e;
                        for (e in n) if (void 0 !== g.style[n[e]]) {
                            a.pfx = n[e].replace("Perspective", "").toLowerCase();
                            a.prop = "-" + a.pfx + "-transform";
                            g = !0;
                            break a;
                        }
                        g = !1;
                    }
                    b.transitions = g;
                    "" !== c.controlsContainer && (a.controlsContainer = 0 < d(c.controlsContainer).length && d(c.controlsContainer));
                    "" !== c.manualControls && (a.manualControls = 0 < d(c.manualControls).length && d(c.manualControls));
                    c.randomize && (a.slides.sort(function() {
                        return Math.round(Math.random()) - .5;
                    }), a.container.empty().append(a.slides));
                    a.doMath();
                    p && f.asNav.setup();
                    a.setup("init");
                    c.controlNav && f.controlNav.setup();
                    c.directionNav && f.directionNav.setup();
                    c.keyboard && (1 === d(a.containerSelector).length || c.multipleKeyboard) && d(document).bind("keyup", function(b) {
                        b = b.keyCode;
                        if (!a.animating && (b === 39 || b === 37)) {
                            b = b === 39 ? a.getTarget("next") : b === 37 ? a.getTarget("prev") : false;
                            a.flexAnimate(b, c.pauseOnAction);
                        }
                    });
                    c.mousewheel && a.bind("mousewheel", function(b, g) {
                        b.preventDefault();
                        var d = g < 0 ? a.getTarget("next") : a.getTarget("prev");
                        a.flexAnimate(d, c.pauseOnAction);
                    });
                    c.pausePlay && f.pausePlay.setup();
                    c.slideshow && (c.pauseOnHover && a.hover(function() {
                        !a.manualPlay && !a.manualPause && a.pause();
                    }, function() {
                        !a.manualPause && !a.manualPlay && a.play();
                    }), 0 < c.initDelay ? setTimeout(a.play, c.initDelay) : a.play());
                    r && c.touch && f.touch();
                    (!q || q && c.smoothHeight) && d(window).bind("resize focus", f.resize);
                    setTimeout(function() {
                        c.start(a);
                    }, 200);
                },
                asNav: {
                    setup: function() {
                        a.asNav = !0;
                        a.animatingTo = Math.floor(a.currentSlide / a.move);
                        a.currentItem = a.currentSlide;
                        a.slides.removeClass(e + "active-slide").eq(a.currentItem).addClass(e + "active-slide");
                        a.slides.click(function(b) {
                            b.preventDefault();
                            var b = d(this), g = b.index();
                            !d(c.asNavFor).data("flexslider").animating && !b.hasClass("active") && (a.direction = a.currentItem < g ? "next" : "prev", 
                            a.flexAnimate(g, c.pauseOnAction, !1, !0, !0));
                        });
                    }
                },
                controlNav: {
                    setup: function() {
                        a.manualControls ? f.controlNav.setupManual() : f.controlNav.setupPaging();
                    },
                    setupPaging: function() {
                        var b = 1, g;
                        a.controlNavScaffold = d('<ol class="' + e + "control-nav " + e + ("thumbnails" === c.controlNav ? "control-thumbs" : "control-paging") + '"></ol>');
                        if (1 < a.pagingCount) for (var n = 0; n < a.pagingCount; n++) g = "thumbnails" === c.controlNav ? '<img src="' + a.slides.eq(n).attr("data-thumb") + '"/>' : "<a>" + b + "</a>", 
                        a.controlNavScaffold.append("<li>" + g + "</li>"), b++;
                        a.controlsContainer ? d(a.controlsContainer).append(a.controlNavScaffold) : a.append(a.controlNavScaffold);
                        f.controlNav.set();
                        f.controlNav.active();
                        a.controlNavScaffold.delegate("a, img", s, function(b) {
                            b.preventDefault();
                            var b = d(this), g = a.controlNav.index(b);
                            b.hasClass(e + "active") || (a.direction = g > a.currentSlide ? "next" : "prev", 
                            a.flexAnimate(g, c.pauseOnAction));
                        });
                        r && a.controlNavScaffold.delegate("a", "click touchstart", function(a) {
                            a.preventDefault();
                        });
                    },
                    setupManual: function() {
                        a.controlNav = a.manualControls;
                        f.controlNav.active();
                        a.controlNav.live(s, function(b) {
                            b.preventDefault();
                            var b = d(this), g = a.controlNav.index(b);
                            b.hasClass(e + "active") || (g > a.currentSlide ? a.direction = "next" : a.direction = "prev", 
                            a.flexAnimate(g, c.pauseOnAction));
                        });
                        r && a.controlNav.live("click touchstart", function(a) {
                            a.preventDefault();
                        });
                    },
                    set: function() {
                        a.controlNav = d("." + e + "control-nav li " + ("thumbnails" === c.controlNav ? "img" : "a"), a.controlsContainer ? a.controlsContainer : a);
                    },
                    active: function() {
                        a.controlNav.removeClass(e + "active").eq(a.animatingTo).addClass(e + "active");
                    },
                    update: function(b, c) {
                        1 < a.pagingCount && "add" === b ? a.controlNavScaffold.append(d("<li><a>" + a.count + "</a></li>")) : 1 === a.pagingCount ? a.controlNavScaffold.find("li").remove() : a.controlNav.eq(c).closest("li").remove();
                        f.controlNav.set();
                        1 < a.pagingCount && a.pagingCount !== a.controlNav.length ? a.update(c, b) : f.controlNav.active();
                    }
                },
                directionNav: {
                    setup: function() {
                        var b = d('<ul class="' + e + 'direction-nav"><li><a class="' + e + 'prev" href="#"><i class="icon-angle-left"></i>' + c.prevText + '</a></li><li><a class="' + e + 'next" href="#"><i class="icon-angle-right ' + c.nextText + '"></i>' + c.nextText + "</a></li></ul>");
                        a.controlsContainer ? (d(a.controlsContainer).append(b), a.directionNav = d("." + e + "direction-nav li a", a.controlsContainer)) : (a.append(b), 
                        a.directionNav = d("." + e + "direction-nav li a", a));
                        f.directionNav.update();
                        a.directionNav.bind(s, function(b) {
                            b.preventDefault();
                            b = d(this).hasClass(e + "next") ? a.getTarget("next") : a.getTarget("prev");
                            a.flexAnimate(b, c.pauseOnAction);
                        });
                        r && a.directionNav.bind("click touchstart", function(a) {
                            a.preventDefault();
                        });
                    },
                    update: function() {
                        var b = e + "disabled";
                        1 === a.pagingCount ? a.directionNav.addClass(b) : c.animationLoop ? a.directionNav.removeClass(b) : 0 === a.animatingTo ? a.directionNav.removeClass(b).filter("." + e + "prev").addClass(b) : a.animatingTo === a.last ? a.directionNav.removeClass(b).filter("." + e + "next").addClass(b) : a.directionNav.removeClass(b);
                    }
                },
                pausePlay: {
                    setup: function() {
                        var b = d('<div class="' + e + 'pauseplay"><a></a></div>');
                        a.controlsContainer ? (a.controlsContainer.append(b), a.pausePlay = d("." + e + "pauseplay a", a.controlsContainer)) : (a.append(b), 
                        a.pausePlay = d("." + e + "pauseplay a", a));
                        f.pausePlay.update(c.slideshow ? e + "pause" : e + "play");
                        a.pausePlay.bind(s, function(b) {
                            b.preventDefault();
                            if (d(this).hasClass(e + "pause")) {
                                a.manualPause = true;
                                a.manualPlay = false;
                                a.pause();
                            } else {
                                a.manualPause = false;
                                a.manualPlay = true;
                                a.play();
                            }
                        });
                        r && a.pausePlay.bind("click touchstart", function(a) {
                            a.preventDefault();
                        });
                    },
                    update: function(b) {
                        "play" === b ? a.pausePlay.removeClass(e + "pause").addClass(e + "play").text(c.playText) : a.pausePlay.removeClass(e + "play").addClass(e + "pause").text(c.pauseText);
                    }
                },
                touch: function() {
                    function b(b) {
                        j = l ? d - b.touches[0].pageY : d - b.touches[0].pageX;
                        p = l ? Math.abs(j) < Math.abs(b.touches[0].pageX - e) : Math.abs(j) < Math.abs(b.touches[0].pageY - e);
                        if (!p || 500 < Number(new Date()) - k) b.preventDefault(), !q && a.transitions && (c.animationLoop || (j /= 0 === a.currentSlide && 0 > j || a.currentSlide === a.last && 0 < j ? Math.abs(j) / o + 2 : 1), 
                        a.setProps(f + j, "setTouch"));
                    }
                    function g() {
                        if (a.animatingTo === a.currentSlide && !p && null !== j) {
                            var h = m ? -j : j, l = 0 < h ? a.getTarget("next") : a.getTarget("prev");
                            a.canAdvance(l) && (550 > Number(new Date()) - k && 50 < Math.abs(h) || Math.abs(h) > o / 2) ? a.flexAnimate(l, c.pauseOnAction) : a.flexAnimate(a.currentSlide, c.pauseOnAction, !0);
                        }
                        i.removeEventListener("touchmove", b, !1);
                        i.removeEventListener("touchend", g, !1);
                        f = j = e = d = null;
                    }
                    var d, e, f, o, j, k, p = !1;
                    i.addEventListener("touchstart", function(j) {
                        a.animating ? j.preventDefault() : 1 === j.touches.length && (a.pause(), o = l ? a.h : a.w, 
                        k = Number(new Date()), f = h && m && a.animatingTo === a.last ? 0 : h && m ? a.limit - (a.itemW + c.itemMargin) * a.move * a.animatingTo : h && a.currentSlide === a.last ? a.limit : h ? (a.itemW + c.itemMargin) * a.move * a.currentSlide : m ? (a.last - a.currentSlide + a.cloneOffset) * o : (a.currentSlide + a.cloneOffset) * o, 
                        d = l ? j.touches[0].pageY : j.touches[0].pageX, e = l ? j.touches[0].pageX : j.touches[0].pageY, 
                        i.addEventListener("touchmove", b, !1), i.addEventListener("touchend", g, !1));
                    }, !1);
                },
                resize: function() {
                    !a.animating && a.is(":visible") && (h || a.doMath(), q ? f.smoothHeight() : h ? (a.slides.width(a.computedW), 
                    a.update(a.pagingCount), a.setProps()) : l ? (a.viewport.height(a.h), a.setProps(a.h, "setTotal")) : (c.smoothHeight && f.smoothHeight(), 
                    a.newSlides.width(a.computedW), a.setProps(a.computedW, "setTotal")));
                },
                smoothHeight: function(b) {
                    if (!l || q) {
                        var c = q ? a : a.viewport;
                        b ? c.animate({
                            height: a.slides.eq(a.animatingTo).height()
                        }, b) : c.height(a.slides.eq(a.animatingTo).height());
                    }
                },
                sync: function(b) {
                    var g = d(c.sync).data("flexslider"), e = a.animatingTo;
                    switch (b) {
                      case "animate":
                        g.flexAnimate(e, c.pauseOnAction, !1, !0);
                        break;

                      case "play":
                        !g.playing && !g.asNav && g.play();
                        break;

                      case "pause":
                        g.pause();
                    }
                }
            };
            a.flexAnimate = function(b, g, n, i, k) {
                p && 1 === a.pagingCount && (a.direction = a.currentItem < b ? "next" : "prev");
                if (!a.animating && (a.canAdvance(b, k) || n) && a.is(":visible")) {
                    if (p && i) if (n = d(c.asNavFor).data("flexslider"), a.atEnd = 0 === b || b === a.count - 1, 
                    n.flexAnimate(b, !0, !1, !0, k), a.direction = a.currentItem < b ? "next" : "prev", 
                    n.direction = a.direction, Math.ceil((b + 1) / a.visible) - 1 !== a.currentSlide && 0 !== b) a.currentItem = b, 
                    a.slides.removeClass(e + "active-slide").eq(b).addClass(e + "active-slide"), b = Math.floor(b / a.visible); else return a.currentItem = b, 
                    a.slides.removeClass(e + "active-slide").eq(b).addClass(e + "active-slide"), !1;
                    a.animating = !0;
                    a.animatingTo = b;
                    c.before(a);
                    g && a.pause();
                    a.syncExists && !k && f.sync("animate");
                    c.controlNav && f.controlNav.active();
                    h || a.slides.removeClass(e + "active-slide").eq(b).addClass(e + "active-slide");
                    a.atEnd = 0 === b || b === a.last;
                    c.directionNav && f.directionNav.update();
                    b === a.last && (c.end(a), c.animationLoop || a.pause());
                    if (q) a.slides.eq(a.currentSlide).fadeOut(c.animationSpeed, c.easing), a.slides.eq(b).fadeIn(c.animationSpeed, c.easing, a.wrapup); else {
                        var o = l ? a.slides.filter(":first").height() : a.computedW;
                        h ? (b = c.itemWidth > a.w ? 2 * c.itemMargin : c.itemMargin, b = (a.itemW + b) * a.move * a.animatingTo, 
                        b = b > a.limit && 1 !== a.visible ? a.limit : b) : b = 0 === a.currentSlide && b === a.count - 1 && c.animationLoop && "next" !== a.direction ? m ? (a.count + a.cloneOffset) * o : 0 : a.currentSlide === a.last && 0 === b && c.animationLoop && "prev" !== a.direction ? m ? 0 : (a.count + 1) * o : m ? (a.count - 1 - b + a.cloneOffset) * o : (b + a.cloneOffset) * o;
                        a.setProps(b, "", c.animationSpeed);
                        if (a.transitions) {
                            if (!c.animationLoop || !a.atEnd) a.animating = !1, a.currentSlide = a.animatingTo;
                            a.container.unbind("webkitTransitionEnd transitionend");
                            a.container.bind("webkitTransitionEnd transitionend", function() {
                                a.wrapup(o);
                            });
                        } else a.container.animate(a.args, c.animationSpeed, c.easing, function() {
                            a.wrapup(o);
                        });
                    }
                    c.smoothHeight && f.smoothHeight(c.animationSpeed);
                }
            };
            a.wrapup = function(b) {
                !q && !h && (0 === a.currentSlide && a.animatingTo === a.last && c.animationLoop ? a.setProps(b, "jumpEnd") : a.currentSlide === a.last && (0 === a.animatingTo && c.animationLoop) && a.setProps(b, "jumpStart"));
                a.animating = !1;
                a.currentSlide = a.animatingTo;
                c.after(a);
            };
            a.animateSlides = function() {
                a.animating || a.flexAnimate(a.getTarget("next"));
            };
            a.pause = function() {
                clearInterval(a.animatedSlides);
                a.playing = !1;
                c.pausePlay && f.pausePlay.update("play");
                a.syncExists && f.sync("pause");
            };
            a.play = function() {
                a.animatedSlides = setInterval(a.animateSlides, c.slideshowSpeed);
                a.playing = !0;
                c.pausePlay && f.pausePlay.update("pause");
                a.syncExists && f.sync("play");
            };
            a.canAdvance = function(b, g) {
                var d = p ? a.pagingCount - 1 : a.last;
                return g ? !0 : p && a.currentItem === a.count - 1 && 0 === b && "prev" === a.direction ? !0 : p && 0 === a.currentItem && b === a.pagingCount - 1 && "next" !== a.direction ? !1 : b === a.currentSlide && !p ? !1 : c.animationLoop ? !0 : a.atEnd && 0 === a.currentSlide && b === d && "next" !== a.direction ? !1 : a.atEnd && a.currentSlide === d && 0 === b && "next" === a.direction ? !1 : !0;
            };
            a.getTarget = function(b) {
                a.direction = b;
                return "next" === b ? a.currentSlide === a.last ? 0 : a.currentSlide + 1 : 0 === a.currentSlide ? a.last : a.currentSlide - 1;
            };
            a.setProps = function(b, g, d) {
                var e, f = b ? b : (a.itemW + c.itemMargin) * a.move * a.animatingTo;
                e = -1 * function() {
                    if (h) return "setTouch" === g ? b : m && a.animatingTo === a.last ? 0 : m ? a.limit - (a.itemW + c.itemMargin) * a.move * a.animatingTo : a.animatingTo === a.last ? a.limit : f;
                    switch (g) {
                      case "setTotal":
                        return m ? (a.count - 1 - a.currentSlide + a.cloneOffset) * b : (a.currentSlide + a.cloneOffset) * b;

                      case "setTouch":
                        return b;

                      case "jumpEnd":
                        return m ? b : a.count * b;

                      case "jumpStart":
                        return m ? a.count * b : b;

                      default:
                        return b;
                    }
                }() + "px";
                a.transitions && (e = l ? "translate3d(0," + e + ",0)" : "translate3d(" + e + ",0,0)", 
                d = void 0 !== d ? d / 1e3 + "s" : "0s", a.container.css("-" + a.pfx + "-transition-duration", d));
                a.args[a.prop] = e;
                (a.transitions || void 0 === d) && a.container.css(a.args);
            };
            a.setup = function(b) {
                if (q) a.slides.css({
                    width: "100%",
                    "float": "left",
                    marginRight: "-100%",
                    position: "relative"
                }), "init" === b && a.slides.eq(a.currentSlide).fadeIn(c.animationSpeed, c.easing), 
                c.smoothHeight && f.smoothHeight(); else {
                    var g, n;
                    "init" === b && (a.viewport = d('<div class="' + e + 'viewport"></div>').css({
                        overflow: "hidden",
                        position: "relative"
                    }).appendTo(a).append(a.container), a.cloneCount = 0, a.cloneOffset = 0, m && (n = d.makeArray(a.slides).reverse(), 
                    a.slides = d(n), a.container.empty().append(a.slides)));
                    c.animationLoop && !h && (a.cloneCount = 2, a.cloneOffset = 1, "init" !== b && a.container.find(".clone").remove(), 
                    a.container.append(a.slides.first().clone().addClass("clone")).prepend(a.slides.last().clone().addClass("clone")));
                    a.newSlides = d(c.selector, a);
                    g = m ? a.count - 1 - a.currentSlide + a.cloneOffset : a.currentSlide + a.cloneOffset;
                    l && !h ? (a.container.height(200 * (a.count + a.cloneCount) + "%").css("position", "absolute").width("100%"), 
                    setTimeout(function() {
                        a.newSlides.css({
                            display: "block"
                        });
                        a.doMath();
                        a.viewport.height(a.h);
                        a.setProps(g * a.h, "init");
                    }, "init" === b ? 100 : 0)) : (a.container.width(200 * (a.count + a.cloneCount) + "%"), 
                    a.setProps(g * a.computedW, "init"), setTimeout(function() {
                        a.doMath();
                        a.newSlides.css({
                            width: a.computedW,
                            "float": "left",
                            display: "block"
                        });
                        c.smoothHeight && f.smoothHeight();
                    }, "init" === b ? 100 : 0));
                }
                h || a.slides.removeClass(e + "active-slide").eq(a.currentSlide).addClass(e + "active-slide");
            };
            a.doMath = function() {
                var b = a.slides.first(), d = c.itemMargin, e = c.minItems, f = c.maxItems;
                a.w = a.width();
                a.h = b.height();
                a.boxPadding = b.outerWidth() - b.width();
                h ? (a.itemT = c.itemWidth + d, a.minW = e ? e * a.itemT : a.w, a.maxW = f ? f * a.itemT : a.w, 
                a.itemW = a.minW > a.w ? (a.w - d * e) / e : a.maxW < a.w ? (a.w - d * f) / f : c.itemWidth > a.w ? a.w : c.itemWidth, 
                a.visible = Math.floor(a.w / (a.itemW + d)), a.move = 0 < c.move && c.move < a.visible ? c.move : a.visible, 
                a.pagingCount = Math.ceil((a.count - a.visible) / a.move + 1), a.last = a.pagingCount - 1, 
                a.limit = 1 === a.pagingCount ? 0 : c.itemWidth > a.w ? (a.itemW + 2 * d) * a.count - a.w - d : (a.itemW + d) * a.count - a.w - d) : (a.itemW = a.w, 
                a.pagingCount = a.count, a.last = a.count - 1);
                a.computedW = a.itemW - a.boxPadding;
            };
            a.update = function(b, d) {
                a.doMath();
                h || (b < a.currentSlide ? a.currentSlide += 1 : b <= a.currentSlide && 0 !== b && (a.currentSlide -= 1), 
                a.animatingTo = a.currentSlide);
                if (c.controlNav && !a.manualControls) if ("add" === d && !h || a.pagingCount > a.controlNav.length) f.controlNav.update("add"); else if ("remove" === d && !h || a.pagingCount < a.controlNav.length) h && a.currentSlide > a.last && (a.currentSlide -= 1, 
                a.animatingTo -= 1), f.controlNav.update("remove", a.last);
                c.directionNav && f.directionNav.update();
            };
            a.addSlide = function(b, e) {
                var f = d(b);
                a.count += 1;
                a.last = a.count - 1;
                l && m ? void 0 !== e ? a.slides.eq(a.count - e).after(f) : a.container.prepend(f) : void 0 !== e ? a.slides.eq(e).before(f) : a.container.append(f);
                a.update(e, "add");
                a.slides = d(c.selector + ":not(.clone)", a);
                a.setup();
                c.added(a);
            };
            a.removeSlide = function(b) {
                var e = isNaN(b) ? a.slides.index(d(b)) : b;
                a.count -= 1;
                a.last = a.count - 1;
                isNaN(b) ? d(b, a.slides).remove() : l && m ? a.slides.eq(a.last).remove() : a.slides.eq(b).remove();
                a.doMath();
                a.update(e, "remove");
                a.slides = d(c.selector + ":not(.clone)", a);
                a.setup();
                c.removed(a);
            };
            f.init();
        };
        d.flexslider.defaults = {
            namespace: "flex-",
            selector: ".slides > li",
            animation: "fade",
            easing: "swing",
            direction: "horizontal",
            reverse: !1,
            animationLoop: !0,
            smoothHeight: !1,
            startAt: 0,
            slideshow: !0,
            slideshowSpeed: 7e3,
            animationSpeed: 600,
            initDelay: 0,
            randomize: !1,
            pauseOnAction: !0,
            pauseOnHover: !1,
            useCSS: !0,
            touch: !0,
            video: !1,
            controlNav: !0,
            directionNav: !0,
            prevText: "",
            nextText: "",
            keyboard: !0,
            multipleKeyboard: !1,
            mousewheel: !1,
            pausePlay: !1,
            pauseText: "Pause",
            playText: "Play",
            controlsContainer: "",
            manualControls: "",
            sync: "",
            asNavFor: "",
            itemWidth: 0,
            itemMargin: 0,
            minItems: 0,
            maxItems: 0,
            move: 0,
            start: function() {},
            before: function() {},
            after: function() {},
            end: function() {},
            added: function() {},
            removed: function() {}
        };
        d.fn.flexslider = function(i) {
            void 0 === i && (i = {});
            if ("object" === typeof i) return this.each(function() {
                var a = d(this), c = a.find(i.selector ? i.selector : ".slides > li");
                1 === c.length ? (c.fadeIn(400), i.start && i.start(a)) : void 0 === a.data("flexslider") && new d.flexslider(this, i);
            });
            var k = d(this).data("flexslider");
            switch (i) {
              case "play":
                k.play();
                break;

              case "pause":
                k.pause();
                break;

              case "next":
                k.flexAnimate(k.getTarget("next"), !0);
                break;

              case "prev":
              case "previous":
                k.flexAnimate(k.getTarget("prev"), !0);
                break;

              default:
                "number" === typeof i && k.flexAnimate(i, !0);
            }
        };
    })(jQuery);
    (function($) {
        "use strict";
        var k = {}, max = Math.max, min = Math.min;
        k.c = {};
        k.c.d = $(document);
        k.c.t = function(e) {
            return e.originalEvent.touches.length - 1;
        };
        k.o = function() {
            var s = this;
            this.o = null;
            this.$ = null;
            this.i = null;
            this.g = null;
            this.v = null;
            this.cv = null;
            this.x = 0;
            this.y = 0;
            this.$c = null;
            this.c = null;
            this.t = 0;
            this.isInit = false;
            this.fgColor = null;
            this.pColor = null;
            this.dH = null;
            this.cH = null;
            this.eH = null;
            this.rH = null;
            this.usesExcanvas = typeof G_vmlCanvasManager !== "undefined";
            this.run = function() {
                var cf = function(e, conf) {
                    var k;
                    for (k in conf) {
                        s.o[k] = conf[k];
                    }
                    s.init();
                    s._configure()._draw();
                };
                if (this.$.data("kontroled")) return;
                this.$.data("kontroled", true);
                this.extend();
                this.o = $.extend({
                    min: this.$.data("min") || 0,
                    max: this.$.data("max") || 100,
                    stopper: true,
                    readOnly: this.$.data("readonly"),
                    cursor: this.$.data("cursor") === true && 30 || this.$.data("cursor") || 0,
                    thickness: this.$.data("thickness") || .35,
                    width: this.$.data("width") || 200,
                    height: this.$.data("height") || 200,
                    displayInput: this.$.data("displayinput") == null || this.$.data("displayinput"),
                    displayPrevious: this.$.data("displayprevious"),
                    fgColor: this.$.data("fgcolor") || "#87CEEB",
                    inline: false,
                    draw: null,
                    change: null,
                    cancel: null,
                    release: null
                }, this.o);
                if (this.$.is("fieldset")) {
                    this.v = {};
                    this.i = this.$.find("input");
                    this.i.each(function(k) {
                        var $this = $(this);
                        s.i[k] = $this;
                        s.v[k] = $this.val();
                        $this.bind("change", function() {
                            var val = {};
                            val[k] = $this.val();
                            s.val(val);
                        });
                    });
                    this.$.find("legend").remove();
                } else {
                    this.i = this.$;
                    this.v = this.$.val();
                    this.v == "" && (this.v = this.o.min);
                    this.$.bind("change", function() {
                        s.val(s.$.val());
                    });
                }
                !this.o.displayInput && this.$.hide();
                this.$c = $(document.createElement("canvas")).attr({
                    width: this.o.width,
                    height: this.o.height
                });
                this.$.wrap($('<div style="' + (this.o.inline ? "display:inline;" : "") + "width:" + this.o.width + "px;height:" + this.o.height + 'px;"></div>')).before(this.$c);
                if (this.usesExcanvas) {
                    G_vmlCanvasManager.initElement(this.$c[0]);
                }
                this.c = this.$c[0].getContext("2d");
                if (this.v instanceof Object) {
                    this.cv = {};
                    this.copy(this.v, this.cv);
                } else {
                    this.cv = this.v;
                }
                this.$.bind("configure", cf).parent().bind("configure", cf);
                this._listen()._configure()._xy().init();
                this.isInit = true;
                this._draw();
                return this;
            };
            this._draw = function() {
                var d = true;
                s.g = s.c;
                s.clear();
                s.dH && (d = s.dH());
                d !== false && s.draw();
            };
            this._touch = function(e) {
                var touchMove = function(e) {
                    var v = s.xy2val(e.originalEvent.touches[s.t].pageX, e.originalEvent.touches[s.t].pageY);
                    if (v == s.cv) return;
                    if (s.cH && s.cH(v) === false) return;
                    s.change(v);
                    s._draw();
                };
                this.t = k.c.t(e);
                touchMove(e);
                k.c.d.bind("touchmove.k", touchMove).bind("touchend.k", function() {
                    k.c.d.unbind("touchmove.k touchend.k");
                    if (s.rH && s.rH(s.cv) === false) return;
                    s.val(s.cv);
                });
                return this;
            };
            this._mouse = function(e) {
                var mouseMove = function(e) {
                    var v = s.xy2val(e.pageX, e.pageY);
                    if (v == s.cv) return;
                    if (s.cH && s.cH(v) === false) return;
                    s.change(v);
                    s._draw();
                };
                mouseMove(e);
                k.c.d.bind("mousemove.k", mouseMove).bind("keyup.k", function(e) {
                    if (e.keyCode === 27) {
                        k.c.d.unbind("mouseup.k mousemove.k keyup.k");
                        if (s.eH && s.eH() === false) return;
                        s.cancel();
                    }
                }).bind("mouseup.k", function(e) {
                    k.c.d.unbind("mousemove.k mouseup.k keyup.k");
                    if (s.rH && s.rH(s.cv) === false) return;
                    s.val(s.cv);
                });
                return this;
            };
            this._xy = function() {
                var o = this.$c.offset();
                this.x = o.left;
                this.y = o.top;
                return this;
            };
            this._listen = function() {
                if (!this.o.readOnly) {
                    this.$c.bind("mousedown", function(e) {
                        e.preventDefault();
                        s._xy()._mouse(e);
                    }).bind("touchstart", function(e) {
                        e.preventDefault();
                        s._xy()._touch(e);
                    });
                    this.listen();
                } else {
                    this.$.attr("readonly", "readonly");
                }
                return this;
            };
            this._configure = function() {
                if (this.o.draw) this.dH = this.o.draw;
                if (this.o.change) this.cH = this.o.change;
                if (this.o.cancel) this.eH = this.o.cancel;
                if (this.o.release) this.rH = this.o.release;
                if (this.o.displayPrevious) {
                    this.pColor = this.h2rgba(this.o.fgColor, "0.4");
                    this.fgColor = this.h2rgba(this.o.fgColor, "0.6");
                } else {
                    this.fgColor = this.o.fgColor;
                }
                return this;
            };
            this._clear = function() {
                this.$c[0].width = this.$c[0].width;
            };
            this.listen = function() {};
            this.extend = function() {};
            this.init = function() {};
            this.change = function(v) {};
            this.val = function(v) {};
            this.xy2val = function(x, y) {};
            this.draw = function() {};
            this.clear = function() {
                this._clear();
            };
            this.h2rgba = function(h, a) {
                var rgb;
                h = h.substring(1, 7);
                rgb = [ parseInt(h.substring(0, 2), 16), parseInt(h.substring(2, 4), 16), parseInt(h.substring(4, 6), 16) ];
                return "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + "," + a + ")";
            };
            this.copy = function(f, t) {
                for (var i in f) {
                    t[i] = f[i];
                }
            };
        };
        k.Dial = function() {
            k.o.call(this);
            this.startAngle = null;
            this.xy = null;
            this.radius = null;
            this.lineWidth = null;
            this.cursorExt = null;
            this.w2 = null;
            this.PI2 = 2 * Math.PI;
            this.extend = function() {
                this.o = $.extend({
                    bgColor: this.$.data("bgcolor") || "#EEEEEE",
                    angleOffset: this.$.data("angleoffset") || 0,
                    angleArc: this.$.data("anglearc") || 360,
                    inline: true
                }, this.o);
            };
            this.val = function(v) {
                if (null != v) {
                    this.cv = this.o.stopper ? max(min(v, this.o.max), this.o.min) : v;
                    this.v = this.cv;
                    if (v !== "") {
                        this.$.val(this.v);
                    }
                    this._draw();
                } else {
                    return this.v;
                }
            };
            this.xy2val = function(x, y) {
                var a, ret;
                a = Math.atan2(x - (this.x + this.w2), -(y - this.y - this.w2)) - this.angleOffset;
                if (this.angleArc != this.PI2 && a < 0 && a > -.5) {
                    a = 0;
                } else if (a < 0) {
                    a += this.PI2;
                }
                ret = ~~(.5 + a * (this.o.max - this.o.min) / this.angleArc) + this.o.min;
                this.o.stopper && (ret = max(min(ret, this.o.max), this.o.min));
                return ret;
            };
            this.listen = function() {
                var s = this, mw = function(e) {
                    e.preventDefault();
                    var ori = e.originalEvent, deltaX = ori.detail || ori.wheelDeltaX || ori.wheelDelta, deltaY = ori.detail || ori.wheelDeltaY || ori.wheelDelta, v = parseInt(s.$.val() || s.o.min) + (deltaX > 0 || deltaY > 0 ? 1 : deltaX < 0 || deltaY < 0 ? -1 : 0);
                    if (s.cH && s.cH(v) === false) return;
                    s.val(v);
                }, kval, to, m = 1, kv = {
                    37: -1,
                    38: 1,
                    39: 1,
                    40: -1
                };
                this.$.bind("keydown", function(e) {
                    var kc = e.keyCode;
                    if (kc >= 96 && kc <= 105) {
                        kc = e.keyCode = kc - 48;
                    }
                    kval = parseInt(String.fromCharCode(kc));
                    if (isNaN(kval)) {
                        kc !== 13 && kc !== 8 && kc !== 9 && kc !== 189 && e.preventDefault();
                        if ($.inArray(kc, [ 37, 38, 39, 40 ]) > -1) {
                            e.preventDefault();
                            var v = parseInt(s.$.val()) + kv[kc] * m;
                            s.o.stopper && (v = max(min(v, s.o.max), s.o.min));
                            s.change(v);
                            s._draw();
                            to = window.setTimeout(function() {
                                m *= 2;
                            }, 30);
                        }
                    }
                }).bind("keyup", function(e) {
                    if (isNaN(kval)) {
                        if (to) {
                            window.clearTimeout(to);
                            to = null;
                            m = 1;
                            s.val(s.$.val());
                        }
                    } else {
                        s.$.val() > s.o.max && s.$.val(s.o.max) || s.$.val() < s.o.min && s.$.val(s.o.min);
                    }
                });
                this.$c.bind("mousewheel DOMMouseScroll", mw);
                this.$.bind("mousewheel DOMMouseScroll", mw);
            };
            this.init = function() {
                if (this.v < this.o.min || this.v > this.o.max) this.v = this.o.min;
                this.$.val(this.v);
                this.w2 = this.o.width / 2;
                this.cursorExt = this.o.cursor / 100;
                this.xy = this.w2;
                this.lineWidth = this.xy * this.o.thickness;
                this.radius = this.xy - this.lineWidth / 2;
                this.o.angleOffset && (this.o.angleOffset = isNaN(this.o.angleOffset) ? 0 : this.o.angleOffset);
                this.o.angleArc && (this.o.angleArc = isNaN(this.o.angleArc) ? this.PI2 : this.o.angleArc);
                this.angleOffset = this.o.angleOffset * Math.PI / 180;
                this.angleArc = this.o.angleArc * Math.PI / 180;
                this.startAngle = 1.5 * Math.PI + this.angleOffset;
                this.endAngle = 1.5 * Math.PI + this.angleOffset + this.angleArc;
                var s = max(String(Math.abs(this.o.max)).length, String(Math.abs(this.o.min)).length, 2) + 2;
                this.o.displayInput && this.i.css({
                    width: (this.o.width / 2 + 4 >> 0) + "px",
                    height: (this.o.width / 3 >> 0) + "px",
                    position: "absolute",
                    "vertical-align": "middle",
                    "margin-top": (this.o.width / 3 >> 0) + "px",
                    "margin-left": "-" + (this.o.width * 3 / 4 + 2 >> 0) + "px",
                    border: 0,
                    background: "none",
                    font: "bold " + (this.o.width / s >> 0) + "px Arial",
                    "text-align": "center",
                    color: this.o.fgColor,
                    padding: "0px",
                    "-webkit-appearance": "none"
                }) || this.i.css({
                    width: "0px",
                    visibility: "hidden"
                });
            };
            this.change = function(v) {
                this.cv = v;
                this.$.val(v);
            };
            this.angle = function(v) {
                return (v - this.o.min) * this.angleArc / (this.o.max - this.o.min);
            };
            this.draw = function() {
                var c = this.g, a = this.angle(this.cv), sat = this.startAngle, eat = sat + a, oa, sa, ea, r = 1;
                c.lineWidth = this.lineWidth;
                this.o.cursor && (sat = eat - this.cursorExt) && (eat = eat + this.cursorExt);
                c.beginPath();
                c.strokeStyle = this.o.bgColor;
                c.arc(this.xy, this.xy, this.radius, this.endAngle, this.startAngle, true);
                if (this.angleArc === 2 * Math.PI) {
                    c.closePath();
                }
                c.stroke();
                if (this.o.displayPrevious) {
                    oa = this.angle(this.v);
                    ea = this.startAngle + oa;
                    sa = this.startAngle;
                    this.o.cursor && (sa = ea - this.cursorExt) && (ea = ea + this.cursorExt);
                    if (oa) {
                        c.beginPath();
                        c.strokeStyle = this.pColor;
                        if (oa === 2 * Math.PI) {
                            c.arc(this.xy, this.xy, this.radius, sa, ea - 1e-4, false);
                            c.closePath();
                        } else {
                            c.arc(this.xy, this.xy, this.radius, sa, ea, false);
                        }
                        c.stroke();
                    }
                    r = this.cv == this.v;
                }
                if (a) {
                    c.beginPath();
                    c.strokeStyle = r ? this.o.fgColor : this.fgColor;
                    if (a === 2 * Math.PI) {
                        c.arc(this.xy, this.xy, this.radius, sat, eat - 1e-4, false);
                        c.closePath();
                    } else {
                        c.arc(this.xy, this.xy, this.radius, sat, eat, false);
                    }
                    c.stroke();
                }
            };
            this.cancel = function() {
                this.val(this.v);
            };
        };
        $.fn.dial = $.fn.knob = function(o) {
            return this.each(function() {
                var d = new k.Dial();
                d.o = o;
                d.$ = $(this);
                d.run();
            }).parent();
        };
    })(jQuery);
    (function(e) {
        var z = !1, E = !1, L = 5e3, M = 2e3, y = 0, N = function() {
            var e = document.getElementsByTagName("script"), e = e[e.length - 1].src.split("?")[0];
            return 0 < e.split("/").length ? e.split("/").slice(0, -1).join("/") + "/" : "";
        }(), H = [ "ms", "moz", "webkit", "o" ], v = window.requestAnimationFrame || !1, w = window.cancelAnimationFrame || !1;
        if (!v) for (var O in H) {
            var F = H[O];
            v || (v = window[F + "RequestAnimationFrame"]);
            w || (w = window[F + "CancelAnimationFrame"] || window[F + "CancelRequestAnimationFrame"]);
        }
        var A = window.MutationObserver || window.WebKitMutationObserver || !1, I = {
            zindex: "auto",
            cursoropacitymin: 0,
            cursoropacitymax: 1,
            cursorcolor: "#424242",
            cursorwidth: "5px",
            cursorborder: "1px solid #fff",
            cursorborderradius: "5px",
            scrollspeed: 60,
            mousescrollstep: 24,
            touchbehavior: !1,
            hwacceleration: !0,
            usetransition: !0,
            boxzoom: !1,
            dblclickzoom: !0,
            gesturezoom: !0,
            grabcursorenabled: !0,
            autohidemode: !0,
            background: "",
            iframeautoresize: !0,
            cursorminheight: 32,
            preservenativescrolling: !0,
            railoffset: !1,
            bouncescroll: !0,
            spacebarenabled: !0,
            railpadding: {
                top: 0,
                right: 0,
                left: 0,
                bottom: 0
            },
            disableoutline: !0,
            horizrailenabled: !0,
            railalign: "right",
            railvalign: "bottom",
            enabletranslate3d: !0,
            enablemousewheel: !0,
            enablekeyboard: !0,
            smoothscroll: !0,
            sensitiverail: !0,
            enablemouselockapi: !0,
            cursorfixedheight: !1,
            directionlockdeadzone: 6,
            hidecursordelay: 400,
            nativeparentscrolling: !0,
            enablescrollonselection: !0,
            overflowx: !0,
            overflowy: !0,
            cursordragspeed: .3,
            rtlmode: !1,
            cursordragontouch: !1,
            oneaxismousemode: "auto"
        }, G = !1, P = function() {
            if (G) return G;
            var e = document.createElement("DIV"), c = {
                haspointerlock: "pointerLockElement" in document || "mozPointerLockElement" in document || "webkitPointerLockElement" in document
            };
            c.isopera = "opera" in window;
            c.isopera12 = c.isopera && "getUserMedia" in navigator;
            c.isoperamini = "[object OperaMini]" === Object.prototype.toString.call(window.operamini);
            c.isie = "all" in document && "attachEvent" in e && !c.isopera;
            c.isieold = c.isie && !("msInterpolationMode" in e.style);
            c.isie7 = c.isie && !c.isieold && (!("documentMode" in document) || 7 == document.documentMode);
            c.isie8 = c.isie && "documentMode" in document && 8 == document.documentMode;
            c.isie9 = c.isie && "performance" in window && 9 <= document.documentMode;
            c.isie10 = c.isie && "performance" in window && 10 <= document.documentMode;
            c.isie9mobile = /iemobile.9/i.test(navigator.userAgent);
            c.isie9mobile && (c.isie9 = !1);
            c.isie7mobile = !c.isie9mobile && c.isie7 && /iemobile/i.test(navigator.userAgent);
            c.ismozilla = "MozAppearance" in e.style;
            c.iswebkit = "WebkitAppearance" in e.style;
            c.ischrome = "chrome" in window;
            c.ischrome22 = c.ischrome && c.haspointerlock;
            c.ischrome26 = c.ischrome && "transition" in e.style;
            c.cantouch = "ontouchstart" in document.documentElement || "ontouchstart" in window;
            c.hasmstouch = window.navigator.msPointerEnabled || !1;
            c.ismac = /^mac$/i.test(navigator.platform);
            c.isios = c.cantouch && /iphone|ipad|ipod/i.test(navigator.platform);
            c.isios4 = c.isios && !("seal" in Object);
            c.isandroid = /android/i.test(navigator.userAgent);
            c.trstyle = !1;
            c.hastransform = !1;
            c.hastranslate3d = !1;
            c.transitionstyle = !1;
            c.hastransition = !1;
            c.transitionend = !1;
            for (var h = [ "transform", "msTransform", "webkitTransform", "MozTransform", "OTransform" ], l = 0; l < h.length; l++) if ("undefined" != typeof e.style[h[l]]) {
                c.trstyle = h[l];
                break;
            }
            c.hastransform = !1 != c.trstyle;
            c.hastransform && (e.style[c.trstyle] = "translate3d(1px,2px,3px)", c.hastranslate3d = /translate3d/.test(e.style[c.trstyle]));
            c.transitionstyle = !1;
            c.prefixstyle = "";
            c.transitionend = !1;
            for (var h = "transition webkitTransition MozTransition OTransition OTransition msTransition KhtmlTransition".split(" "), q = " -webkit- -moz- -o- -o -ms- -khtml-".split(" "), t = "transitionend webkitTransitionEnd transitionend otransitionend oTransitionEnd msTransitionEnd KhtmlTransitionEnd".split(" "), l = 0; l < h.length; l++) if (h[l] in e.style) {
                c.transitionstyle = h[l];
                c.prefixstyle = q[l];
                c.transitionend = t[l];
                break;
            }
            c.ischrome26 && (c.prefixstyle = q[1]);
            c.hastransition = c.transitionstyle;
            a: {
                h = [ "-moz-grab", "-webkit-grab", "grab" ];
                if (c.ischrome && !c.ischrome22 || c.isie) h = [];
                for (l = 0; l < h.length; l++) if (q = h[l], e.style.cursor = q, e.style.cursor == q) {
                    h = q;
                    break a;
                }
                h = "url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur),n-resize";
            }
            c.cursorgrabvalue = h;
            c.hasmousecapture = "setCapture" in e;
            c.hasMutationObserver = !1 !== A;
            return G = c;
        }, Q = function(k, c) {
            function h() {
                var d = b.win;
                if ("zIndex" in d) return d.zIndex();
                for (;0 < d.length && 9 != d[0].nodeType; ) {
                    var c = d.css("zIndex");
                    if (!isNaN(c) && 0 != c) return parseInt(c);
                    d = d.parent();
                }
                return !1;
            }
            function l(d, c, g) {
                c = d.css(c);
                d = parseFloat(c);
                return isNaN(d) ? (d = u[c] || 0, g = 3 == d ? g ? b.win.outerHeight() - b.win.innerHeight() : b.win.outerWidth() - b.win.innerWidth() : 1, 
                b.isie8 && d && (d += 1), g ? d : 0) : d;
            }
            function q(d, c, g, f) {
                b._bind(d, c, function(b) {
                    b = b ? b : window.event;
                    var f = {
                        original: b,
                        target: b.target || b.srcElement,
                        type: "wheel",
                        deltaMode: "MozMousePixelScroll" == b.type ? 0 : 1,
                        deltaX: 0,
                        deltaZ: 0,
                        preventDefault: function() {
                            b.preventDefault ? b.preventDefault() : b.returnValue = !1;
                            return !1;
                        },
                        stopImmediatePropagation: function() {
                            b.stopImmediatePropagation ? b.stopImmediatePropagation() : b.cancelBubble = !0;
                        }
                    };
                    "mousewheel" == c ? (f.deltaY = -.025 * b.wheelDelta, b.wheelDeltaX && (f.deltaX = -.025 * b.wheelDeltaX)) : f.deltaY = b.detail;
                    return g.call(d, f);
                }, f);
            }
            function t(d, c, g) {
                var f, e;
                0 == d.deltaMode ? (f = -Math.floor(d.deltaX * (b.opt.mousescrollstep / 54)), e = -Math.floor(d.deltaY * (b.opt.mousescrollstep / 54))) : 1 == d.deltaMode && (f = -Math.floor(d.deltaX * b.opt.mousescrollstep), 
                e = -Math.floor(d.deltaY * b.opt.mousescrollstep));
                c && (b.opt.oneaxismousemode && 0 == f && e) && (f = e, e = 0);
                f && (b.scrollmom && b.scrollmom.stop(), b.lastdeltax += f, b.debounced("mousewheelx", function() {
                    var d = b.lastdeltax;
                    b.lastdeltax = 0;
                    b.rail.drag || b.doScrollLeftBy(d);
                }, 120));
                if (e) {
                    if (b.opt.nativeparentscrolling && g && !b.ispage && !b.zoomactive) if (0 > e) {
                        if (b.getScrollTop() >= b.page.maxh) return !0;
                    } else if (0 >= b.getScrollTop()) return !0;
                    b.scrollmom && b.scrollmom.stop();
                    b.lastdeltay += e;
                    b.debounced("mousewheely", function() {
                        var d = b.lastdeltay;
                        b.lastdeltay = 0;
                        b.rail.drag || b.doScrollBy(d);
                    }, 120);
                }
                d.stopImmediatePropagation();
                return d.preventDefault();
            }
            var b = this;
            this.version = "3.5.1";
            this.name = "nicescroll";
            this.me = c;
            this.opt = {
                doc: e("body"),
                win: !1
            };
            e.extend(this.opt, I);
            this.opt.snapbackspeed = 80;
            if (k) for (var p in b.opt) "undefined" != typeof k[p] && (b.opt[p] = k[p]);
            this.iddoc = (this.doc = b.opt.doc) && this.doc[0] ? this.doc[0].id || "" : "";
            this.ispage = /BODY|HTML/.test(b.opt.win ? b.opt.win[0].nodeName : this.doc[0].nodeName);
            this.haswrapper = !1 !== b.opt.win;
            this.win = b.opt.win || (this.ispage ? e(window) : this.doc);
            this.docscroll = this.ispage && !this.haswrapper ? e(window) : this.win;
            this.body = e("body");
            this.iframe = this.isfixed = this.viewport = !1;
            this.isiframe = "IFRAME" == this.doc[0].nodeName && "IFRAME" == this.win[0].nodeName;
            this.istextarea = "TEXTAREA" == this.win[0].nodeName;
            this.forcescreen = !1;
            this.canshowonmouseevent = "scroll" != b.opt.autohidemode;
            this.page = this.view = this.onzoomout = this.onzoomin = this.onscrollcancel = this.onscrollend = this.onscrollstart = this.onclick = this.ongesturezoom = this.onkeypress = this.onmousewheel = this.onmousemove = this.onmouseup = this.onmousedown = !1;
            this.scroll = {
                x: 0,
                y: 0
            };
            this.scrollratio = {
                x: 0,
                y: 0
            };
            this.cursorheight = 20;
            this.scrollvaluemax = 0;
            this.observerremover = this.observer = this.scrollmom = this.scrollrunning = this.checkrtlmode = !1;
            do this.id = "ascrail" + M++; while (document.getElementById(this.id));
            this.hasmousefocus = this.hasfocus = this.zoomactive = this.zoom = this.selectiondrag = this.cursorfreezed = this.cursor = this.rail = !1;
            this.visibility = !0;
            this.hidden = this.locked = !1;
            this.cursoractive = !0;
            this.overflowx = b.opt.overflowx;
            this.overflowy = b.opt.overflowy;
            this.nativescrollingarea = !1;
            this.checkarea = 0;
            this.events = [];
            this.saved = {};
            this.delaylist = {};
            this.synclist = {};
            this.lastdeltay = this.lastdeltax = 0;
            this.detected = P();
            var f = e.extend({}, this.detected);
            this.ishwscroll = (this.canhwscroll = f.hastransform && b.opt.hwacceleration) && b.haswrapper;
            this.istouchcapable = !1;
            f.cantouch && (f.ischrome && !f.isios && !f.isandroid) && (this.istouchcapable = !0, 
            f.cantouch = !1);
            f.cantouch && (f.ismozilla && !f.isios && !f.isandroid) && (this.istouchcapable = !0, 
            f.cantouch = !1);
            b.opt.enablemouselockapi || (f.hasmousecapture = !1, f.haspointerlock = !1);
            this.delayed = function(d, c, g, f) {
                var e = b.delaylist[d], h = new Date().getTime();
                if (!f && e && e.tt) return !1;
                e && e.tt && clearTimeout(e.tt);
                if (e && e.last + g > h && !e.tt) b.delaylist[d] = {
                    last: h + g,
                    tt: setTimeout(function() {
                        b.delaylist[d].tt = 0;
                        c.call();
                    }, g)
                }; else if (!e || !e.tt) b.delaylist[d] = {
                    last: h,
                    tt: 0
                }, setTimeout(function() {
                    c.call();
                }, 0);
            };
            this.debounced = function(d, c, g) {
                var f = b.delaylist[d];
                new Date().getTime();
                b.delaylist[d] = c;
                f || setTimeout(function() {
                    var c = b.delaylist[d];
                    b.delaylist[d] = !1;
                    c.call();
                }, g);
            };
            this.synched = function(d, c) {
                b.synclist[d] = c;
                (function() {
                    b.onsync || (v(function() {
                        b.onsync = !1;
                        for (d in b.synclist) {
                            var c = b.synclist[d];
                            c && c.call(b);
                            b.synclist[d] = !1;
                        }
                    }), b.onsync = !0);
                })();
                return d;
            };
            this.unsynched = function(d) {
                b.synclist[d] && (b.synclist[d] = !1);
            };
            this.css = function(d, c) {
                for (var g in c) b.saved.css.push([ d, g, d.css(g) ]), d.css(g, c[g]);
            };
            this.scrollTop = function(d) {
                return "undefined" == typeof d ? b.getScrollTop() : b.setScrollTop(d);
            };
            this.scrollLeft = function(d) {
                return "undefined" == typeof d ? b.getScrollLeft() : b.setScrollLeft(d);
            };
            BezierClass = function(b, c, g, f, e, h, l) {
                this.st = b;
                this.ed = c;
                this.spd = g;
                this.p1 = f || 0;
                this.p2 = e || 1;
                this.p3 = h || 0;
                this.p4 = l || 1;
                this.ts = new Date().getTime();
                this.df = this.ed - this.st;
            };
            BezierClass.prototype = {
                B2: function(b) {
                    return 3 * b * b * (1 - b);
                },
                B3: function(b) {
                    return 3 * b * (1 - b) * (1 - b);
                },
                B4: function(b) {
                    return (1 - b) * (1 - b) * (1 - b);
                },
                getNow: function() {
                    var b = 1 - (new Date().getTime() - this.ts) / this.spd, c = this.B2(b) + this.B3(b) + this.B4(b);
                    return 0 > b ? this.ed : this.st + Math.round(this.df * c);
                },
                update: function(b, c) {
                    this.st = this.getNow();
                    this.ed = b;
                    this.spd = c;
                    this.ts = new Date().getTime();
                    this.df = this.ed - this.st;
                    return this;
                }
            };
            if (this.ishwscroll) {
                this.doc.translate = {
                    x: 0,
                    y: 0,
                    tx: "0px",
                    ty: "0px"
                };
                f.hastranslate3d && f.isios && this.doc.css("-webkit-backface-visibility", "hidden");
                var s = function() {
                    var d = b.doc.css(f.trstyle);
                    return d && "matrix" == d.substr(0, 6) ? d.replace(/^.*\((.*)\)$/g, "$1").replace(/px/g, "").split(/, +/) : !1;
                };
                this.getScrollTop = function(d) {
                    if (!d) {
                        if (d = s()) return 16 == d.length ? -d[13] : -d[5];
                        if (b.timerscroll && b.timerscroll.bz) return b.timerscroll.bz.getNow();
                    }
                    return b.doc.translate.y;
                };
                this.getScrollLeft = function(d) {
                    if (!d) {
                        if (d = s()) return 16 == d.length ? -d[12] : -d[4];
                        if (b.timerscroll && b.timerscroll.bh) return b.timerscroll.bh.getNow();
                    }
                    return b.doc.translate.x;
                };
                this.notifyScrollEvent = document.createEvent ? function(b) {
                    var c = document.createEvent("UIEvents");
                    c.initUIEvent("scroll", !1, !0, window, 1);
                    b.dispatchEvent(c);
                } : document.fireEvent ? function(b) {
                    var c = document.createEventObject();
                    b.fireEvent("onscroll");
                    c.cancelBubble = !0;
                } : function(b, c) {};
                f.hastranslate3d && b.opt.enabletranslate3d ? (this.setScrollTop = function(d, c) {
                    b.doc.translate.y = d;
                    b.doc.translate.ty = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate3d(" + b.doc.translate.tx + "," + b.doc.translate.ty + ",0px)");
                    c || b.notifyScrollEvent(b.win[0]);
                }, this.setScrollLeft = function(d, c) {
                    b.doc.translate.x = d;
                    b.doc.translate.tx = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate3d(" + b.doc.translate.tx + "," + b.doc.translate.ty + ",0px)");
                    c || b.notifyScrollEvent(b.win[0]);
                }) : (this.setScrollTop = function(d, c) {
                    b.doc.translate.y = d;
                    b.doc.translate.ty = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate(" + b.doc.translate.tx + "," + b.doc.translate.ty + ")");
                    c || b.notifyScrollEvent(b.win[0]);
                }, this.setScrollLeft = function(d, c) {
                    b.doc.translate.x = d;
                    b.doc.translate.tx = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate(" + b.doc.translate.tx + "," + b.doc.translate.ty + ")");
                    c || b.notifyScrollEvent(b.win[0]);
                });
            } else this.getScrollTop = function() {
                return b.docscroll.scrollTop();
            }, this.setScrollTop = function(d) {
                return b.docscroll.scrollTop(d);
            }, this.getScrollLeft = function() {
                return b.docscroll.scrollLeft();
            }, this.setScrollLeft = function(d) {
                return b.docscroll.scrollLeft(d);
            };
            this.getTarget = function(b) {
                return !b ? !1 : b.target ? b.target : b.srcElement ? b.srcElement : !1;
            };
            this.hasParent = function(b, c) {
                if (!b) return !1;
                for (var g = b.target || b.srcElement || b || !1; g && g.id != c; ) g = g.parentNode || !1;
                return !1 !== g;
            };
            var u = {
                thin: 1,
                medium: 3,
                thick: 5
            };
            this.getOffset = function() {
                if (b.isfixed) return {
                    top: parseFloat(b.win.css("top")),
                    left: parseFloat(b.win.css("left"))
                };
                if (!b.viewport) return b.win.offset();
                var d = b.win.offset(), c = b.viewport.offset();
                return {
                    top: d.top - c.top + b.viewport.scrollTop(),
                    left: d.left - c.left + b.viewport.scrollLeft()
                };
            };
            this.updateScrollBar = function(d) {
                if (b.ishwscroll) b.rail.css({
                    height: b.win.innerHeight()
                }), b.railh && b.railh.css({
                    width: b.win.innerWidth()
                }); else {
                    var c = b.getOffset(), g = c.top, f = c.left, g = g + l(b.win, "border-top-width", !0);
                    b.win.outerWidth();
                    b.win.innerWidth();
                    var f = f + (b.rail.align ? b.win.outerWidth() - l(b.win, "border-right-width") - b.rail.width : l(b.win, "border-left-width")), e = b.opt.railoffset;
                    e && (e.top && (g += e.top), b.rail.align && e.left && (f += e.left));
                    b.locked || b.rail.css({
                        top: g,
                        left: f,
                        height: d ? d.h : b.win.innerHeight()
                    });
                    b.zoom && b.zoom.css({
                        top: g + 1,
                        left: 1 == b.rail.align ? f - 20 : f + b.rail.width + 4
                    });
                    b.railh && !b.locked && (g = c.top, f = c.left, d = b.railh.align ? g + l(b.win, "border-top-width", !0) + b.win.innerHeight() - b.railh.height : g + l(b.win, "border-top-width", !0), 
                    f += l(b.win, "border-left-width"), b.railh.css({
                        top: d,
                        left: f,
                        width: b.railh.width
                    }));
                }
            };
            this.doRailClick = function(d, c, g) {
                var f;
                b.locked || (b.cancelEvent(d), c ? (c = g ? b.doScrollLeft : b.doScrollTop, f = g ? (d.pageX - b.railh.offset().left - b.cursorwidth / 2) * b.scrollratio.x : (d.pageY - b.rail.offset().top - b.cursorheight / 2) * b.scrollratio.y, 
                c(f)) : (c = g ? b.doScrollLeftBy : b.doScrollBy, f = g ? b.scroll.x : b.scroll.y, 
                d = g ? d.pageX - b.railh.offset().left : d.pageY - b.rail.offset().top, g = g ? b.view.w : b.view.h, 
                f >= d ? c(g) : c(-g)));
            };
            b.hasanimationframe = v;
            b.hascancelanimationframe = w;
            b.hasanimationframe ? b.hascancelanimationframe || (w = function() {
                b.cancelAnimationFrame = !0;
            }) : (v = function(b) {
                return setTimeout(b, 15 - Math.floor(+new Date() / 1e3) % 16);
            }, w = clearInterval);
            this.init = function() {
                b.saved.css = [];
                if (f.isie7mobile || f.isoperamini) return !0;
                f.hasmstouch && b.css(b.ispage ? e("html") : b.win, {
                    "-ms-touch-action": "none"
                });
                b.zindex = "auto";
                b.zindex = !b.ispage && "auto" == b.opt.zindex ? h() || "auto" : b.opt.zindex;
                !b.ispage && "auto" != b.zindex && b.zindex > y && (y = b.zindex);
                b.isie && (0 == b.zindex && "auto" == b.opt.zindex) && (b.zindex = "auto");
                if (!b.ispage || !f.cantouch && !f.isieold && !f.isie9mobile) {
                    var d = b.docscroll;
                    b.ispage && (d = b.haswrapper ? b.win : b.doc);
                    f.isie9mobile || b.css(d, {
                        "overflow-y": "hidden"
                    });
                    b.ispage && f.isie7 && ("BODY" == b.doc[0].nodeName ? b.css(e("html"), {
                        "overflow-y": "hidden"
                    }) : "HTML" == b.doc[0].nodeName && b.css(e("body"), {
                        "overflow-y": "hidden"
                    }));
                    f.isios && (!b.ispage && !b.haswrapper) && b.css(e("body"), {
                        "-webkit-overflow-scrolling": "touch"
                    });
                    var c = e(document.createElement("div"));
                    c.css({
                        position: "relative",
                        top: 0,
                        "float": "right",
                        width: b.opt.cursorwidth,
                        height: "0px",
                        "background-color": b.opt.cursorcolor,
                        border: b.opt.cursorborder,
                        "background-clip": "padding-box",
                        "-webkit-border-radius": b.opt.cursorborderradius,
                        "-moz-border-radius": b.opt.cursorborderradius,
                        "border-radius": b.opt.cursorborderradius
                    });
                    c.hborder = parseFloat(c.outerHeight() - c.innerHeight());
                    b.cursor = c;
                    var g = e(document.createElement("div"));
                    g.attr("id", b.id);
                    g.addClass("nicescroll-rails");
                    var l, k, x = [ "left", "right" ], q;
                    for (q in x) k = x[q], (l = b.opt.railpadding[k]) ? g.css("padding-" + k, l + "px") : b.opt.railpadding[k] = 0;
                    g.append(c);
                    g.width = Math.max(parseFloat(b.opt.cursorwidth), c.outerWidth()) + b.opt.railpadding.left + b.opt.railpadding.right;
                    g.css({
                        width: g.width + "px",
                        zIndex: b.zindex,
                        background: b.opt.background,
                        cursor: "default"
                    });
                    g.visibility = !0;
                    g.scrollable = !0;
                    g.align = "left" == b.opt.railalign ? 0 : 1;
                    b.rail = g;
                    c = b.rail.drag = !1;
                    b.opt.boxzoom && (!b.ispage && !f.isieold) && (c = document.createElement("div"), 
                    b.bind(c, "click", b.doZoom), b.zoom = e(c), b.zoom.css({
                        cursor: "pointer",
                        "z-index": b.zindex,
                        backgroundImage: "url(" + N + "zoomico.png)",
                        height: 18,
                        width: 18,
                        backgroundPosition: "0px 0px"
                    }), b.opt.dblclickzoom && b.bind(b.win, "dblclick", b.doZoom), f.cantouch && b.opt.gesturezoom && (b.ongesturezoom = function(d) {
                        1.5 < d.scale && b.doZoomIn(d);
                        .8 > d.scale && b.doZoomOut(d);
                        return b.cancelEvent(d);
                    }, b.bind(b.win, "gestureend", b.ongesturezoom)));
                    b.railh = !1;
                    if (b.opt.horizrailenabled) {
                        b.css(d, {
                            "overflow-x": "hidden"
                        });
                        c = e(document.createElement("div"));
                        c.css({
                            position: "relative",
                            top: 0,
                            height: b.opt.cursorwidth,
                            width: "0px",
                            "background-color": b.opt.cursorcolor,
                            border: b.opt.cursorborder,
                            "background-clip": "padding-box",
                            "-webkit-border-radius": b.opt.cursorborderradius,
                            "-moz-border-radius": b.opt.cursorborderradius,
                            "border-radius": b.opt.cursorborderradius
                        });
                        c.wborder = parseFloat(c.outerWidth() - c.innerWidth());
                        b.cursorh = c;
                        var m = e(document.createElement("div"));
                        m.attr("id", b.id + "-hr");
                        m.addClass("nicescroll-rails");
                        m.height = Math.max(parseFloat(b.opt.cursorwidth), c.outerHeight());
                        m.css({
                            height: m.height + "px",
                            zIndex: b.zindex,
                            background: b.opt.background
                        });
                        m.append(c);
                        m.visibility = !0;
                        m.scrollable = !0;
                        m.align = "top" == b.opt.railvalign ? 0 : 1;
                        b.railh = m;
                        b.railh.drag = !1;
                    }
                    b.ispage ? (g.css({
                        position: "fixed",
                        top: "0px",
                        height: "100%"
                    }), g.align ? g.css({
                        right: "0px"
                    }) : g.css({
                        left: "0px"
                    }), b.body.append(g), b.railh && (m.css({
                        position: "fixed",
                        left: "0px",
                        width: "100%"
                    }), m.align ? m.css({
                        bottom: "0px"
                    }) : m.css({
                        top: "0px"
                    }), b.body.append(m))) : (b.ishwscroll ? ("static" == b.win.css("position") && b.css(b.win, {
                        position: "relative"
                    }), d = "HTML" == b.win[0].nodeName ? b.body : b.win, b.zoom && (b.zoom.css({
                        position: "absolute",
                        top: 1,
                        right: 0,
                        "margin-right": g.width + 4
                    }), d.append(b.zoom)), g.css({
                        position: "absolute",
                        top: 0
                    }), g.align ? g.css({
                        right: 0
                    }) : g.css({
                        left: 0
                    }), d.append(g), m && (m.css({
                        position: "absolute",
                        left: 0,
                        bottom: 0
                    }), m.align ? m.css({
                        bottom: 0
                    }) : m.css({
                        top: 0
                    }), d.append(m))) : (b.isfixed = "fixed" == b.win.css("position"), d = b.isfixed ? "fixed" : "absolute", 
                    b.isfixed || (b.viewport = b.getViewport(b.win[0])), b.viewport && (b.body = b.viewport, 
                    !1 == /fixed|relative|absolute/.test(b.viewport.css("position")) && b.css(b.viewport, {
                        position: "relative"
                    })), g.css({
                        position: d
                    }), b.zoom && b.zoom.css({
                        position: d
                    }), b.updateScrollBar(), b.body.append(g), b.zoom && b.body.append(b.zoom), b.railh && (m.css({
                        position: d
                    }), b.body.append(m))), f.isios && b.css(b.win, {
                        "-webkit-tap-highlight-color": "rgba(0,0,0,0)",
                        "-webkit-touch-callout": "none"
                    }), f.isie && b.opt.disableoutline && b.win.attr("hideFocus", "true"), f.iswebkit && b.opt.disableoutline && b.win.css({
                        outline: "none"
                    }));
                    !1 === b.opt.autohidemode ? (b.autohidedom = !1, b.rail.css({
                        opacity: b.opt.cursoropacitymax
                    }), b.railh && b.railh.css({
                        opacity: b.opt.cursoropacitymax
                    })) : !0 === b.opt.autohidemode || "leave" === b.opt.autohidemode ? (b.autohidedom = e().add(b.rail), 
                    f.isie8 && (b.autohidedom = b.autohidedom.add(b.cursor)), b.railh && (b.autohidedom = b.autohidedom.add(b.railh)), 
                    b.railh && f.isie8 && (b.autohidedom = b.autohidedom.add(b.cursorh))) : "scroll" == b.opt.autohidemode ? (b.autohidedom = e().add(b.rail), 
                    b.railh && (b.autohidedom = b.autohidedom.add(b.railh))) : "cursor" == b.opt.autohidemode ? (b.autohidedom = e().add(b.cursor), 
                    b.railh && (b.autohidedom = b.autohidedom.add(b.cursorh))) : "hidden" == b.opt.autohidemode && (b.autohidedom = !1, 
                    b.hide(), b.locked = !1);
                    if (f.isie9mobile) b.scrollmom = new J(b), b.onmangotouch = function(d) {
                        d = b.getScrollTop();
                        var c = b.getScrollLeft();
                        if (d == b.scrollmom.lastscrolly && c == b.scrollmom.lastscrollx) return !0;
                        var g = d - b.mangotouch.sy, f = c - b.mangotouch.sx;
                        if (0 != Math.round(Math.sqrt(Math.pow(f, 2) + Math.pow(g, 2)))) {
                            var n = 0 > g ? -1 : 1, e = 0 > f ? -1 : 1, h = +new Date();
                            b.mangotouch.lazy && clearTimeout(b.mangotouch.lazy);
                            80 < h - b.mangotouch.tm || b.mangotouch.dry != n || b.mangotouch.drx != e ? (b.scrollmom.stop(), 
                            b.scrollmom.reset(c, d), b.mangotouch.sy = d, b.mangotouch.ly = d, b.mangotouch.sx = c, 
                            b.mangotouch.lx = c, b.mangotouch.dry = n, b.mangotouch.drx = e, b.mangotouch.tm = h) : (b.scrollmom.stop(), 
                            b.scrollmom.update(b.mangotouch.sx - f, b.mangotouch.sy - g), b.mangotouch.tm = h, 
                            g = Math.max(Math.abs(b.mangotouch.ly - d), Math.abs(b.mangotouch.lx - c)), b.mangotouch.ly = d, 
                            b.mangotouch.lx = c, 2 < g && (b.mangotouch.lazy = setTimeout(function() {
                                b.mangotouch.lazy = !1;
                                b.mangotouch.dry = 0;
                                b.mangotouch.drx = 0;
                                b.mangotouch.tm = 0;
                                b.scrollmom.doMomentum(30);
                            }, 100)));
                        }
                    }, g = b.getScrollTop(), m = b.getScrollLeft(), b.mangotouch = {
                        sy: g,
                        ly: g,
                        dry: 0,
                        sx: m,
                        lx: m,
                        drx: 0,
                        lazy: !1,
                        tm: 0
                    }, b.bind(b.docscroll, "scroll", b.onmangotouch); else {
                        if (f.cantouch || b.istouchcapable || b.opt.touchbehavior || f.hasmstouch) {
                            b.scrollmom = new J(b);
                            b.ontouchstart = function(d) {
                                if (d.pointerType && 2 != d.pointerType) return !1;
                                b.hasmoving = !1;
                                if (!b.locked) {
                                    if (f.hasmstouch) for (var c = d.target ? d.target : !1; c; ) {
                                        var g = e(c).getNiceScroll();
                                        if (0 < g.length && g[0].me == b.me) break;
                                        if (0 < g.length) return !1;
                                        if ("DIV" == c.nodeName && c.id == b.id) break;
                                        c = c.parentNode ? c.parentNode : !1;
                                    }
                                    b.cancelScroll();
                                    if ((c = b.getTarget(d)) && /INPUT/i.test(c.nodeName) && /range/i.test(c.type)) return b.stopPropagation(d);
                                    !("clientX" in d) && "changedTouches" in d && (d.clientX = d.changedTouches[0].clientX, 
                                    d.clientY = d.changedTouches[0].clientY);
                                    b.forcescreen && (g = d, d = {
                                        original: d.original ? d.original : d
                                    }, d.clientX = g.screenX, d.clientY = g.screenY);
                                    b.rail.drag = {
                                        x: d.clientX,
                                        y: d.clientY,
                                        sx: b.scroll.x,
                                        sy: b.scroll.y,
                                        st: b.getScrollTop(),
                                        sl: b.getScrollLeft(),
                                        pt: 2,
                                        dl: !1
                                    };
                                    if (b.ispage || !b.opt.directionlockdeadzone) b.rail.drag.dl = "f"; else {
                                        var g = e(window).width(), n = e(window).height(), h = Math.max(document.body.scrollWidth, document.documentElement.scrollWidth), l = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight), n = Math.max(0, l - n), g = Math.max(0, h - g);
                                        b.rail.drag.ck = !b.rail.scrollable && b.railh.scrollable ? 0 < n ? "v" : !1 : b.rail.scrollable && !b.railh.scrollable ? 0 < g ? "h" : !1 : !1;
                                        b.rail.drag.ck || (b.rail.drag.dl = "f");
                                    }
                                    b.opt.touchbehavior && (b.isiframe && f.isie) && (g = b.win.position(), b.rail.drag.x += g.left, 
                                    b.rail.drag.y += g.top);
                                    b.hasmoving = !1;
                                    b.lastmouseup = !1;
                                    b.scrollmom.reset(d.clientX, d.clientY);
                                    if (!f.cantouch && !this.istouchcapable && !f.hasmstouch) {
                                        if (!c || !/INPUT|SELECT|TEXTAREA/i.test(c.nodeName)) return !b.ispage && f.hasmousecapture && c.setCapture(), 
                                        b.opt.touchbehavior ? (c.onclick && !c._onclick && (c._onclick = c.onclick, c.onclick = function(d) {
                                            if (b.hasmoving) return !1;
                                            c._onclick.call(this, d);
                                        }), b.cancelEvent(d)) : b.stopPropagation(d);
                                        /SUBMIT|CANCEL|BUTTON/i.test(e(c).attr("type")) && (pc = {
                                            tg: c,
                                            click: !1
                                        }, b.preventclick = pc);
                                    }
                                }
                            };
                            b.ontouchend = function(d) {
                                if (d.pointerType && 2 != d.pointerType) return !1;
                                if (b.rail.drag && 2 == b.rail.drag.pt && (b.scrollmom.doMomentum(), b.rail.drag = !1, 
                                b.hasmoving && (b.lastmouseup = !0, b.hideCursor(), f.hasmousecapture && document.releaseCapture(), 
                                !f.cantouch))) return b.cancelEvent(d);
                            };
                            var t = b.opt.touchbehavior && b.isiframe && !f.hasmousecapture;
                            b.ontouchmove = function(d, c) {
                                if (d.pointerType && 2 != d.pointerType) return !1;
                                if (b.rail.drag && 2 == b.rail.drag.pt) {
                                    if (f.cantouch && "undefined" == typeof d.original) return !0;
                                    b.hasmoving = !0;
                                    b.preventclick && !b.preventclick.click && (b.preventclick.click = b.preventclick.tg.onclick || !1, 
                                    b.preventclick.tg.onclick = b.onpreventclick);
                                    d = e.extend({
                                        original: d
                                    }, d);
                                    "changedTouches" in d && (d.clientX = d.changedTouches[0].clientX, d.clientY = d.changedTouches[0].clientY);
                                    if (b.forcescreen) {
                                        var g = d;
                                        d = {
                                            original: d.original ? d.original : d
                                        };
                                        d.clientX = g.screenX;
                                        d.clientY = g.screenY;
                                    }
                                    g = ofy = 0;
                                    if (t && !c) {
                                        var n = b.win.position(), g = -n.left;
                                        ofy = -n.top;
                                    }
                                    var h = d.clientY + ofy, n = h - b.rail.drag.y, l = d.clientX + g, k = l - b.rail.drag.x, r = b.rail.drag.st - n;
                                    b.ishwscroll && b.opt.bouncescroll ? 0 > r ? r = Math.round(r / 2) : r > b.page.maxh && (r = b.page.maxh + Math.round((r - b.page.maxh) / 2)) : (0 > r && (h = r = 0), 
                                    r > b.page.maxh && (r = b.page.maxh, h = 0));
                                    if (b.railh && b.railh.scrollable) {
                                        var m = b.rail.drag.sl - k;
                                        b.ishwscroll && b.opt.bouncescroll ? 0 > m ? m = Math.round(m / 2) : m > b.page.maxw && (m = b.page.maxw + Math.round((m - b.page.maxw) / 2)) : (0 > m && (l = m = 0), 
                                        m > b.page.maxw && (m = b.page.maxw, l = 0));
                                    }
                                    g = !1;
                                    if (b.rail.drag.dl) g = !0, "v" == b.rail.drag.dl ? m = b.rail.drag.sl : "h" == b.rail.drag.dl && (r = b.rail.drag.st); else {
                                        var n = Math.abs(n), k = Math.abs(k), x = b.opt.directionlockdeadzone;
                                        if ("v" == b.rail.drag.ck) {
                                            if (n > x && k <= .3 * n) return b.rail.drag = !1, !0;
                                            k > x && (b.rail.drag.dl = "f", e("body").scrollTop(e("body").scrollTop()));
                                        } else if ("h" == b.rail.drag.ck) {
                                            if (k > x && n <= .3 * k) return b.rail.drag = !1, !0;
                                            n > x && (b.rail.drag.dl = "f", e("body").scrollLeft(e("body").scrollLeft()));
                                        }
                                    }
                                    b.synched("touchmove", function() {
                                        b.rail.drag && 2 == b.rail.drag.pt && (b.prepareTransition && b.prepareTransition(0), 
                                        b.rail.scrollable && b.setScrollTop(r), b.scrollmom.update(l, h), b.railh && b.railh.scrollable ? (b.setScrollLeft(m), 
                                        b.showCursor(r, m)) : b.showCursor(r), f.isie10 && document.selection.clear());
                                    });
                                    f.ischrome && b.istouchcapable && (g = !1);
                                    if (g) return b.cancelEvent(d);
                                }
                            };
                        }
                        b.onmousedown = function(d, c) {
                            if (!(b.rail.drag && 1 != b.rail.drag.pt)) {
                                if (b.locked) return b.cancelEvent(d);
                                b.cancelScroll();
                                b.rail.drag = {
                                    x: d.clientX,
                                    y: d.clientY,
                                    sx: b.scroll.x,
                                    sy: b.scroll.y,
                                    pt: 1,
                                    hr: !!c
                                };
                                var g = b.getTarget(d);
                                !b.ispage && f.hasmousecapture && g.setCapture();
                                b.isiframe && !f.hasmousecapture && (b.saved.csspointerevents = b.doc.css("pointer-events"), 
                                b.css(b.doc, {
                                    "pointer-events": "none"
                                }));
                                return b.cancelEvent(d);
                            }
                        };
                        b.onmouseup = function(d) {
                            if (b.rail.drag && (f.hasmousecapture && document.releaseCapture(), b.isiframe && !f.hasmousecapture && b.doc.css("pointer-events", b.saved.csspointerevents), 
                            1 == b.rail.drag.pt)) return b.rail.drag = !1, b.cancelEvent(d);
                        };
                        b.onmousemove = function(d) {
                            if (b.rail.drag && 1 == b.rail.drag.pt) {
                                if (f.ischrome && 0 == d.which) return b.onmouseup(d);
                                b.cursorfreezed = !0;
                                if (b.rail.drag.hr) {
                                    b.scroll.x = b.rail.drag.sx + (d.clientX - b.rail.drag.x);
                                    0 > b.scroll.x && (b.scroll.x = 0);
                                    var c = b.scrollvaluemaxw;
                                    b.scroll.x > c && (b.scroll.x = c);
                                } else b.scroll.y = b.rail.drag.sy + (d.clientY - b.rail.drag.y), 0 > b.scroll.y && (b.scroll.y = 0), 
                                c = b.scrollvaluemax, b.scroll.y > c && (b.scroll.y = c);
                                b.synched("mousemove", function() {
                                    b.rail.drag && 1 == b.rail.drag.pt && (b.showCursor(), b.rail.drag.hr ? b.doScrollLeft(Math.round(b.scroll.x * b.scrollratio.x), b.opt.cursordragspeed) : b.doScrollTop(Math.round(b.scroll.y * b.scrollratio.y), b.opt.cursordragspeed));
                                });
                                return b.cancelEvent(d);
                            }
                        };
                        if (f.cantouch || b.opt.touchbehavior) b.onpreventclick = function(d) {
                            if (b.preventclick) return b.preventclick.tg.onclick = b.preventclick.click, b.preventclick = !1, 
                            b.cancelEvent(d);
                        }, b.bind(b.win, "mousedown", b.ontouchstart), b.onclick = f.isios ? !1 : function(d) {
                            return b.lastmouseup ? (b.lastmouseup = !1, b.cancelEvent(d)) : !0;
                        }, b.opt.grabcursorenabled && f.cursorgrabvalue && (b.css(b.ispage ? b.doc : b.win, {
                            cursor: f.cursorgrabvalue
                        }), b.css(b.rail, {
                            cursor: f.cursorgrabvalue
                        })); else {
                            var p = function(d) {
                                if (b.selectiondrag) {
                                    if (d) {
                                        var c = b.win.outerHeight();
                                        d = d.pageY - b.selectiondrag.top;
                                        0 < d && d < c && (d = 0);
                                        d >= c && (d -= c);
                                        b.selectiondrag.df = d;
                                    }
                                    0 != b.selectiondrag.df && (b.doScrollBy(2 * -Math.floor(b.selectiondrag.df / 6)), 
                                    b.debounced("doselectionscroll", function() {
                                        p();
                                    }, 50));
                                }
                            };
                            b.hasTextSelected = "getSelection" in document ? function() {
                                return 0 < document.getSelection().rangeCount;
                            } : "selection" in document ? function() {
                                return "None" != document.selection.type;
                            } : function() {
                                return !1;
                            };
                            b.onselectionstart = function(d) {
                                b.ispage || (b.selectiondrag = b.win.offset());
                            };
                            b.onselectionend = function(d) {
                                b.selectiondrag = !1;
                            };
                            b.onselectiondrag = function(d) {
                                b.selectiondrag && b.hasTextSelected() && b.debounced("selectionscroll", function() {
                                    p(d);
                                }, 250);
                            };
                        }
                        f.hasmstouch && (b.css(b.rail, {
                            "-ms-touch-action": "none"
                        }), b.css(b.cursor, {
                            "-ms-touch-action": "none"
                        }), b.bind(b.win, "MSPointerDown", b.ontouchstart), b.bind(document, "MSPointerUp", b.ontouchend), 
                        b.bind(document, "MSPointerMove", b.ontouchmove), b.bind(b.cursor, "MSGestureHold", function(b) {
                            b.preventDefault();
                        }), b.bind(b.cursor, "contextmenu", function(b) {
                            b.preventDefault();
                        }));
                        this.istouchcapable && (b.bind(b.win, "touchstart", b.ontouchstart), b.bind(document, "touchend", b.ontouchend), 
                        b.bind(document, "touchcancel", b.ontouchend), b.bind(document, "touchmove", b.ontouchmove));
                        b.bind(b.cursor, "mousedown", b.onmousedown);
                        b.bind(b.cursor, "mouseup", b.onmouseup);
                        b.railh && (b.bind(b.cursorh, "mousedown", function(d) {
                            b.onmousedown(d, !0);
                        }), b.bind(b.cursorh, "mouseup", function(d) {
                            if (!(b.rail.drag && 2 == b.rail.drag.pt)) return b.rail.drag = !1, b.hasmoving = !1, 
                            b.hideCursor(), f.hasmousecapture && document.releaseCapture(), b.cancelEvent(d);
                        }));
                        if (b.opt.cursordragontouch || !f.cantouch && !b.opt.touchbehavior) b.rail.css({
                            cursor: "default"
                        }), b.railh && b.railh.css({
                            cursor: "default"
                        }), b.jqbind(b.rail, "mouseenter", function() {
                            b.canshowonmouseevent && b.showCursor();
                            b.rail.active = !0;
                        }), b.jqbind(b.rail, "mouseleave", function() {
                            b.rail.active = !1;
                            b.rail.drag || b.hideCursor();
                        }), b.opt.sensitiverail && (b.bind(b.rail, "click", function(d) {
                            b.doRailClick(d, !1, !1);
                        }), b.bind(b.rail, "dblclick", function(d) {
                            b.doRailClick(d, !0, !1);
                        }), b.bind(b.cursor, "click", function(d) {
                            b.cancelEvent(d);
                        }), b.bind(b.cursor, "dblclick", function(d) {
                            b.cancelEvent(d);
                        })), b.railh && (b.jqbind(b.railh, "mouseenter", function() {
                            b.canshowonmouseevent && b.showCursor();
                            b.rail.active = !0;
                        }), b.jqbind(b.railh, "mouseleave", function() {
                            b.rail.active = !1;
                            b.rail.drag || b.hideCursor();
                        }), b.opt.sensitiverail && (b.bind(b.railh, "click", function(d) {
                            b.doRailClick(d, !1, !0);
                        }), b.bind(b.railh, "dblclick", function(d) {
                            b.doRailClick(d, !0, !0);
                        }), b.bind(b.cursorh, "click", function(d) {
                            b.cancelEvent(d);
                        }), b.bind(b.cursorh, "dblclick", function(d) {
                            b.cancelEvent(d);
                        })));
                        !f.cantouch && !b.opt.touchbehavior ? (b.bind(f.hasmousecapture ? b.win : document, "mouseup", b.onmouseup), 
                        b.bind(document, "mousemove", b.onmousemove), b.onclick && b.bind(document, "click", b.onclick), 
                        !b.ispage && b.opt.enablescrollonselection && (b.bind(b.win[0], "mousedown", b.onselectionstart), 
                        b.bind(document, "mouseup", b.onselectionend), b.bind(b.cursor, "mouseup", b.onselectionend), 
                        b.cursorh && b.bind(b.cursorh, "mouseup", b.onselectionend), b.bind(document, "mousemove", b.onselectiondrag)), 
                        b.zoom && (b.jqbind(b.zoom, "mouseenter", function() {
                            b.canshowonmouseevent && b.showCursor();
                            b.rail.active = !0;
                        }), b.jqbind(b.zoom, "mouseleave", function() {
                            b.rail.active = !1;
                            b.rail.drag || b.hideCursor();
                        }))) : (b.bind(f.hasmousecapture ? b.win : document, "mouseup", b.ontouchend), b.bind(document, "mousemove", b.ontouchmove), 
                        b.onclick && b.bind(document, "click", b.onclick), b.opt.cursordragontouch && (b.bind(b.cursor, "mousedown", b.onmousedown), 
                        b.bind(b.cursor, "mousemove", b.onmousemove), b.cursorh && b.bind(b.cursorh, "mousedown", function(d) {
                            b.onmousedown(d, !0);
                        }), b.cursorh && b.bind(b.cursorh, "mousemove", b.onmousemove)));
                        b.opt.enablemousewheel && (b.isiframe || b.bind(f.isie && b.ispage ? document : b.win, "mousewheel", b.onmousewheel), 
                        b.bind(b.rail, "mousewheel", b.onmousewheel), b.railh && b.bind(b.railh, "mousewheel", b.onmousewheelhr));
                        !b.ispage && (!f.cantouch && !/HTML|BODY/.test(b.win[0].nodeName)) && (b.win.attr("tabindex") || b.win.attr({
                            tabindex: L++
                        }), b.jqbind(b.win, "focus", function(d) {
                            z = b.getTarget(d).id || !0;
                            b.hasfocus = !0;
                            b.canshowonmouseevent && b.noticeCursor();
                        }), b.jqbind(b.win, "blur", function(d) {
                            z = !1;
                            b.hasfocus = !1;
                        }), b.jqbind(b.win, "mouseenter", function(d) {
                            E = b.getTarget(d).id || !0;
                            b.hasmousefocus = !0;
                            b.canshowonmouseevent && b.noticeCursor();
                        }), b.jqbind(b.win, "mouseleave", function() {
                            E = !1;
                            b.hasmousefocus = !1;
                            b.rail.drag || b.hideCursor();
                        }));
                    }
                    b.onkeypress = function(d) {
                        if (b.locked && 0 == b.page.maxh) return !0;
                        d = d ? d : window.e;
                        var c = b.getTarget(d);
                        if (c && /INPUT|TEXTAREA|SELECT|OPTION/.test(c.nodeName) && (!c.getAttribute("type") && !c.type || !/submit|button|cancel/i.tp)) return !0;
                        if (b.hasfocus || b.hasmousefocus && !z || b.ispage && !z && !E) {
                            c = d.keyCode;
                            if (b.locked && 27 != c) return b.cancelEvent(d);
                            var g = d.ctrlKey || !1, n = d.shiftKey || !1, f = !1;
                            switch (c) {
                              case 38:
                              case 63233:
                                b.doScrollBy(72);
                                f = !0;
                                break;

                              case 40:
                              case 63235:
                                b.doScrollBy(-72);
                                f = !0;
                                break;

                              case 37:
                              case 63232:
                                b.railh && (g ? b.doScrollLeft(0) : b.doScrollLeftBy(72), f = !0);
                                break;

                              case 39:
                              case 63234:
                                b.railh && (g ? b.doScrollLeft(b.page.maxw) : b.doScrollLeftBy(-72), f = !0);
                                break;

                              case 33:
                              case 63276:
                                b.doScrollBy(b.view.h);
                                f = !0;
                                break;

                              case 34:
                              case 63277:
                                b.doScrollBy(-b.view.h);
                                f = !0;
                                break;

                              case 36:
                              case 63273:
                                b.railh && g ? b.doScrollPos(0, 0) : b.doScrollTo(0);
                                f = !0;
                                break;

                              case 35:
                              case 63275:
                                b.railh && g ? b.doScrollPos(b.page.maxw, b.page.maxh) : b.doScrollTo(b.page.maxh);
                                f = !0;
                                break;

                              case 32:
                                b.opt.spacebarenabled && (n ? b.doScrollBy(b.view.h) : b.doScrollBy(-b.view.h), 
                                f = !0);
                                break;

                              case 27:
                                b.zoomactive && (b.doZoom(), f = !0);
                            }
                            if (f) return b.cancelEvent(d);
                        }
                    };
                    b.opt.enablekeyboard && b.bind(document, f.isopera && !f.isopera12 ? "keypress" : "keydown", b.onkeypress);
                    b.bind(window, "resize", b.lazyResize);
                    b.bind(window, "orientationchange", b.lazyResize);
                    b.bind(window, "load", b.lazyResize);
                    if (f.ischrome && !b.ispage && !b.haswrapper) {
                        var s = b.win.attr("style"), g = parseFloat(b.win.css("width")) + 1;
                        b.win.css("width", g);
                        b.synched("chromefix", function() {
                            b.win.attr("style", s);
                        });
                    }
                    b.onAttributeChange = function(d) {
                        b.lazyResize(250);
                    };
                    !b.ispage && !b.haswrapper && (!1 !== A ? (b.observer = new A(function(d) {
                        d.forEach(b.onAttributeChange);
                    }), b.observer.observe(b.win[0], {
                        childList: !0,
                        characterData: !1,
                        attributes: !0,
                        subtree: !1
                    }), b.observerremover = new A(function(d) {
                        d.forEach(function(d) {
                            if (0 < d.removedNodes.length) for (var c in d.removedNodes) if (d.removedNodes[c] == b.win[0]) return b.remove();
                        });
                    }), b.observerremover.observe(b.win[0].parentNode, {
                        childList: !0,
                        characterData: !1,
                        attributes: !1,
                        subtree: !1
                    })) : (b.bind(b.win, f.isie && !f.isie9 ? "propertychange" : "DOMAttrModified", b.onAttributeChange), 
                    f.isie9 && b.win[0].attachEvent("onpropertychange", b.onAttributeChange), b.bind(b.win, "DOMNodeRemoved", function(d) {
                        d.target == b.win[0] && b.remove();
                    })));
                    !b.ispage && b.opt.boxzoom && b.bind(window, "resize", b.resizeZoom);
                    b.istextarea && b.bind(b.win, "mouseup", b.lazyResize);
                    b.checkrtlmode = !0;
                    b.lazyResize(30);
                }
                if ("IFRAME" == this.doc[0].nodeName) {
                    var K = function(d) {
                        b.iframexd = !1;
                        try {
                            var c = "contentDocument" in this ? this.contentDocument : this.contentWindow.document;
                        } catch (g) {
                            b.iframexd = !0, c = !1;
                        }
                        if (b.iframexd) return "console" in window && console.log("NiceScroll error: policy restriced iframe"), 
                        !0;
                        b.forcescreen = !0;
                        b.isiframe && (b.iframe = {
                            doc: e(c),
                            html: b.doc.contents().find("html")[0],
                            body: b.doc.contents().find("body")[0]
                        }, b.getContentSize = function() {
                            return {
                                w: Math.max(b.iframe.html.scrollWidth, b.iframe.body.scrollWidth),
                                h: Math.max(b.iframe.html.scrollHeight, b.iframe.body.scrollHeight)
                            };
                        }, b.docscroll = e(b.iframe.body));
                        !f.isios && (b.opt.iframeautoresize && !b.isiframe) && (b.win.scrollTop(0), b.doc.height(""), 
                        d = Math.max(c.getElementsByTagName("html")[0].scrollHeight, c.body.scrollHeight), 
                        b.doc.height(d));
                        b.lazyResize(30);
                        f.isie7 && b.css(e(b.iframe.html), {
                            "overflow-y": "hidden"
                        });
                        b.css(e(b.iframe.body), {
                            "overflow-y": "hidden"
                        });
                        f.isios && b.haswrapper && b.css(e(c.body), {
                            "-webkit-transform": "translate3d(0,0,0)"
                        });
                        "contentWindow" in this ? b.bind(this.contentWindow, "scroll", b.onscroll) : b.bind(c, "scroll", b.onscroll);
                        b.opt.enablemousewheel && b.bind(c, "mousewheel", b.onmousewheel);
                        b.opt.enablekeyboard && b.bind(c, f.isopera ? "keypress" : "keydown", b.onkeypress);
                        if (f.cantouch || b.opt.touchbehavior) b.bind(c, "mousedown", b.ontouchstart), b.bind(c, "mousemove", function(d) {
                            b.ontouchmove(d, !0);
                        }), b.opt.grabcursorenabled && f.cursorgrabvalue && b.css(e(c.body), {
                            cursor: f.cursorgrabvalue
                        });
                        b.bind(c, "mouseup", b.ontouchend);
                        b.zoom && (b.opt.dblclickzoom && b.bind(c, "dblclick", b.doZoom), b.ongesturezoom && b.bind(c, "gestureend", b.ongesturezoom));
                    };
                    this.doc[0].readyState && "complete" == this.doc[0].readyState && setTimeout(function() {
                        K.call(b.doc[0], !1);
                    }, 500);
                    b.bind(this.doc, "load", K);
                }
            };
            this.showCursor = function(d, c) {
                b.cursortimeout && (clearTimeout(b.cursortimeout), b.cursortimeout = 0);
                if (b.rail) {
                    b.autohidedom && (b.autohidedom.stop().css({
                        opacity: b.opt.cursoropacitymax
                    }), b.cursoractive = !0);
                    if (!b.rail.drag || 1 != b.rail.drag.pt) "undefined" != typeof d && !1 !== d && (b.scroll.y = Math.round(1 * d / b.scrollratio.y)), 
                    "undefined" != typeof c && (b.scroll.x = Math.round(1 * c / b.scrollratio.x));
                    b.cursor.css({
                        height: b.cursorheight,
                        top: b.scroll.y
                    });
                    b.cursorh && (!b.rail.align && b.rail.visibility ? b.cursorh.css({
                        width: b.cursorwidth,
                        left: b.scroll.x + b.rail.width
                    }) : b.cursorh.css({
                        width: b.cursorwidth,
                        left: b.scroll.x
                    }), b.cursoractive = !0);
                    b.zoom && b.zoom.stop().css({
                        opacity: b.opt.cursoropacitymax
                    });
                }
            };
            this.hideCursor = function(d) {
                !b.cursortimeout && (b.rail && b.autohidedom && !(b.hasmousefocus && "leave" == b.opt.autohidemode)) && (b.cursortimeout = setTimeout(function() {
                    if (!b.rail.active || !b.showonmouseevent) b.autohidedom.stop().animate({
                        opacity: b.opt.cursoropacitymin
                    }), b.zoom && b.zoom.stop().animate({
                        opacity: b.opt.cursoropacitymin
                    }), b.cursoractive = !1;
                    b.cursortimeout = 0;
                }, d || b.opt.hidecursordelay));
            };
            this.noticeCursor = function(d, c, g) {
                b.showCursor(c, g);
                b.rail.active || b.hideCursor(d);
            };
            this.getContentSize = b.ispage ? function() {
                return {
                    w: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
                    h: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                };
            } : b.haswrapper ? function() {
                return {
                    w: b.doc.outerWidth() + parseInt(b.win.css("paddingLeft")) + parseInt(b.win.css("paddingRight")),
                    h: b.doc.outerHeight() + parseInt(b.win.css("paddingTop")) + parseInt(b.win.css("paddingBottom"))
                };
            } : function() {
                return {
                    w: b.docscroll[0].scrollWidth,
                    h: b.docscroll[0].scrollHeight
                };
            };
            this.onResize = function(d, c) {
                if (!b || !b.win) return !1;
                if (!b.haswrapper && !b.ispage) {
                    if ("none" == b.win.css("display")) return b.visibility && b.hideRail().hideRailHr(), 
                    !1;
                    !b.hidden && !b.visibility && b.showRail().showRailHr();
                }
                var g = b.page.maxh, f = b.page.maxw, e = b.view.w;
                b.view = {
                    w: b.ispage ? b.win.width() : parseInt(b.win[0].clientWidth),
                    h: b.ispage ? b.win.height() : parseInt(b.win[0].clientHeight)
                };
                b.page = c ? c : b.getContentSize();
                b.page.maxh = Math.max(0, b.page.h - b.view.h);
                b.page.maxw = Math.max(0, b.page.w - b.view.w);
                if (b.page.maxh == g && b.page.maxw == f && b.view.w == e) {
                    if (b.ispage) return b;
                    g = b.win.offset();
                    if (b.lastposition && (f = b.lastposition, f.top == g.top && f.left == g.left)) return b;
                    b.lastposition = g;
                }
                0 == b.page.maxh ? (b.hideRail(), b.scrollvaluemax = 0, b.scroll.y = 0, b.scrollratio.y = 0, 
                b.cursorheight = 0, b.setScrollTop(0), b.rail.scrollable = !1) : b.rail.scrollable = !0;
                0 == b.page.maxw ? (b.hideRailHr(), b.scrollvaluemaxw = 0, b.scroll.x = 0, b.scrollratio.x = 0, 
                b.cursorwidth = 0, b.setScrollLeft(0), b.railh.scrollable = !1) : b.railh.scrollable = !0;
                b.locked = 0 == b.page.maxh && 0 == b.page.maxw;
                if (b.locked) return b.ispage || b.updateScrollBar(b.view), !1;
                !b.hidden && !b.visibility ? b.showRail().showRailHr() : !b.hidden && !b.railh.visibility && b.showRailHr();
                b.istextarea && (b.win.css("resize") && "none" != b.win.css("resize")) && (b.view.h -= 20);
                b.cursorheight = Math.min(b.view.h, Math.round(b.view.h * (b.view.h / b.page.h)));
                b.cursorheight = b.opt.cursorfixedheight ? b.opt.cursorfixedheight : Math.max(b.opt.cursorminheight, b.cursorheight);
                b.cursorwidth = Math.min(b.view.w, Math.round(b.view.w * (b.view.w / b.page.w)));
                b.cursorwidth = b.opt.cursorfixedheight ? b.opt.cursorfixedheight : Math.max(b.opt.cursorminheight, b.cursorwidth);
                b.scrollvaluemax = b.view.h - b.cursorheight - b.cursor.hborder;
                b.railh && (b.railh.width = 0 < b.page.maxh ? b.view.w - b.rail.width : b.view.w, 
                b.scrollvaluemaxw = b.railh.width - b.cursorwidth - b.cursorh.wborder);
                b.checkrtlmode && b.railh && (b.checkrtlmode = !1, b.opt.rtlmode && 0 == b.scroll.x && b.setScrollLeft(b.page.maxw));
                b.ispage || b.updateScrollBar(b.view);
                b.scrollratio = {
                    x: b.page.maxw / b.scrollvaluemaxw,
                    y: b.page.maxh / b.scrollvaluemax
                };
                b.getScrollTop() > b.page.maxh ? b.doScrollTop(b.page.maxh) : (b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y)), 
                b.scroll.x = Math.round(b.getScrollLeft() * (1 / b.scrollratio.x)), b.cursoractive && b.noticeCursor());
                b.scroll.y && 0 == b.getScrollTop() && b.doScrollTo(Math.floor(b.scroll.y * b.scrollratio.y));
                return b;
            };
            this.resize = b.onResize;
            this.lazyResize = function(d) {
                d = isNaN(d) ? 30 : d;
                b.delayed("resize", b.resize, d);
                return b;
            };
            this._bind = function(d, c, g, f) {
                b.events.push({
                    e: d,
                    n: c,
                    f: g,
                    b: f,
                    q: !1
                });
                d.addEventListener ? d.addEventListener(c, g, f || !1) : d.attachEvent ? d.attachEvent("on" + c, g) : d["on" + c] = g;
            };
            this.jqbind = function(d, c, g) {
                b.events.push({
                    e: d,
                    n: c,
                    f: g,
                    q: !0
                });
                e(d).bind(c, g);
            };
            this.bind = function(d, c, g, e) {
                var h = "jquery" in d ? d[0] : d;
                "mousewheel" == c ? "onwheel" in b.win ? b._bind(h, "wheel", g, e || !1) : (d = "undefined" != typeof document.onmousewheel ? "mousewheel" : "DOMMouseScroll", 
                q(h, d, g, e || !1), "DOMMouseScroll" == d && q(h, "MozMousePixelScroll", g, e || !1)) : h.addEventListener ? (f.cantouch && /mouseup|mousedown|mousemove/.test(c) && b._bind(h, "mousedown" == c ? "touchstart" : "mouseup" == c ? "touchend" : "touchmove", function(b) {
                    if (b.touches) {
                        if (2 > b.touches.length) {
                            var d = b.touches.length ? b.touches[0] : b;
                            d.original = b;
                            g.call(this, d);
                        }
                    } else b.changedTouches && (d = b.changedTouches[0], d.original = b, g.call(this, d));
                }, e || !1), b._bind(h, c, g, e || !1), f.cantouch && "mouseup" == c && b._bind(h, "touchcancel", g, e || !1)) : b._bind(h, c, function(d) {
                    if ((d = d || window.event || !1) && d.srcElement) d.target = d.srcElement;
                    "pageY" in d || (d.pageX = d.clientX + document.documentElement.scrollLeft, d.pageY = d.clientY + document.documentElement.scrollTop);
                    return !1 === g.call(h, d) || !1 === e ? b.cancelEvent(d) : !0;
                });
            };
            this._unbind = function(b, c, g, f) {
                b.removeEventListener ? b.removeEventListener(c, g, f) : b.detachEvent ? b.detachEvent("on" + c, g) : b["on" + c] = !1;
            };
            this.unbindAll = function() {
                for (var d = 0; d < b.events.length; d++) {
                    var c = b.events[d];
                    c.q ? c.e.unbind(c.n, c.f) : b._unbind(c.e, c.n, c.f, c.b);
                }
            };
            this.cancelEvent = function(b) {
                b = b.original ? b.original : b ? b : window.event || !1;
                if (!b) return !1;
                b.preventDefault && b.preventDefault();
                b.stopPropagation && b.stopPropagation();
                b.preventManipulation && b.preventManipulation();
                b.cancelBubble = !0;
                b.cancel = !0;
                return b.returnValue = !1;
            };
            this.stopPropagation = function(b) {
                b = b.original ? b.original : b ? b : window.event || !1;
                if (!b) return !1;
                if (b.stopPropagation) return b.stopPropagation();
                b.cancelBubble && (b.cancelBubble = !0);
                return !1;
            };
            this.showRail = function() {
                if (0 != b.page.maxh && (b.ispage || "none" != b.win.css("display"))) b.visibility = !0, 
                b.rail.visibility = !0, b.rail.css("display", "block");
                return b;
            };
            this.showRailHr = function() {
                if (!b.railh) return b;
                if (0 != b.page.maxw && (b.ispage || "none" != b.win.css("display"))) b.railh.visibility = !0, 
                b.railh.css("display", "block");
                return b;
            };
            this.hideRail = function() {
                b.visibility = !1;
                b.rail.visibility = !1;
                b.rail.css("display", "none");
                return b;
            };
            this.hideRailHr = function() {
                if (!b.railh) return b;
                b.railh.visibility = !1;
                b.railh.css("display", "none");
                return b;
            };
            this.show = function() {
                b.hidden = !1;
                b.locked = !1;
                return b.showRail().showRailHr();
            };
            this.hide = function() {
                b.hidden = !0;
                b.locked = !0;
                return b.hideRail().hideRailHr();
            };
            this.toggle = function() {
                return b.hidden ? b.show() : b.hide();
            };
            this.remove = function() {
                b.stop();
                b.cursortimeout && clearTimeout(b.cursortimeout);
                b.doZoomOut();
                b.unbindAll();
                f.isie9 && b.win[0].detachEvent("onpropertychange", b.onAttributeChange);
                !1 !== b.observer && b.observer.disconnect();
                !1 !== b.observerremover && b.observerremover.disconnect();
                b.events = null;
                b.cursor && b.cursor.remove();
                b.cursorh && b.cursorh.remove();
                b.rail && b.rail.remove();
                b.railh && b.railh.remove();
                b.zoom && b.zoom.remove();
                for (var d = 0; d < b.saved.css.length; d++) {
                    var c = b.saved.css[d];
                    c[0].css(c[1], "undefined" == typeof c[2] ? "" : c[2]);
                }
                b.saved = !1;
                b.me.data("__nicescroll", "");
                var g = e.nicescroll;
                g.each(function(d) {
                    if (this && this.id === b.id) {
                        delete g[d];
                        for (var c = ++d; c < g.length; c++, d++) g[d] = g[c];
                        g.length--;
                        g.length && delete g[g.length];
                    }
                });
                for (var h in b) b[h] = null, delete b[h];
                b = null;
            };
            this.scrollstart = function(d) {
                this.onscrollstart = d;
                return b;
            };
            this.scrollend = function(d) {
                this.onscrollend = d;
                return b;
            };
            this.scrollcancel = function(d) {
                this.onscrollcancel = d;
                return b;
            };
            this.zoomin = function(d) {
                this.onzoomin = d;
                return b;
            };
            this.zoomout = function(d) {
                this.onzoomout = d;
                return b;
            };
            this.isScrollable = function(b) {
                b = b.target ? b.target : b;
                if ("OPTION" == b.nodeName) return !0;
                for (;b && 1 == b.nodeType && !/BODY|HTML/.test(b.nodeName); ) {
                    var c = e(b), c = c.css("overflowY") || c.css("overflowX") || c.css("overflow") || "";
                    if (/scroll|auto/.test(c)) return b.clientHeight != b.scrollHeight;
                    b = b.parentNode ? b.parentNode : !1;
                }
                return !1;
            };
            this.getViewport = function(b) {
                for (b = b && b.parentNode ? b.parentNode : !1; b && 1 == b.nodeType && !/BODY|HTML/.test(b.nodeName); ) {
                    var c = e(b);
                    if (/fixed|absolute/.test(c.css("position"))) return c;
                    var g = c.css("overflowY") || c.css("overflowX") || c.css("overflow") || "";
                    if (/scroll|auto/.test(g) && b.clientHeight != b.scrollHeight || 0 < c.getNiceScroll().length) return c;
                    b = b.parentNode ? b.parentNode : !1;
                }
                return !1;
            };
            this.onmousewheel = function(d) {
                if (b.locked) return b.debounced("checkunlock", b.resize, 250), !0;
                if (b.rail.drag) return b.cancelEvent(d);
                "auto" == b.opt.oneaxismousemode && 0 != d.deltaX && (b.opt.oneaxismousemode = !1);
                if (b.opt.oneaxismousemode && 0 == d.deltaX && !b.rail.scrollable) return b.railh && b.railh.scrollable ? b.onmousewheelhr(d) : !0;
                var c = +new Date(), g = !1;
                b.opt.preservenativescrolling && b.checkarea + 600 < c && (b.nativescrollingarea = b.isScrollable(d), 
                g = !0);
                b.checkarea = c;
                if (b.nativescrollingarea) return !0;
                if (d = t(d, !1, g)) b.checkarea = 0;
                return d;
            };
            this.onmousewheelhr = function(d) {
                if (b.locked || !b.railh.scrollable) return !0;
                if (b.rail.drag) return b.cancelEvent(d);
                var c = +new Date(), g = !1;
                b.opt.preservenativescrolling && b.checkarea + 600 < c && (b.nativescrollingarea = b.isScrollable(d), 
                g = !0);
                b.checkarea = c;
                return b.nativescrollingarea ? !0 : b.locked ? b.cancelEvent(d) : t(d, !0, g);
            };
            this.stop = function() {
                b.cancelScroll();
                b.scrollmon && b.scrollmon.stop();
                b.cursorfreezed = !1;
                b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y));
                b.noticeCursor();
                return b;
            };
            this.getTransitionSpeed = function(c) {
                var f = Math.round(10 * b.opt.scrollspeed);
                c = Math.min(f, Math.round(c / 20 * b.opt.scrollspeed));
                return 20 < c ? c : 0;
            };
            b.opt.smoothscroll ? b.ishwscroll && f.hastransition && b.opt.usetransition ? (this.prepareTransition = function(c, e) {
                var g = e ? 20 < c ? c : 0 : b.getTransitionSpeed(c), h = g ? f.prefixstyle + "transform " + g + "ms ease-out" : "";
                if (!b.lasttransitionstyle || b.lasttransitionstyle != h) b.lasttransitionstyle = h, 
                b.doc.css(f.transitionstyle, h);
                return g;
            }, this.doScrollLeft = function(c, f) {
                var g = b.scrollrunning ? b.newscrolly : b.getScrollTop();
                b.doScrollPos(c, g, f);
            }, this.doScrollTop = function(c, f) {
                var g = b.scrollrunning ? b.newscrollx : b.getScrollLeft();
                b.doScrollPos(g, c, f);
            }, this.doScrollPos = function(c, e, g) {
                var h = b.getScrollTop(), l = b.getScrollLeft();
                (0 > (b.newscrolly - h) * (e - h) || 0 > (b.newscrollx - l) * (c - l)) && b.cancelScroll();
                !1 == b.opt.bouncescroll && (0 > e ? e = 0 : e > b.page.maxh && (e = b.page.maxh), 
                0 > c ? c = 0 : c > b.page.maxw && (c = b.page.maxw));
                if (b.scrollrunning && c == b.newscrollx && e == b.newscrolly) return !1;
                b.newscrolly = e;
                b.newscrollx = c;
                b.newscrollspeed = g || !1;
                if (b.timer) return !1;
                b.timer = setTimeout(function() {
                    var g = b.getScrollTop(), h = b.getScrollLeft(), l, k;
                    l = c - h;
                    k = e - g;
                    l = Math.round(Math.sqrt(Math.pow(l, 2) + Math.pow(k, 2)));
                    l = b.newscrollspeed && 1 < b.newscrollspeed ? b.newscrollspeed : b.getTransitionSpeed(l);
                    b.newscrollspeed && 1 >= b.newscrollspeed && (l *= b.newscrollspeed);
                    b.prepareTransition(l, !0);
                    b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm);
                    0 < l && (!b.scrollrunning && b.onscrollstart && b.onscrollstart.call(b, {
                        type: "scrollstart",
                        current: {
                            x: h,
                            y: g
                        },
                        request: {
                            x: c,
                            y: e
                        },
                        end: {
                            x: b.newscrollx,
                            y: b.newscrolly
                        },
                        speed: l
                    }), f.transitionend ? b.scrollendtrapped || (b.scrollendtrapped = !0, b.bind(b.doc, f.transitionend, b.onScrollEnd, !1)) : (b.scrollendtrapped && clearTimeout(b.scrollendtrapped), 
                    b.scrollendtrapped = setTimeout(b.onScrollEnd, l)), b.timerscroll = {
                        bz: new BezierClass(g, b.newscrolly, l, 0, 0, .58, 1),
                        bh: new BezierClass(h, b.newscrollx, l, 0, 0, .58, 1)
                    }, b.cursorfreezed || (b.timerscroll.tm = setInterval(function() {
                        b.showCursor(b.getScrollTop(), b.getScrollLeft());
                    }, 60)));
                    b.synched("doScroll-set", function() {
                        b.timer = 0;
                        b.scrollendtrapped && (b.scrollrunning = !0);
                        b.setScrollTop(b.newscrolly);
                        b.setScrollLeft(b.newscrollx);
                        if (!b.scrollendtrapped) b.onScrollEnd();
                    });
                }, 50);
            }, this.cancelScroll = function() {
                if (!b.scrollendtrapped) return !0;
                var c = b.getScrollTop(), e = b.getScrollLeft();
                b.scrollrunning = !1;
                f.transitionend || clearTimeout(f.transitionend);
                b.scrollendtrapped = !1;
                b._unbind(b.doc, f.transitionend, b.onScrollEnd);
                b.prepareTransition(0);
                b.setScrollTop(c);
                b.railh && b.setScrollLeft(e);
                b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm);
                b.timerscroll = !1;
                b.cursorfreezed = !1;
                b.showCursor(c, e);
                return b;
            }, this.onScrollEnd = function() {
                b.scrollendtrapped && b._unbind(b.doc, f.transitionend, b.onScrollEnd);
                b.scrollendtrapped = !1;
                b.prepareTransition(0);
                b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm);
                b.timerscroll = !1;
                var c = b.getScrollTop(), e = b.getScrollLeft();
                b.setScrollTop(c);
                b.railh && b.setScrollLeft(e);
                b.noticeCursor(!1, c, e);
                b.cursorfreezed = !1;
                0 > c ? c = 0 : c > b.page.maxh && (c = b.page.maxh);
                0 > e ? e = 0 : e > b.page.maxw && (e = b.page.maxw);
                if (c != b.newscrolly || e != b.newscrollx) return b.doScrollPos(e, c, b.opt.snapbackspeed);
                b.onscrollend && b.scrollrunning && b.onscrollend.call(b, {
                    type: "scrollend",
                    current: {
                        x: e,
                        y: c
                    },
                    end: {
                        x: b.newscrollx,
                        y: b.newscrolly
                    }
                });
                b.scrollrunning = !1;
            }) : (this.doScrollLeft = function(c, f) {
                var g = b.scrollrunning ? b.newscrolly : b.getScrollTop();
                b.doScrollPos(c, g, f);
            }, this.doScrollTop = function(c, f) {
                var g = b.scrollrunning ? b.newscrollx : b.getScrollLeft();
                b.doScrollPos(g, c, f);
            }, this.doScrollPos = function(c, f, g) {
                function e() {
                    if (b.cancelAnimationFrame) return !0;
                    b.scrollrunning = !0;
                    if (p = 1 - p) return b.timer = v(e) || 1;
                    var c = 0, d = sy = b.getScrollTop();
                    if (b.dst.ay) {
                        var d = b.bzscroll ? b.dst.py + b.bzscroll.getNow() * b.dst.ay : b.newscrolly, g = d - sy;
                        if (0 > g && d < b.newscrolly || 0 < g && d > b.newscrolly) d = b.newscrolly;
                        b.setScrollTop(d);
                        d == b.newscrolly && (c = 1);
                    } else c = 1;
                    var f = sx = b.getScrollLeft();
                    if (b.dst.ax) {
                        f = b.bzscroll ? b.dst.px + b.bzscroll.getNow() * b.dst.ax : b.newscrollx;
                        g = f - sx;
                        if (0 > g && f < b.newscrollx || 0 < g && f > b.newscrollx) f = b.newscrollx;
                        b.setScrollLeft(f);
                        f == b.newscrollx && (c += 1);
                    } else c += 1;
                    2 == c ? (b.timer = 0, b.cursorfreezed = !1, b.bzscroll = !1, b.scrollrunning = !1, 
                    0 > d ? d = 0 : d > b.page.maxh && (d = b.page.maxh), 0 > f ? f = 0 : f > b.page.maxw && (f = b.page.maxw), 
                    f != b.newscrollx || d != b.newscrolly ? b.doScrollPos(f, d) : b.onscrollend && b.onscrollend.call(b, {
                        type: "scrollend",
                        current: {
                            x: sx,
                            y: sy
                        },
                        end: {
                            x: b.newscrollx,
                            y: b.newscrolly
                        }
                    })) : b.timer = v(e) || 1;
                }
                f = "undefined" == typeof f || !1 === f ? b.getScrollTop(!0) : f;
                if (b.timer && b.newscrolly == f && b.newscrollx == c) return !0;
                b.timer && w(b.timer);
                b.timer = 0;
                var h = b.getScrollTop(), l = b.getScrollLeft();
                (0 > (b.newscrolly - h) * (f - h) || 0 > (b.newscrollx - l) * (c - l)) && b.cancelScroll();
                b.newscrolly = f;
                b.newscrollx = c;
                if (!b.bouncescroll || !b.rail.visibility) 0 > b.newscrolly ? b.newscrolly = 0 : b.newscrolly > b.page.maxh && (b.newscrolly = b.page.maxh);
                if (!b.bouncescroll || !b.railh.visibility) 0 > b.newscrollx ? b.newscrollx = 0 : b.newscrollx > b.page.maxw && (b.newscrollx = b.page.maxw);
                b.dst = {};
                b.dst.x = c - l;
                b.dst.y = f - h;
                b.dst.px = l;
                b.dst.py = h;
                var k = Math.round(Math.sqrt(Math.pow(b.dst.x, 2) + Math.pow(b.dst.y, 2)));
                b.dst.ax = b.dst.x / k;
                b.dst.ay = b.dst.y / k;
                var m = 0, q = k;
                0 == b.dst.x ? (m = h, q = f, b.dst.ay = 1, b.dst.py = 0) : 0 == b.dst.y && (m = l, 
                q = c, b.dst.ax = 1, b.dst.px = 0);
                k = b.getTransitionSpeed(k);
                g && 1 >= g && (k *= g);
                b.bzscroll = 0 < k ? b.bzscroll ? b.bzscroll.update(q, k) : new BezierClass(m, q, k, 0, 1, 0, 1) : !1;
                if (!b.timer) {
                    (h == b.page.maxh && f >= b.page.maxh || l == b.page.maxw && c >= b.page.maxw) && b.checkContentSize();
                    var p = 1;
                    b.cancelAnimationFrame = !1;
                    b.timer = 1;
                    b.onscrollstart && !b.scrollrunning && b.onscrollstart.call(b, {
                        type: "scrollstart",
                        current: {
                            x: l,
                            y: h
                        },
                        request: {
                            x: c,
                            y: f
                        },
                        end: {
                            x: b.newscrollx,
                            y: b.newscrolly
                        },
                        speed: k
                    });
                    e();
                    (h == b.page.maxh && f >= h || l == b.page.maxw && c >= l) && b.checkContentSize();
                    b.noticeCursor();
                }
            }, this.cancelScroll = function() {
                b.timer && w(b.timer);
                b.timer = 0;
                b.bzscroll = !1;
                b.scrollrunning = !1;
                return b;
            }) : (this.doScrollLeft = function(c, f) {
                var g = b.getScrollTop();
                b.doScrollPos(c, g, f);
            }, this.doScrollTop = function(c, f) {
                var g = b.getScrollLeft();
                b.doScrollPos(g, c, f);
            }, this.doScrollPos = function(c, f, g) {
                var e = c > b.page.maxw ? b.page.maxw : c;
                0 > e && (e = 0);
                var h = f > b.page.maxh ? b.page.maxh : f;
                0 > h && (h = 0);
                b.synched("scroll", function() {
                    b.setScrollTop(h);
                    b.setScrollLeft(e);
                });
            }, this.cancelScroll = function() {});
            this.doScrollBy = function(c, f) {
                var g = 0, g = f ? Math.floor((b.scroll.y - c) * b.scrollratio.y) : (b.timer ? b.newscrolly : b.getScrollTop(!0)) - c;
                if (b.bouncescroll) {
                    var e = Math.round(b.view.h / 2);
                    g < -e ? g = -e : g > b.page.maxh + e && (g = b.page.maxh + e);
                }
                b.cursorfreezed = !1;
                py = b.getScrollTop(!0);
                if (0 > g && 0 >= py) return b.noticeCursor();
                if (g > b.page.maxh && py >= b.page.maxh) return b.checkContentSize(), b.noticeCursor();
                b.doScrollTop(g);
            };
            this.doScrollLeftBy = function(c, f) {
                var g = 0, g = f ? Math.floor((b.scroll.x - c) * b.scrollratio.x) : (b.timer ? b.newscrollx : b.getScrollLeft(!0)) - c;
                if (b.bouncescroll) {
                    var e = Math.round(b.view.w / 2);
                    g < -e ? g = -e : g > b.page.maxw + e && (g = b.page.maxw + e);
                }
                b.cursorfreezed = !1;
                px = b.getScrollLeft(!0);
                if (0 > g && 0 >= px || g > b.page.maxw && px >= b.page.maxw) return b.noticeCursor();
                b.doScrollLeft(g);
            };
            this.doScrollTo = function(c, f) {
                f && Math.round(c * b.scrollratio.y);
                b.cursorfreezed = !1;
                b.doScrollTop(c);
            };
            this.checkContentSize = function() {
                var c = b.getContentSize();
                (c.h != b.page.h || c.w != b.page.w) && b.resize(!1, c);
            };
            b.onscroll = function(c) {
                b.rail.drag || b.cursorfreezed || b.synched("scroll", function() {
                    b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y));
                    b.railh && (b.scroll.x = Math.round(b.getScrollLeft() * (1 / b.scrollratio.x)));
                    b.noticeCursor();
                });
            };
            b.bind(b.docscroll, "scroll", b.onscroll);
            this.doZoomIn = function(c) {
                if (!b.zoomactive) {
                    b.zoomactive = !0;
                    b.zoomrestore = {
                        style: {}
                    };
                    var h = "position top left zIndex backgroundColor marginTop marginBottom marginLeft marginRight".split(" "), g = b.win[0].style, l;
                    for (l in h) {
                        var k = h[l];
                        b.zoomrestore.style[k] = "undefined" != typeof g[k] ? g[k] : "";
                    }
                    b.zoomrestore.style.width = b.win.css("width");
                    b.zoomrestore.style.height = b.win.css("height");
                    b.zoomrestore.padding = {
                        w: b.win.outerWidth() - b.win.width(),
                        h: b.win.outerHeight() - b.win.height()
                    };
                    f.isios4 && (b.zoomrestore.scrollTop = e(window).scrollTop(), e(window).scrollTop(0));
                    b.win.css({
                        position: f.isios4 ? "absolute" : "fixed",
                        top: 0,
                        left: 0,
                        "z-index": y + 100,
                        margin: "0px"
                    });
                    h = b.win.css("backgroundColor");
                    ("" == h || /transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(h)) && b.win.css("backgroundColor", "#fff");
                    b.rail.css({
                        "z-index": y + 101
                    });
                    b.zoom.css({
                        "z-index": y + 102
                    });
                    b.zoom.css("backgroundPosition", "0px -18px");
                    b.resizeZoom();
                    b.onzoomin && b.onzoomin.call(b);
                    return b.cancelEvent(c);
                }
            };
            this.doZoomOut = function(c) {
                if (b.zoomactive) return b.zoomactive = !1, b.win.css("margin", ""), b.win.css(b.zoomrestore.style), 
                f.isios4 && e(window).scrollTop(b.zoomrestore.scrollTop), b.rail.css({
                    "z-index": b.zindex
                }), b.zoom.css({
                    "z-index": b.zindex
                }), b.zoomrestore = !1, b.zoom.css("backgroundPosition", "0px 0px"), b.onResize(), 
                b.onzoomout && b.onzoomout.call(b), b.cancelEvent(c);
            };
            this.doZoom = function(c) {
                return b.zoomactive ? b.doZoomOut(c) : b.doZoomIn(c);
            };
            this.resizeZoom = function() {
                if (b.zoomactive) {
                    var c = b.getScrollTop();
                    b.win.css({
                        width: e(window).width() - b.zoomrestore.padding.w + "px",
                        height: e(window).height() - b.zoomrestore.padding.h + "px"
                    });
                    b.onResize();
                    b.setScrollTop(Math.min(b.page.maxh, c));
                }
            };
            this.init();
            e.nicescroll.push(this);
        }, J = function(e) {
            var c = this;
            this.nc = e;
            this.steptime = this.lasttime = this.speedy = this.speedx = this.lasty = this.lastx = 0;
            this.snapy = this.snapx = !1;
            this.demuly = this.demulx = 0;
            this.lastscrolly = this.lastscrollx = -1;
            this.timer = this.chky = this.chkx = 0;
            this.time = function() {
                return +new Date();
            };
            this.reset = function(e, l) {
                c.stop();
                var k = c.time();
                c.steptime = 0;
                c.lasttime = k;
                c.speedx = 0;
                c.speedy = 0;
                c.lastx = e;
                c.lasty = l;
                c.lastscrollx = -1;
                c.lastscrolly = -1;
            };
            this.update = function(e, l) {
                var k = c.time();
                c.steptime = k - c.lasttime;
                c.lasttime = k;
                var k = l - c.lasty, t = e - c.lastx, b = c.nc.getScrollTop(), p = c.nc.getScrollLeft(), b = b + k, p = p + t;
                c.snapx = 0 > p || p > c.nc.page.maxw;
                c.snapy = 0 > b || b > c.nc.page.maxh;
                c.speedx = t;
                c.speedy = k;
                c.lastx = e;
                c.lasty = l;
            };
            this.stop = function() {
                c.nc.unsynched("domomentum2d");
                c.timer && clearTimeout(c.timer);
                c.timer = 0;
                c.lastscrollx = -1;
                c.lastscrolly = -1;
            };
            this.doSnapy = function(e, l) {
                var k = !1;
                0 > l ? (l = 0, k = !0) : l > c.nc.page.maxh && (l = c.nc.page.maxh, k = !0);
                0 > e ? (e = 0, k = !0) : e > c.nc.page.maxw && (e = c.nc.page.maxw, k = !0);
                k && c.nc.doScrollPos(e, l, c.nc.opt.snapbackspeed);
            };
            this.doMomentum = function(e) {
                var l = c.time(), k = e ? l + e : c.lasttime;
                e = c.nc.getScrollLeft();
                var t = c.nc.getScrollTop(), b = c.nc.page.maxh, p = c.nc.page.maxw;
                c.speedx = 0 < p ? Math.min(60, c.speedx) : 0;
                c.speedy = 0 < b ? Math.min(60, c.speedy) : 0;
                k = k && 60 >= l - k;
                if (0 > t || t > b || 0 > e || e > p) k = !1;
                e = c.speedx && k ? c.speedx : !1;
                if (c.speedy && k && c.speedy || e) {
                    var f = Math.max(16, c.steptime);
                    50 < f && (e = f / 50, c.speedx *= e, c.speedy *= e, f = 50);
                    c.demulxy = 0;
                    c.lastscrollx = c.nc.getScrollLeft();
                    c.chkx = c.lastscrollx;
                    c.lastscrolly = c.nc.getScrollTop();
                    c.chky = c.lastscrolly;
                    var s = c.lastscrollx, u = c.lastscrolly, d = function() {
                        var e = 600 < c.time() - l ? .04 : .02;
                        if (c.speedx && (s = Math.floor(c.lastscrollx - c.speedx * (1 - c.demulxy)), c.lastscrollx = s, 
                        0 > s || s > p)) e = .1;
                        if (c.speedy && (u = Math.floor(c.lastscrolly - c.speedy * (1 - c.demulxy)), c.lastscrolly = u, 
                        0 > u || u > b)) e = .1;
                        c.demulxy = Math.min(1, c.demulxy + e);
                        c.nc.synched("domomentum2d", function() {
                            c.speedx && (c.nc.getScrollLeft() != c.chkx && c.stop(), c.chkx = s, c.nc.setScrollLeft(s));
                            c.speedy && (c.nc.getScrollTop() != c.chky && c.stop(), c.chky = u, c.nc.setScrollTop(u));
                            c.timer || (c.nc.hideCursor(), c.doSnapy(s, u));
                        });
                        1 > c.demulxy ? c.timer = setTimeout(d, f) : (c.stop(), c.nc.hideCursor(), c.doSnapy(s, u));
                    };
                    d();
                } else c.doSnapy(c.nc.getScrollLeft(), c.nc.getScrollTop());
            };
        }, B = e.fn.scrollTop;
        e.cssHooks.pageYOffset = {
            get: function(k, c, h) {
                return (c = e.data(k, "__nicescroll") || !1) && c.ishwscroll ? c.getScrollTop() : B.call(k);
            },
            set: function(k, c) {
                var h = e.data(k, "__nicescroll") || !1;
                h && h.ishwscroll ? h.setScrollTop(parseInt(c)) : B.call(k, c);
                return this;
            }
        };
        e.fn.scrollTop = function(k) {
            if ("undefined" == typeof k) {
                var c = this[0] ? e.data(this[0], "__nicescroll") || !1 : !1;
                return c && c.ishwscroll ? c.getScrollTop() : B.call(this);
            }
            return this.each(function() {
                var c = e.data(this, "__nicescroll") || !1;
                c && c.ishwscroll ? c.setScrollTop(parseInt(k)) : B.call(e(this), k);
            });
        };
        var C = e.fn.scrollLeft;
        e.cssHooks.pageXOffset = {
            get: function(k, c, h) {
                return (c = e.data(k, "__nicescroll") || !1) && c.ishwscroll ? c.getScrollLeft() : C.call(k);
            },
            set: function(k, c) {
                var h = e.data(k, "__nicescroll") || !1;
                h && h.ishwscroll ? h.setScrollLeft(parseInt(c)) : C.call(k, c);
                return this;
            }
        };
        e.fn.scrollLeft = function(k) {
            if ("undefined" == typeof k) {
                var c = this[0] ? e.data(this[0], "__nicescroll") || !1 : !1;
                return c && c.ishwscroll ? c.getScrollLeft() : C.call(this);
            }
            return this.each(function() {
                var c = e.data(this, "__nicescroll") || !1;
                c && c.ishwscroll ? c.setScrollLeft(parseInt(k)) : C.call(e(this), k);
            });
        };
        var D = function(k) {
            var c = this;
            this.length = 0;
            this.name = "nicescrollarray";
            this.each = function(e) {
                for (var h = 0, k = 0; h < c.length; h++) e.call(c[h], k++);
                return c;
            };
            this.push = function(e) {
                c[c.length] = e;
                c.length++;
            };
            this.eq = function(e) {
                return c[e];
            };
            if (k) for (a = 0; a < k.length; a++) {
                var h = e.data(k[a], "__nicescroll") || !1;
                h && (this[this.length] = h, this.length++);
            }
            return this;
        };
        (function(e, c, h) {
            for (var l = 0; l < c.length; l++) h(e, c[l]);
        })(D.prototype, "show hide toggle onResize resize remove stop doScrollPos".split(" "), function(e, c) {
            e[c] = function() {
                var e = arguments;
                return this.each(function() {
                    this[c].apply(this, e);
                });
            };
        });
        e.fn.getNiceScroll = function(k) {
            return "undefined" == typeof k ? new D(this) : this[k] && e.data(this[k], "__nicescroll") || !1;
        };
        e.extend(e.expr[":"], {
            nicescroll: function(k) {
                return e.data(k, "__nicescroll") ? !0 : !1;
            }
        });
        e.fn.niceScroll = function(k, c) {
            "undefined" == typeof c && ("object" == typeof k && !("jquery" in k)) && (c = k, 
            k = !1);
            var h = new D();
            "undefined" == typeof c && (c = {});
            k && (c.doc = e(k), c.win = e(this));
            var l = !("doc" in c);
            !l && !("win" in c) && (c.win = e(this));
            this.each(function() {
                var k = e(this).data("__nicescroll") || !1;
                k || (c.doc = l ? e(this) : c.doc, k = new Q(c, e(this)), e(this).data("__nicescroll", k));
                h.push(k);
            });
            return 1 == h.length ? h[0] : h;
        };
        window.NiceScroll = {
            getjQuery: function() {
                return e;
            }
        };
        e.nicescroll || (e.nicescroll = new D(), e.nicescroll.options = I);
    })(jQuery);
    (function($, undef) {
        var defaults, gId = 0;
        function initDefaults() {
            if (!defaults) {
                defaults = {
                    verbose: false,
                    queryLimit: {
                        attempt: 5,
                        delay: 250,
                        random: 250
                    },
                    classes: {
                        Map: google.maps.Map,
                        Marker: google.maps.Marker,
                        InfoWindow: google.maps.InfoWindow,
                        Circle: google.maps.Circle,
                        Rectangle: google.maps.Rectangle,
                        OverlayView: google.maps.OverlayView,
                        StreetViewPanorama: google.maps.StreetViewPanorama,
                        KmlLayer: google.maps.KmlLayer,
                        TrafficLayer: google.maps.TrafficLayer,
                        BicyclingLayer: google.maps.BicyclingLayer,
                        GroundOverlay: google.maps.GroundOverlay,
                        StyledMapType: google.maps.StyledMapType,
                        ImageMapType: google.maps.ImageMapType
                    },
                    map: {
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        center: [ 46.578498, 2.457275 ],
                        zoom: 2
                    },
                    overlay: {
                        pane: "floatPane",
                        content: "",
                        offset: {
                            x: 0,
                            y: 0
                        }
                    },
                    geoloc: {
                        getCurrentPosition: {
                            maximumAge: 6e4,
                            timeout: 5e3
                        }
                    }
                };
            }
        }
        function globalId(id, simulate) {
            return id !== undef ? id : "gmap3_" + (simulate ? gId + 1 : ++gId);
        }
        function googleVersionMin(version) {
            var toInt = function(v) {
                return parseInt(v, 10);
            }, gmVersion = google.maps.version.split(".").map(toInt), i;
            version = version.split(".").map(toInt);
            for (i = 0; i < version.length; i++) {
                if (gmVersion.hasOwnProperty(i)) {
                    if (gmVersion[i] < version[i]) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
        function attachEvents($container, args, sender, id, senders) {
            if (args.todo.events || args.todo.onces) {
                var context = {
                    id: id,
                    data: args.todo.data,
                    tag: args.todo.tag
                };
                if (args.todo.events) {
                    $.each(args.todo.events, function(name, f) {
                        var that = $container, fn = f;
                        if ($.isArray(f)) {
                            that = f[0];
                            fn = f[1];
                        }
                        google.maps.event.addListener(sender, name, function(event) {
                            fn.apply(that, [ senders ? senders : sender, event, context ]);
                        });
                    });
                }
                if (args.todo.onces) {
                    $.each(args.todo.onces, function(name, f) {
                        var that = $container, fn = f;
                        if ($.isArray(f)) {
                            that = f[0];
                            fn = f[1];
                        }
                        google.maps.event.addListenerOnce(sender, name, function(event) {
                            fn.apply(that, [ senders ? senders : sender, event, context ]);
                        });
                    });
                }
            }
        }
        function Stack() {
            var st = [];
            this.empty = function() {
                return !st.length;
            };
            this.add = function(v) {
                st.push(v);
            };
            this.get = function() {
                return st.length ? st[0] : false;
            };
            this.ack = function() {
                st.shift();
            };
        }
        function Task(ctx, onEnd, todo) {
            var session = {}, that = this, current, resolve = {
                latLng: {
                    map: false,
                    marker: false,
                    infowindow: false,
                    circle: false,
                    overlay: false,
                    getlatlng: false,
                    getmaxzoom: false,
                    getelevation: false,
                    streetviewpanorama: false,
                    getaddress: true
                },
                geoloc: {
                    getgeoloc: true
                }
            };
            if (typeof todo === "string") {
                todo = unify(todo);
            }
            function unify(todo) {
                var result = {};
                result[todo] = {};
                return result;
            }
            function next() {
                var k;
                for (k in todo) {
                    if (k in session) {
                        continue;
                    }
                    return k;
                }
            }
            this.run = function() {
                var k, opts;
                while (k = next()) {
                    if (typeof ctx[k] === "function") {
                        current = k;
                        opts = $.extend(true, {}, defaults[k] || {}, todo[k].options || {});
                        if (k in resolve.latLng) {
                            if (todo[k].values) {
                                resolveAllLatLng(todo[k].values, ctx, ctx[k], {
                                    todo: todo[k],
                                    opts: opts,
                                    session: session
                                });
                            } else {
                                resolveLatLng(ctx, ctx[k], resolve.latLng[k], {
                                    todo: todo[k],
                                    opts: opts,
                                    session: session
                                });
                            }
                        } else if (k in resolve.geoloc) {
                            geoloc(ctx, ctx[k], {
                                todo: todo[k],
                                opts: opts,
                                session: session
                            });
                        } else {
                            ctx[k].apply(ctx, [ {
                                todo: todo[k],
                                opts: opts,
                                session: session
                            } ]);
                        }
                        return;
                    } else {
                        session[k] = null;
                    }
                }
                onEnd.apply(ctx, [ todo, session ]);
            };
            this.ack = function(result) {
                session[current] = result;
                that.run.apply(that, []);
            };
        }
        function getKeys(obj) {
            var k, keys = [];
            for (k in obj) {
                keys.push(k);
            }
            return keys;
        }
        function tuple(args, value) {
            var todo = {};
            if (args.todo) {
                for (var k in args.todo) {
                    if (k !== "options" && k !== "values") {
                        todo[k] = args.todo[k];
                    }
                }
            }
            var i, keys = [ "data", "tag", "id", "events", "onces" ];
            for (i = 0; i < keys.length; i++) {
                copyKey(todo, keys[i], value, args.todo);
            }
            todo.options = $.extend({}, args.opts || {}, value.options || {});
            return todo;
        }
        function copyKey(target, key) {
            for (var i = 2; i < arguments.length; i++) {
                if (key in arguments[i]) {
                    target[key] = arguments[i][key];
                    return;
                }
            }
        }
        function GeocoderCache() {
            var cache = [];
            this.get = function(request) {
                if (cache.length) {
                    var i, j, k, item, eq, keys = getKeys(request);
                    for (i = 0; i < cache.length; i++) {
                        item = cache[i];
                        eq = keys.length == item.keys.length;
                        for (j = 0; j < keys.length && eq; j++) {
                            k = keys[j];
                            eq = k in item.request;
                            if (eq) {
                                if (typeof request[k] === "object" && "equals" in request[k] && typeof request[k] === "function") {
                                    eq = request[k].equals(item.request[k]);
                                } else {
                                    eq = request[k] === item.request[k];
                                }
                            }
                        }
                        if (eq) {
                            return item.results;
                        }
                    }
                }
            };
            this.store = function(request, results) {
                cache.push({
                    request: request,
                    keys: getKeys(request),
                    results: results
                });
            };
        }
        function OverlayView(map, opts, latLng, $div) {
            var that = this, listeners = [];
            defaults.classes.OverlayView.call(this);
            this.setMap(map);
            this.onAdd = function() {
                var panes = this.getPanes();
                if (opts.pane in panes) {
                    $(panes[opts.pane]).append($div);
                }
                $.each("dblclick click mouseover mousemove mouseout mouseup mousedown".split(" "), function(i, name) {
                    listeners.push(google.maps.event.addDomListener($div[0], name, function(e) {
                        $.Event(e).stopPropagation();
                        google.maps.event.trigger(that, name, [ e ]);
                        that.draw();
                    }));
                });
                listeners.push(google.maps.event.addDomListener($div[0], "contextmenu", function(e) {
                    $.Event(e).stopPropagation();
                    google.maps.event.trigger(that, "rightclick", [ e ]);
                    that.draw();
                }));
            };
            this.getPosition = function() {
                return latLng;
            };
            this.draw = function() {
                var ps = this.getProjection().fromLatLngToDivPixel(latLng);
                $div.css("left", ps.x + opts.offset.x + "px").css("top", ps.y + opts.offset.y + "px");
            };
            this.onRemove = function() {
                for (var i = 0; i < listeners.length; i++) {
                    google.maps.event.removeListener(listeners[i]);
                }
                $div.remove();
            };
            this.hide = function() {
                $div.hide();
            };
            this.show = function() {
                $div.show();
            };
            this.toggle = function() {
                if ($div) {
                    if ($div.is(":visible")) {
                        this.show();
                    } else {
                        this.hide();
                    }
                }
            };
            this.toggleDOM = function() {
                if (this.getMap()) {
                    this.setMap(null);
                } else {
                    this.setMap(map);
                }
            };
            this.getDOMElement = function() {
                return $div[0];
            };
        }
        function newEmptyOverlay(map, radius) {
            function Overlay() {
                this.onAdd = function() {};
                this.onRemove = function() {};
                this.draw = function() {};
                return defaults.classes.OverlayView.apply(this, []);
            }
            Overlay.prototype = defaults.classes.OverlayView.prototype;
            var obj = new Overlay();
            obj.setMap(map);
            return obj;
        }
        function InternalClusterer($container, map, raw) {
            var updating = false, updated = false, redrawing = false, ready = false, enabled = true, that = this, events = [], store = {}, ids = {}, idxs = {}, markers = [], todos = [], values = [], overlay = newEmptyOverlay(map, raw.radius), timer, projection, ffilter, fdisplay, ferror;
            main();
            function prepareMarker(index) {
                if (!markers[index]) {
                    delete todos[index].options.map;
                    markers[index] = new defaults.classes.Marker(todos[index].options);
                    attachEvents($container, {
                        todo: todos[index]
                    }, markers[index], todos[index].id);
                }
            }
            this.getById = function(id) {
                if (id in ids) {
                    prepareMarker(ids[id]);
                    return markers[ids[id]];
                }
                return false;
            };
            this.rm = function(id) {
                var index = ids[id];
                if (markers[index]) {
                    markers[index].setMap(null);
                }
                delete markers[index];
                markers[index] = false;
                delete todos[index];
                todos[index] = false;
                delete values[index];
                values[index] = false;
                delete ids[id];
                delete idxs[index];
                updated = true;
            };
            this.clearById = function(id) {
                if (id in ids) {
                    this.rm(id);
                    return true;
                }
            };
            this.clear = function(last, first, tag) {
                var start, stop, step, index, i, list = [], check = ftag(tag);
                if (last) {
                    start = todos.length - 1;
                    stop = -1;
                    step = -1;
                } else {
                    start = 0;
                    stop = todos.length;
                    step = 1;
                }
                for (index = start; index != stop; index += step) {
                    if (todos[index]) {
                        if (!check || check(todos[index].tag)) {
                            list.push(idxs[index]);
                            if (first || last) {
                                break;
                            }
                        }
                    }
                }
                for (i = 0; i < list.length; i++) {
                    this.rm(list[i]);
                }
            };
            this.add = function(todo, value) {
                todo.id = globalId(todo.id);
                this.clearById(todo.id);
                ids[todo.id] = markers.length;
                idxs[markers.length] = todo.id;
                markers.push(null);
                todos.push(todo);
                values.push(value);
                updated = true;
            };
            this.addMarker = function(marker, todo) {
                todo = todo || {};
                todo.id = globalId(todo.id);
                this.clearById(todo.id);
                if (!todo.options) {
                    todo.options = {};
                }
                todo.options.position = marker.getPosition();
                attachEvents($container, {
                    todo: todo
                }, marker, todo.id);
                ids[todo.id] = markers.length;
                idxs[markers.length] = todo.id;
                markers.push(marker);
                todos.push(todo);
                values.push(todo.data || {});
                updated = true;
            };
            this.todo = function(index) {
                return todos[index];
            };
            this.value = function(index) {
                return values[index];
            };
            this.marker = function(index) {
                if (index in markers) {
                    prepareMarker(index);
                    return markers[index];
                }
                return false;
            };
            this.markerIsSet = function(index) {
                return Boolean(markers[index]);
            };
            this.setMarker = function(index, marker) {
                markers[index] = marker;
            };
            this.store = function(cluster, obj, shadow) {
                store[cluster.ref] = {
                    obj: obj,
                    shadow: shadow
                };
            };
            this.free = function() {
                for (var i = 0; i < events.length; i++) {
                    google.maps.event.removeListener(events[i]);
                }
                events = [];
                $.each(store, function(key) {
                    flush(key);
                });
                store = {};
                $.each(todos, function(i) {
                    todos[i] = null;
                });
                todos = [];
                $.each(markers, function(i) {
                    if (markers[i]) {
                        markers[i].setMap(null);
                        delete markers[i];
                    }
                });
                markers = [];
                $.each(values, function(i) {
                    delete values[i];
                });
                values = [];
                ids = {};
                idxs = {};
            };
            this.filter = function(f) {
                ffilter = f;
                redraw();
            };
            this.enable = function(value) {
                if (enabled != value) {
                    enabled = value;
                    redraw();
                }
            };
            this.display = function(f) {
                fdisplay = f;
            };
            this.error = function(f) {
                ferror = f;
            };
            this.beginUpdate = function() {
                updating = true;
            };
            this.endUpdate = function() {
                updating = false;
                if (updated) {
                    redraw();
                }
            };
            this.autofit = function(bounds) {
                for (var i = 0; i < todos.length; i++) {
                    if (todos[i]) {
                        bounds.extend(todos[i].options.position);
                    }
                }
            };
            function main() {
                projection = overlay.getProjection();
                if (!projection) {
                    setTimeout(function() {
                        main.apply(that, []);
                    }, 25);
                    return;
                }
                ready = true;
                events.push(google.maps.event.addListener(map, "zoom_changed", function() {
                    delayRedraw();
                }));
                events.push(google.maps.event.addListener(map, "bounds_changed", function() {
                    delayRedraw();
                }));
                redraw();
            }
            function flush(key) {
                if (typeof store[key] === "object") {
                    if (typeof store[key].obj.setMap === "function") {
                        store[key].obj.setMap(null);
                    }
                    if (typeof store[key].obj.remove === "function") {
                        store[key].obj.remove();
                    }
                    if (typeof store[key].shadow.remove === "function") {
                        store[key].obj.remove();
                    }
                    if (typeof store[key].shadow.setMap === "function") {
                        store[key].shadow.setMap(null);
                    }
                    delete store[key].obj;
                    delete store[key].shadow;
                } else if (markers[key]) {
                    markers[key].setMap(null);
                }
                delete store[key];
            }
            function distanceInMeter() {
                var lat1, lat2, lng1, lng2, e, f, g, h;
                if (arguments[0] instanceof google.maps.LatLng) {
                    lat1 = arguments[0].lat();
                    lng1 = arguments[0].lng();
                    if (arguments[1] instanceof google.maps.LatLng) {
                        lat2 = arguments[1].lat();
                        lng2 = arguments[1].lng();
                    } else {
                        lat2 = arguments[1];
                        lng2 = arguments[2];
                    }
                } else {
                    lat1 = arguments[0];
                    lng1 = arguments[1];
                    if (arguments[2] instanceof google.maps.LatLng) {
                        lat2 = arguments[2].lat();
                        lng2 = arguments[2].lng();
                    } else {
                        lat2 = arguments[2];
                        lng2 = arguments[3];
                    }
                }
                e = Math.PI * lat1 / 180;
                f = Math.PI * lng1 / 180;
                g = Math.PI * lat2 / 180;
                h = Math.PI * lng2 / 180;
                return 1e3 * 6371 * Math.acos(Math.min(Math.cos(e) * Math.cos(g) * Math.cos(f) * Math.cos(h) + Math.cos(e) * Math.sin(f) * Math.cos(g) * Math.sin(h) + Math.sin(e) * Math.sin(g), 1));
            }
            function extendsMapBounds() {
                var radius = distanceInMeter(map.getCenter(), map.getBounds().getNorthEast()), circle = new google.maps.Circle({
                    center: map.getCenter(),
                    radius: 1.25 * radius
                });
                return circle.getBounds();
            }
            function getStoreKeys() {
                var keys = {}, k;
                for (k in store) {
                    keys[k] = true;
                }
                return keys;
            }
            function delayRedraw() {
                clearTimeout(timer);
                timer = setTimeout(function() {
                    redraw();
                }, 25);
            }
            function extendsBounds(latLng) {
                var p = projection.fromLatLngToDivPixel(latLng), ne = projection.fromDivPixelToLatLng(new google.maps.Point(p.x + raw.radius, p.y - raw.radius)), sw = projection.fromDivPixelToLatLng(new google.maps.Point(p.x - raw.radius, p.y + raw.radius));
                return new google.maps.LatLngBounds(sw, ne);
            }
            function redraw() {
                if (updating || redrawing || !ready) {
                    return;
                }
                var keys = [], used = {}, zoom = map.getZoom(), forceDisabled = "maxZoom" in raw && zoom > raw.maxZoom, previousKeys = getStoreKeys(), i, j, k, indexes, check = false, bounds, cluster, position, previous, lat, lng, loop;
                updated = false;
                if (zoom > 3) {
                    bounds = extendsMapBounds();
                    check = bounds.getSouthWest().lng() < bounds.getNorthEast().lng();
                }
                for (i = 0; i < todos.length; i++) {
                    if (todos[i] && (!check || bounds.contains(todos[i].options.position)) && (!ffilter || ffilter(values[i]))) {
                        keys.push(i);
                    }
                }
                while (1) {
                    i = 0;
                    while (used[i] && i < keys.length) {
                        i++;
                    }
                    if (i == keys.length) {
                        break;
                    }
                    indexes = [];
                    if (enabled && !forceDisabled) {
                        loop = 10;
                        do {
                            previous = indexes;
                            indexes = [];
                            loop--;
                            if (previous.length) {
                                position = bounds.getCenter();
                            } else {
                                position = todos[keys[i]].options.position;
                            }
                            bounds = extendsBounds(position);
                            for (j = i; j < keys.length; j++) {
                                if (used[j]) {
                                    continue;
                                }
                                if (bounds.contains(todos[keys[j]].options.position)) {
                                    indexes.push(j);
                                }
                            }
                        } while (previous.length < indexes.length && indexes.length > 1 && loop);
                    } else {
                        for (j = i; j < keys.length; j++) {
                            if (used[j]) {
                                continue;
                            }
                            indexes.push(j);
                            break;
                        }
                    }
                    cluster = {
                        indexes: [],
                        ref: []
                    };
                    lat = lng = 0;
                    for (k = 0; k < indexes.length; k++) {
                        used[indexes[k]] = true;
                        cluster.indexes.push(keys[indexes[k]]);
                        cluster.ref.push(keys[indexes[k]]);
                        lat += todos[keys[indexes[k]]].options.position.lat();
                        lng += todos[keys[indexes[k]]].options.position.lng();
                    }
                    lat /= indexes.length;
                    lng /= indexes.length;
                    cluster.latLng = new google.maps.LatLng(lat, lng);
                    cluster.ref = cluster.ref.join("-");
                    if (cluster.ref in previousKeys) {
                        delete previousKeys[cluster.ref];
                    } else {
                        if (indexes.length === 1) {
                            store[cluster.ref] = true;
                        }
                        fdisplay(cluster);
                    }
                }
                $.each(previousKeys, function(key) {
                    flush(key);
                });
                redrawing = false;
            }
        }
        function Clusterer(id, internalClusterer) {
            this.id = function() {
                return id;
            };
            this.filter = function(f) {
                internalClusterer.filter(f);
            };
            this.enable = function() {
                internalClusterer.enable(true);
            };
            this.disable = function() {
                internalClusterer.enable(false);
            };
            this.add = function(marker, todo, lock) {
                if (!lock) {
                    internalClusterer.beginUpdate();
                }
                internalClusterer.addMarker(marker, todo);
                if (!lock) {
                    internalClusterer.endUpdate();
                }
            };
            this.getById = function(id) {
                return internalClusterer.getById(id);
            };
            this.clearById = function(id, lock) {
                var result;
                if (!lock) {
                    internalClusterer.beginUpdate();
                }
                result = internalClusterer.clearById(id);
                if (!lock) {
                    internalClusterer.endUpdate();
                }
                return result;
            };
            this.clear = function(last, first, tag, lock) {
                if (!lock) {
                    internalClusterer.beginUpdate();
                }
                internalClusterer.clear(last, first, tag);
                if (!lock) {
                    internalClusterer.endUpdate();
                }
            };
        }
        function Store() {
            var store = {}, objects = {};
            function normalize(res) {
                return {
                    id: res.id,
                    name: res.name,
                    object: res.obj,
                    tag: res.tag,
                    data: res.data
                };
            }
            this.add = function(args, name, obj, sub) {
                var todo = args.todo || {}, id = globalId(todo.id);
                if (!store[name]) {
                    store[name] = [];
                }
                if (id in objects) {
                    this.clearById(id);
                }
                objects[id] = {
                    obj: obj,
                    sub: sub,
                    name: name,
                    id: id,
                    tag: todo.tag,
                    data: todo.data
                };
                store[name].push(id);
                return id;
            };
            this.getById = function(id, sub, full) {
                if (id in objects) {
                    if (sub) {
                        return objects[id].sub;
                    } else if (full) {
                        return normalize(objects[id]);
                    }
                    return objects[id].obj;
                }
                return false;
            };
            this.get = function(name, last, tag, full) {
                var n, id, check = ftag(tag);
                if (!store[name] || !store[name].length) {
                    return null;
                }
                n = store[name].length;
                while (n) {
                    n--;
                    id = store[name][last ? n : store[name].length - n - 1];
                    if (id && objects[id]) {
                        if (check && !check(objects[id].tag)) {
                            continue;
                        }
                        return full ? normalize(objects[id]) : objects[id].obj;
                    }
                }
                return null;
            };
            this.all = function(name, tag, full) {
                var result = [], check = ftag(tag), find = function(n) {
                    var i, id;
                    for (i = 0; i < store[n].length; i++) {
                        id = store[n][i];
                        if (id && objects[id]) {
                            if (check && !check(objects[id].tag)) {
                                continue;
                            }
                            result.push(full ? normalize(objects[id]) : objects[id].obj);
                        }
                    }
                };
                if (name in store) {
                    find(name);
                } else if (name === undef) {
                    for (name in store) {
                        find(name);
                    }
                }
                return result;
            };
            function rm(obj) {
                if (typeof obj.setMap === "function") {
                    obj.setMap(null);
                }
                if (typeof obj.remove === "function") {
                    obj.remove();
                }
                if (typeof obj.free === "function") {
                    obj.free();
                }
                obj = null;
            }
            this.rm = function(name, check, pop) {
                var idx, id;
                if (!store[name]) {
                    return false;
                }
                if (check) {
                    if (pop) {
                        for (idx = store[name].length - 1; idx >= 0; idx--) {
                            id = store[name][idx];
                            if (check(objects[id].tag)) {
                                break;
                            }
                        }
                    } else {
                        for (idx = 0; idx < store[name].length; idx++) {
                            id = store[name][idx];
                            if (check(objects[id].tag)) {
                                break;
                            }
                        }
                    }
                } else {
                    idx = pop ? store[name].length - 1 : 0;
                }
                if (!(idx in store[name])) {
                    return false;
                }
                return this.clearById(store[name][idx], idx);
            };
            this.clearById = function(id, idx) {
                if (id in objects) {
                    var i, name = objects[id].name;
                    for (i = 0; idx === undef && i < store[name].length; i++) {
                        if (id === store[name][i]) {
                            idx = i;
                        }
                    }
                    rm(objects[id].obj);
                    if (objects[id].sub) {
                        rm(objects[id].sub);
                    }
                    delete objects[id];
                    store[name].splice(idx, 1);
                    return true;
                }
                return false;
            };
            this.objGetById = function(id) {
                var result;
                if (store["clusterer"]) {
                    for (var idx in store["clusterer"]) {
                        if ((result = objects[store["clusterer"][idx]].obj.getById(id)) !== false) {
                            return result;
                        }
                    }
                }
                return false;
            };
            this.objClearById = function(id) {
                if (store["clusterer"]) {
                    for (var idx in store["clusterer"]) {
                        if (objects[store["clusterer"][idx]].obj.clearById(id)) {
                            return true;
                        }
                    }
                }
                return null;
            };
            this.clear = function(list, last, first, tag) {
                var k, i, name, check = ftag(tag);
                if (!list || !list.length) {
                    list = [];
                    for (k in store) {
                        list.push(k);
                    }
                } else {
                    list = array(list);
                }
                for (i = 0; i < list.length; i++) {
                    name = list[i];
                    if (last) {
                        this.rm(name, check, true);
                    } else if (first) {
                        this.rm(name, check, false);
                    } else {
                        while (this.rm(name, check, false)) ;
                    }
                }
            };
            this.objClear = function(list, last, first, tag) {
                if (store["clusterer"] && ($.inArray("marker", list) >= 0 || !list.length)) {
                    for (var idx in store["clusterer"]) {
                        objects[store["clusterer"][idx]].obj.clear(last, first, tag);
                    }
                }
            };
        }
        var services = {}, geocoderCache = new GeocoderCache();
        function geocoder() {
            if (!services.geocoder) {
                services.geocoder = new google.maps.Geocoder();
            }
            return services.geocoder;
        }
        function directionsService() {
            if (!services.directionsService) {
                services.directionsService = new google.maps.DirectionsService();
            }
            return services.directionsService;
        }
        function elevationService() {
            if (!services.elevationService) {
                services.elevationService = new google.maps.ElevationService();
            }
            return services.elevationService;
        }
        function maxZoomService() {
            if (!services.maxZoomService) {
                services.maxZoomService = new google.maps.MaxZoomService();
            }
            return services.maxZoomService;
        }
        function distanceMatrixService() {
            if (!services.distanceMatrixService) {
                services.distanceMatrixService = new google.maps.DistanceMatrixService();
            }
            return services.distanceMatrixService;
        }
        function error() {
            if (defaults.verbose) {
                var i, err = [];
                if (window.console && typeof console.error === "function") {
                    for (i = 0; i < arguments.length; i++) {
                        err.push(arguments[i]);
                    }
                    console.error.apply(console, err);
                } else {
                    err = "";
                    for (i = 0; i < arguments.length; i++) {
                        err += arguments[i].toString() + " ";
                    }
                    alert(err);
                }
            }
        }
        function numeric(mixed) {
            return (typeof mixed === "number" || typeof mixed === "string") && mixed !== "" && !isNaN(mixed);
        }
        function array(mixed) {
            var k, a = [];
            if (mixed !== undef) {
                if (typeof mixed === "object") {
                    if (typeof mixed.length === "number") {
                        a = mixed;
                    } else {
                        for (k in mixed) {
                            a.push(mixed[k]);
                        }
                    }
                } else {
                    a.push(mixed);
                }
            }
            return a;
        }
        function ftag(tag) {
            if (tag) {
                if (typeof tag === "function") {
                    return tag;
                }
                tag = array(tag);
                return function(val) {
                    if (val === undef) {
                        return false;
                    }
                    if (typeof val === "object") {
                        for (var i = 0; i < val.length; i++) {
                            if ($.inArray(val[i], tag) >= 0) {
                                return true;
                            }
                        }
                        return false;
                    }
                    return $.inArray(val, tag) >= 0;
                };
            }
        }
        function toLatLng(mixed, emptyReturnMixed, noFlat) {
            var empty = emptyReturnMixed ? mixed : null;
            if (!mixed || typeof mixed === "string") {
                return empty;
            }
            if (mixed.latLng) {
                return toLatLng(mixed.latLng);
            }
            if (mixed instanceof google.maps.LatLng) {
                return mixed;
            } else if (numeric(mixed.lat)) {
                return new google.maps.LatLng(mixed.lat, mixed.lng);
            } else if (!noFlat && $.isArray(mixed)) {
                if (!numeric(mixed[0]) || !numeric(mixed[1])) {
                    return empty;
                }
                return new google.maps.LatLng(mixed[0], mixed[1]);
            }
            return empty;
        }
        function toLatLngBounds(mixed) {
            var ne, sw;
            if (!mixed || mixed instanceof google.maps.LatLngBounds) {
                return mixed || null;
            }
            if ($.isArray(mixed)) {
                if (mixed.length == 2) {
                    ne = toLatLng(mixed[0]);
                    sw = toLatLng(mixed[1]);
                } else if (mixed.length == 4) {
                    ne = toLatLng([ mixed[0], mixed[1] ]);
                    sw = toLatLng([ mixed[2], mixed[3] ]);
                }
            } else {
                if ("ne" in mixed && "sw" in mixed) {
                    ne = toLatLng(mixed.ne);
                    sw = toLatLng(mixed.sw);
                } else if ("n" in mixed && "e" in mixed && "s" in mixed && "w" in mixed) {
                    ne = toLatLng([ mixed.n, mixed.e ]);
                    sw = toLatLng([ mixed.s, mixed.w ]);
                }
            }
            if (ne && sw) {
                return new google.maps.LatLngBounds(sw, ne);
            }
            return null;
        }
        function resolveLatLng(ctx, method, runLatLng, args, attempt) {
            var latLng = runLatLng ? toLatLng(args.todo, false, true) : false, conf = latLng ? {
                latLng: latLng
            } : args.todo.address ? typeof args.todo.address === "string" ? {
                address: args.todo.address
            } : args.todo.address : false, cache = conf ? geocoderCache.get(conf) : false, that = this;
            if (conf) {
                attempt = attempt || 0;
                if (cache) {
                    args.latLng = cache.results[0].geometry.location;
                    args.results = cache.results;
                    args.status = cache.status;
                    method.apply(ctx, [ args ]);
                } else {
                    if (conf.location) {
                        conf.location = toLatLng(conf.location);
                    }
                    if (conf.bounds) {
                        conf.bounds = toLatLngBounds(conf.bounds);
                    }
                    geocoder().geocode(conf, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            geocoderCache.store(conf, {
                                results: results,
                                status: status
                            });
                            args.latLng = results[0].geometry.location;
                            args.results = results;
                            args.status = status;
                            method.apply(ctx, [ args ]);
                        } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT && attempt < defaults.queryLimit.attempt) {
                            setTimeout(function() {
                                resolveLatLng.apply(that, [ ctx, method, runLatLng, args, attempt + 1 ]);
                            }, defaults.queryLimit.delay + Math.floor(Math.random() * defaults.queryLimit.random));
                        } else {
                            error("geocode failed", status, conf);
                            args.latLng = args.results = false;
                            args.status = status;
                            method.apply(ctx, [ args ]);
                        }
                    });
                }
            } else {
                args.latLng = toLatLng(args.todo, false, true);
                method.apply(ctx, [ args ]);
            }
        }
        function resolveAllLatLng(list, ctx, method, args) {
            var that = this, i = -1;
            function resolve() {
                do {
                    i++;
                } while (i < list.length && !("address" in list[i]));
                if (i >= list.length) {
                    method.apply(ctx, [ args ]);
                    return;
                }
                resolveLatLng(that, function(args) {
                    delete args.todo;
                    $.extend(list[i], args);
                    resolve.apply(that, []);
                }, true, {
                    todo: list[i]
                });
            }
            resolve();
        }
        function geoloc(ctx, method, args) {
            var is_echo = false;
            if (navigator && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(pos) {
                    if (is_echo) {
                        return;
                    }
                    is_echo = true;
                    args.latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    method.apply(ctx, [ args ]);
                }, function() {
                    if (is_echo) {
                        return;
                    }
                    is_echo = true;
                    args.latLng = false;
                    method.apply(ctx, [ args ]);
                }, args.opts.getCurrentPosition);
            } else {
                args.latLng = false;
                method.apply(ctx, [ args ]);
            }
        }
        function Gmap3($this) {
            var that = this, stack = new Stack(), store = new Store(), map = null, task;
            this._plan = function(list) {
                for (var k = 0; k < list.length; k++) {
                    stack.add(new Task(that, end, list[k]));
                }
                run();
            };
            function run() {
                if (!task && (task = stack.get())) {
                    task.run();
                }
            }
            function end() {
                task = null;
                stack.ack();
                run.call(that);
            }
            function callback(args) {
                if (args.todo.callback) {
                    var params = Array.prototype.slice.call(arguments, 1);
                    if (typeof args.todo.callback === "function") {
                        args.todo.callback.apply($this, params);
                    } else if ($.isArray(args.todo.callback)) {
                        if (typeof args.todo.callback[1] === "function") {
                            args.todo.callback[1].apply(args.todo.callback[0], params);
                        }
                    }
                }
            }
            function manageEnd(args, obj, id) {
                if (id) {
                    attachEvents($this, args, obj, id);
                }
                callback(args, obj);
                task.ack(obj);
            }
            function newMap(latLng, args) {
                args = args || {};
                if (map) {
                    if (args.todo && args.todo.options) {
                        if (args.todo.options.center) {
                            args.todo.options.center = toLatLng(args.todo.options.center);
                        }
                        map.setOptions(args.todo.options);
                    }
                } else {
                    var opts = args.opts || $.extend(true, {}, defaults.map, args.todo && args.todo.options ? args.todo.options : {});
                    opts.center = latLng || toLatLng(opts.center);
                    map = new defaults.classes.Map($this.get(0), opts);
                }
            }
            this.map = function(args) {
                newMap(args.latLng, args);
                attachEvents($this, args, map);
                manageEnd(args, map);
            };
            this.destroy = function(args) {
                store.clear();
                $this.empty();
                if (map) {
                    map = null;
                }
                manageEnd(args, true);
            };
            this.infowindow = function(args) {
                var objs = [], multiple = "values" in args.todo;
                if (!multiple) {
                    if (args.latLng) {
                        args.opts.position = args.latLng;
                    }
                    args.todo.values = [ {
                        options: args.opts
                    } ];
                }
                $.each(args.todo.values, function(i, value) {
                    var id, obj, todo = tuple(args, value);
                    todo.options.position = todo.options.position ? toLatLng(todo.options.position) : toLatLng(value.latLng);
                    if (!map) {
                        newMap(todo.options.position);
                    }
                    obj = new defaults.classes.InfoWindow(todo.options);
                    if (obj && (todo.open === undef || todo.open)) {
                        if (multiple) {
                            obj.open(map, todo.anchor ? todo.anchor : undef);
                        } else {
                            obj.open(map, todo.anchor ? todo.anchor : args.latLng ? undef : args.session.marker ? args.session.marker : undef);
                        }
                    }
                    objs.push(obj);
                    id = store.add({
                        todo: todo
                    }, "infowindow", obj);
                    attachEvents($this, {
                        todo: todo
                    }, obj, id);
                });
                manageEnd(args, multiple ? objs : objs[0]);
            };
            this.circle = function(args) {
                var objs = [], multiple = "values" in args.todo;
                if (!multiple) {
                    args.opts.center = args.latLng || toLatLng(args.opts.center);
                    args.todo.values = [ {
                        options: args.opts
                    } ];
                }
                if (!args.todo.values.length) {
                    manageEnd(args, false);
                    return;
                }
                $.each(args.todo.values, function(i, value) {
                    var id, obj, todo = tuple(args, value);
                    todo.options.center = todo.options.center ? toLatLng(todo.options.center) : toLatLng(value);
                    if (!map) {
                        newMap(todo.options.center);
                    }
                    todo.options.map = map;
                    obj = new defaults.classes.Circle(todo.options);
                    objs.push(obj);
                    id = store.add({
                        todo: todo
                    }, "circle", obj);
                    attachEvents($this, {
                        todo: todo
                    }, obj, id);
                });
                manageEnd(args, multiple ? objs : objs[0]);
            };
            this.overlay = function(args, internal) {
                var objs = [], multiple = "values" in args.todo;
                if (!multiple) {
                    args.todo.values = [ {
                        latLng: args.latLng,
                        options: args.opts
                    } ];
                }
                if (!args.todo.values.length) {
                    manageEnd(args, false);
                    return;
                }
                if (!OverlayView.__initialised) {
                    OverlayView.prototype = new defaults.classes.OverlayView();
                    OverlayView.__initialised = true;
                }
                $.each(args.todo.values, function(i, value) {
                    var id, obj, todo = tuple(args, value), $div = $(document.createElement("div")).css({
                        border: "none",
                        borderWidth: "0px",
                        position: "absolute"
                    });
                    $div.append(todo.options.content);
                    obj = new OverlayView(map, todo.options, toLatLng(todo) || toLatLng(value), $div);
                    objs.push(obj);
                    $div = null;
                    if (!internal) {
                        id = store.add(args, "overlay", obj);
                        attachEvents($this, {
                            todo: todo
                        }, obj, id);
                    }
                });
                if (internal) {
                    return objs[0];
                }
                manageEnd(args, multiple ? objs : objs[0]);
            };
            this.getaddress = function(args) {
                callback(args, args.results, args.status);
                task.ack();
            };
            this.getlatlng = function(args) {
                callback(args, args.results, args.status);
                task.ack();
            };
            this.getmaxzoom = function(args) {
                maxZoomService().getMaxZoomAtLatLng(args.latLng, function(result) {
                    callback(args, result.status === google.maps.MaxZoomStatus.OK ? result.zoom : false, status);
                    task.ack();
                });
            };
            this.getelevation = function(args) {
                var i, locations = [], f = function(results, status) {
                    callback(args, status === google.maps.ElevationStatus.OK ? results : false, status);
                    task.ack();
                };
                if (args.latLng) {
                    locations.push(args.latLng);
                } else {
                    locations = array(args.todo.locations || []);
                    for (i = 0; i < locations.length; i++) {
                        locations[i] = toLatLng(locations[i]);
                    }
                }
                if (locations.length) {
                    elevationService().getElevationForLocations({
                        locations: locations
                    }, f);
                } else {
                    if (args.todo.path && args.todo.path.length) {
                        for (i = 0; i < args.todo.path.length; i++) {
                            locations.push(toLatLng(args.todo.path[i]));
                        }
                    }
                    if (locations.length) {
                        elevationService().getElevationAlongPath({
                            path: locations,
                            samples: args.todo.samples
                        }, f);
                    } else {
                        task.ack();
                    }
                }
            };
            this.defaults = function(args) {
                $.each(args.todo, function(name, value) {
                    if (typeof defaults[name] === "object") {
                        defaults[name] = $.extend({}, defaults[name], value);
                    } else {
                        defaults[name] = value;
                    }
                });
                task.ack(true);
            };
            this.rectangle = function(args) {
                var objs = [], multiple = "values" in args.todo;
                if (!multiple) {
                    args.todo.values = [ {
                        options: args.opts
                    } ];
                }
                if (!args.todo.values.length) {
                    manageEnd(args, false);
                    return;
                }
                $.each(args.todo.values, function(i, value) {
                    var id, obj, todo = tuple(args, value);
                    todo.options.bounds = todo.options.bounds ? toLatLngBounds(todo.options.bounds) : toLatLngBounds(value);
                    if (!map) {
                        newMap(todo.options.bounds.getCenter());
                    }
                    todo.options.map = map;
                    obj = new defaults.classes.Rectangle(todo.options);
                    objs.push(obj);
                    id = store.add({
                        todo: todo
                    }, "rectangle", obj);
                    attachEvents($this, {
                        todo: todo
                    }, obj, id);
                });
                manageEnd(args, multiple ? objs : objs[0]);
            };
            function poly(args, poly, path) {
                var objs = [], multiple = "values" in args.todo;
                if (!multiple) {
                    args.todo.values = [ {
                        options: args.opts
                    } ];
                }
                if (!args.todo.values.length) {
                    manageEnd(args, false);
                    return;
                }
                newMap();
                $.each(args.todo.values, function(_, value) {
                    var id, i, j, obj, todo = tuple(args, value);
                    if (todo.options[path]) {
                        if (todo.options[path][0][0] && $.isArray(todo.options[path][0][0])) {
                            for (i = 0; i < todo.options[path].length; i++) {
                                for (j = 0; j < todo.options[path][i].length; j++) {
                                    todo.options[path][i][j] = toLatLng(todo.options[path][i][j]);
                                }
                            }
                        } else {
                            for (i = 0; i < todo.options[path].length; i++) {
                                todo.options[path][i] = toLatLng(todo.options[path][i]);
                            }
                        }
                    }
                    todo.options.map = map;
                    obj = new google.maps[poly](todo.options);
                    objs.push(obj);
                    id = store.add({
                        todo: todo
                    }, poly.toLowerCase(), obj);
                    attachEvents($this, {
                        todo: todo
                    }, obj, id);
                });
                manageEnd(args, multiple ? objs : objs[0]);
            }
            this.polyline = function(args) {
                poly(args, "Polyline", "path");
            };
            this.polygon = function(args) {
                poly(args, "Polygon", "paths");
            };
            this.trafficlayer = function(args) {
                newMap();
                var obj = store.get("trafficlayer");
                if (!obj) {
                    obj = new defaults.classes.TrafficLayer();
                    obj.setMap(map);
                    store.add(args, "trafficlayer", obj);
                }
                manageEnd(args, obj);
            };
            this.bicyclinglayer = function(args) {
                newMap();
                var obj = store.get("bicyclinglayer");
                if (!obj) {
                    obj = new defaults.classes.BicyclingLayer();
                    obj.setMap(map);
                    store.add(args, "bicyclinglayer", obj);
                }
                manageEnd(args, obj);
            };
            this.groundoverlay = function(args) {
                args.opts.bounds = toLatLngBounds(args.opts.bounds);
                if (args.opts.bounds) {
                    newMap(args.opts.bounds.getCenter());
                }
                var id, obj = new defaults.classes.GroundOverlay(args.opts.url, args.opts.bounds, args.opts.opts);
                obj.setMap(map);
                id = store.add(args, "groundoverlay", obj);
                manageEnd(args, obj, id);
            };
            this.streetviewpanorama = function(args) {
                if (!args.opts.opts) {
                    args.opts.opts = {};
                }
                if (args.latLng) {
                    args.opts.opts.position = args.latLng;
                } else if (args.opts.opts.position) {
                    args.opts.opts.position = toLatLng(args.opts.opts.position);
                }
                if (args.todo.divId) {
                    args.opts.container = document.getElementById(args.todo.divId);
                } else if (args.opts.container) {
                    args.opts.container = $(args.opts.container).get(0);
                }
                var id, obj = new defaults.classes.StreetViewPanorama(args.opts.container, args.opts.opts);
                if (obj) {
                    map.setStreetView(obj);
                }
                id = store.add(args, "streetviewpanorama", obj);
                manageEnd(args, obj, id);
            };
            this.kmllayer = function(args) {
                var objs = [], multiple = "values" in args.todo;
                if (!multiple) {
                    args.todo.values = [ {
                        options: args.opts
                    } ];
                }
                if (!args.todo.values.length) {
                    manageEnd(args, false);
                    return;
                }
                $.each(args.todo.values, function(i, value) {
                    var id, obj, options, todo = tuple(args, value);
                    if (!map) {
                        newMap();
                    }
                    options = todo.options;
                    if (todo.options.opts) {
                        options = todo.options.opts;
                        if (todo.options.url) {
                            options.url = todo.options.url;
                        }
                    }
                    options.map = map;
                    if (googleVersionMin("3.10")) {
                        obj = new defaults.classes.KmlLayer(options);
                    } else {
                        obj = new defaults.classes.KmlLayer(options.url, options);
                    }
                    objs.push(obj);
                    id = store.add({
                        todo: todo
                    }, "kmllayer", obj);
                    attachEvents($this, {
                        todo: todo
                    }, obj, id);
                });
                manageEnd(args, multiple ? objs : objs[0]);
            };
            this.panel = function(args) {
                newMap();
                var id, x = 0, y = 0, $content, $div = $(document.createElement("div"));
                $div.css({
                    position: "absolute",
                    zIndex: 1e3,
                    visibility: "hidden"
                });
                if (args.opts.content) {
                    $content = $(args.opts.content);
                    $div.append($content);
                    $this.first().prepend($div);
                    if (args.opts.left !== undef) {
                        x = args.opts.left;
                    } else if (args.opts.right !== undef) {
                        x = $this.width() - $content.width() - args.opts.right;
                    } else if (args.opts.center) {
                        x = ($this.width() - $content.width()) / 2;
                    }
                    if (args.opts.top !== undef) {
                        y = args.opts.top;
                    } else if (args.opts.bottom !== undef) {
                        y = $this.height() - $content.height() - args.opts.bottom;
                    } else if (args.opts.middle) {
                        y = ($this.height() - $content.height()) / 2;
                    }
                    $div.css({
                        top: y,
                        left: x,
                        visibility: "visible"
                    });
                }
                id = store.add(args, "panel", $div);
                manageEnd(args, $div, id);
                $div = null;
            };
            function createClusterer(raw) {
                var internalClusterer = new InternalClusterer($this, map, raw), todo = {}, styles = {}, thresholds = [], isInt = /^[0-9]+$/, calculator, k;
                for (k in raw) {
                    if (isInt.test(k)) {
                        thresholds.push(1 * k);
                        styles[k] = raw[k];
                        styles[k].width = styles[k].width || 0;
                        styles[k].height = styles[k].height || 0;
                    } else {
                        todo[k] = raw[k];
                    }
                }
                thresholds.sort(function(a, b) {
                    return a > b;
                });
                if (todo.calculator) {
                    calculator = function(indexes) {
                        var data = [];
                        $.each(indexes, function(i, index) {
                            data.push(internalClusterer.value(index));
                        });
                        return todo.calculator.apply($this, [ data ]);
                    };
                } else {
                    calculator = function(indexes) {
                        return indexes.length;
                    };
                }
                internalClusterer.error(function() {
                    error.apply(that, arguments);
                });
                internalClusterer.display(function(cluster) {
                    var i, style, atodo, obj, offset, cnt = calculator(cluster.indexes);
                    if (raw.force || cnt > 1) {
                        for (i = 0; i < thresholds.length; i++) {
                            if (thresholds[i] <= cnt) {
                                style = styles[thresholds[i]];
                            }
                        }
                    }
                    if (style) {
                        offset = style.offset || [ -style.width / 2, -style.height / 2 ];
                        atodo = $.extend({}, todo);
                        atodo.options = $.extend({
                            pane: "overlayLayer",
                            content: style.content ? style.content.replace("CLUSTER_COUNT", cnt) : "",
                            offset: {
                                x: ("x" in offset ? offset.x : offset[0]) || 0,
                                y: ("y" in offset ? offset.y : offset[1]) || 0
                            }
                        }, todo.options || {});
                        obj = that.overlay({
                            todo: atodo,
                            opts: atodo.options,
                            latLng: toLatLng(cluster)
                        }, true);
                        atodo.options.pane = "floatShadow";
                        atodo.options.content = $(document.createElement("div")).width(style.width + "px").height(style.height + "px").css({
                            cursor: "pointer"
                        });
                        shadow = that.overlay({
                            todo: atodo,
                            opts: atodo.options,
                            latLng: toLatLng(cluster)
                        }, true);
                        todo.data = {
                            latLng: toLatLng(cluster),
                            markers: []
                        };
                        $.each(cluster.indexes, function(i, index) {
                            todo.data.markers.push(internalClusterer.value(index));
                            if (internalClusterer.markerIsSet(index)) {
                                internalClusterer.marker(index).setMap(null);
                            }
                        });
                        attachEvents($this, {
                            todo: todo
                        }, shadow, undef, {
                            main: obj,
                            shadow: shadow
                        });
                        internalClusterer.store(cluster, obj, shadow);
                    } else {
                        $.each(cluster.indexes, function(i, index) {
                            internalClusterer.marker(index).setMap(map);
                        });
                    }
                });
                return internalClusterer;
            }
            this.marker = function(args) {
                var multiple = "values" in args.todo, init = !map;
                if (!multiple) {
                    args.opts.position = args.latLng || toLatLng(args.opts.position);
                    args.todo.values = [ {
                        options: args.opts
                    } ];
                }
                if (!args.todo.values.length) {
                    manageEnd(args, false);
                    return;
                }
                if (init) {
                    newMap();
                }
                if (args.todo.cluster && !map.getBounds()) {
                    google.maps.event.addListenerOnce(map, "bounds_changed", function() {
                        that.marker.apply(that, [ args ]);
                    });
                    return;
                }
                if (args.todo.cluster) {
                    var clusterer, internalClusterer;
                    if (args.todo.cluster instanceof Clusterer) {
                        clusterer = args.todo.cluster;
                        internalClusterer = store.getById(clusterer.id(), true);
                    } else {
                        internalClusterer = createClusterer(args.todo.cluster);
                        clusterer = new Clusterer(globalId(args.todo.id, true), internalClusterer);
                        store.add(args, "clusterer", clusterer, internalClusterer);
                    }
                    internalClusterer.beginUpdate();
                    $.each(args.todo.values, function(i, value) {
                        var todo = tuple(args, value);
                        todo.options.position = todo.options.position ? toLatLng(todo.options.position) : toLatLng(value);
                        todo.options.map = map;
                        if (init) {
                            map.setCenter(todo.options.position);
                            init = false;
                        }
                        internalClusterer.add(todo, value);
                    });
                    internalClusterer.endUpdate();
                    manageEnd(args, clusterer);
                } else {
                    var objs = [];
                    $.each(args.todo.values, function(i, value) {
                        var id, obj, todo = tuple(args, value);
                        todo.options.position = todo.options.position ? toLatLng(todo.options.position) : toLatLng(value);
                        todo.options.map = map;
                        if (init) {
                            map.setCenter(todo.options.position);
                            init = false;
                        }
                        obj = new defaults.classes.Marker(todo.options);
                        objs.push(obj);
                        id = store.add({
                            todo: todo
                        }, "marker", obj);
                        attachEvents($this, {
                            todo: todo
                        }, obj, id);
                    });
                    manageEnd(args, multiple ? objs : objs[0]);
                }
            };
            this.getroute = function(args) {
                args.opts.origin = toLatLng(args.opts.origin, true);
                args.opts.destination = toLatLng(args.opts.destination, true);
                directionsService().route(args.opts, function(results, status) {
                    callback(args, status == google.maps.DirectionsStatus.OK ? results : false, status);
                    task.ack();
                });
            };
            this.directionsrenderer = function(args) {
                args.opts.map = map;
                var id, obj = new google.maps.DirectionsRenderer(args.opts);
                if (args.todo.divId) {
                    obj.setPanel(document.getElementById(args.todo.divId));
                } else if (args.todo.container) {
                    obj.setPanel($(args.todo.container).get(0));
                }
                id = store.add(args, "directionsrenderer", obj);
                manageEnd(args, obj, id);
            };
            this.getgeoloc = function(args) {
                manageEnd(args, args.latLng);
            };
            this.styledmaptype = function(args) {
                newMap();
                var obj = new defaults.classes.StyledMapType(args.todo.styles, args.opts);
                map.mapTypes.set(args.todo.id, obj);
                manageEnd(args, obj);
            };
            this.imagemaptype = function(args) {
                newMap();
                var obj = new defaults.classes.ImageMapType(args.opts);
                map.mapTypes.set(args.todo.id, obj);
                manageEnd(args, obj);
            };
            this.autofit = function(args) {
                var bounds = new google.maps.LatLngBounds();
                $.each(store.all(), function(i, obj) {
                    if (obj.getPosition) {
                        bounds.extend(obj.getPosition());
                    } else if (obj.getBounds) {
                        bounds.extend(obj.getBounds().getNorthEast());
                        bounds.extend(obj.getBounds().getSouthWest());
                    } else if (obj.getPaths) {
                        obj.getPaths().forEach(function(path) {
                            path.forEach(function(latLng) {
                                bounds.extend(latLng);
                            });
                        });
                    } else if (obj.getPath) {
                        obj.getPath().forEach(function(latLng) {
                            bounds.extend(latLng);
                            "";
                        });
                    } else if (obj.getCenter) {
                        bounds.extend(obj.getCenter());
                    } else if (obj instanceof Clusterer) {
                        obj = store.getById(obj.id(), true);
                        if (obj) {
                            obj.autofit(bounds);
                        }
                    }
                });
                if (!bounds.isEmpty() && (!map.getBounds() || !map.getBounds().equals(bounds))) {
                    if ("maxZoom" in args.todo) {
                        google.maps.event.addListenerOnce(map, "bounds_changed", function() {
                            if (this.getZoom() > args.todo.maxZoom) {
                                this.setZoom(args.todo.maxZoom);
                            }
                        });
                    }
                    map.fitBounds(bounds);
                }
                manageEnd(args, true);
            };
            this.clear = function(args) {
                if (typeof args.todo === "string") {
                    if (store.clearById(args.todo) || store.objClearById(args.todo)) {
                        manageEnd(args, true);
                        return;
                    }
                    args.todo = {
                        name: args.todo
                    };
                }
                if (args.todo.id) {
                    $.each(array(args.todo.id), function(i, id) {
                        store.clearById(id) || store.objClearById(id);
                    });
                } else {
                    store.clear(array(args.todo.name), args.todo.last, args.todo.first, args.todo.tag);
                    store.objClear(array(args.todo.name), args.todo.last, args.todo.first, args.todo.tag);
                }
                manageEnd(args, true);
            };
            this.exec = function(args) {
                var that = this;
                $.each(array(args.todo.func), function(i, func) {
                    $.each(that.get(args.todo, true, args.todo.hasOwnProperty("full") ? args.todo.full : true), function(j, res) {
                        func.call($this, res);
                    });
                });
                manageEnd(args, true);
            };
            this.get = function(args, direct, full) {
                var name, res, todo = direct ? args : args.todo;
                if (!direct) {
                    full = todo.full;
                }
                if (typeof todo === "string") {
                    res = store.getById(todo, false, full) || store.objGetById(todo);
                    if (res === false) {
                        name = todo;
                        todo = {};
                    }
                } else {
                    name = todo.name;
                }
                if (name === "map") {
                    res = map;
                }
                if (!res) {
                    res = [];
                    if (todo.id) {
                        $.each(array(todo.id), function(i, id) {
                            res.push(store.getById(id, false, full) || store.objGetById(id));
                        });
                        if (!$.isArray(todo.id)) {
                            res = res[0];
                        }
                    } else {
                        $.each(name ? array(name) : [ undef ], function(i, aName) {
                            var result;
                            if (todo.first) {
                                result = store.get(aName, false, todo.tag, full);
                                if (result) res.push(result);
                            } else if (todo.all) {
                                $.each(store.all(aName, todo.tag, full), function(i, result) {
                                    res.push(result);
                                });
                            } else {
                                result = store.get(aName, true, todo.tag, full);
                                if (result) res.push(result);
                            }
                        });
                        if (!todo.all && !$.isArray(name)) {
                            res = res[0];
                        }
                    }
                }
                res = $.isArray(res) || !todo.all ? res : [ res ];
                if (direct) {
                    return res;
                } else {
                    manageEnd(args, res);
                }
            };
            this.getdistance = function(args) {
                var i;
                args.opts.origins = array(args.opts.origins);
                for (i = 0; i < args.opts.origins.length; i++) {
                    args.opts.origins[i] = toLatLng(args.opts.origins[i], true);
                }
                args.opts.destinations = array(args.opts.destinations);
                for (i = 0; i < args.opts.destinations.length; i++) {
                    args.opts.destinations[i] = toLatLng(args.opts.destinations[i], true);
                }
                distanceMatrixService().getDistanceMatrix(args.opts, function(results, status) {
                    callback(args, status === google.maps.DistanceMatrixStatus.OK ? results : false, status);
                    task.ack();
                });
            };
            this.trigger = function(args) {
                if (typeof args.todo === "string") {
                    google.maps.event.trigger(map, args.todo);
                } else {
                    var options = [ map, args.todo.eventName ];
                    if (args.todo.var_args) {
                        $.each(args.todo.var_args, function(i, v) {
                            options.push(v);
                        });
                    }
                    google.maps.event.trigger.apply(google.maps.event, options);
                }
                callback(args);
                task.ack();
            };
        }
        function isDirectGet(obj) {
            var k;
            if (!typeof obj === "object" || !obj.hasOwnProperty("get")) {
                return false;
            }
            for (k in obj) {
                if (k !== "get") {
                    return false;
                }
            }
            return !obj.get.hasOwnProperty("callback");
        }
        $.fn.gmap3 = function() {
            var i, list = [], empty = true, results = [];
            initDefaults();
            for (i = 0; i < arguments.length; i++) {
                if (arguments[i]) {
                    list.push(arguments[i]);
                }
            }
            if (!list.length) {
                list.push("map");
            }
            $.each(this, function() {
                var $this = $(this), gmap3 = $this.data("gmap3");
                empty = false;
                if (!gmap3) {
                    gmap3 = new Gmap3($this);
                    $this.data("gmap3", gmap3);
                }
                if (list.length === 1 && (list[0] === "get" || isDirectGet(list[0]))) {
                    if (list[0] === "get") {
                        results.push(gmap3.get("map", true));
                    } else {
                        results.push(gmap3.get(list[0].get, true, list[0].get.full));
                    }
                } else {
                    gmap3._plan(list);
                }
            });
            if (results.length) {
                if (results.length === 1) {
                    return results[0];
                } else {
                    return results;
                }
            }
            return this;
        };
    })(jQuery);
    jQuery(document).ready(function($) {
        function mycarousel4_initCallback(e) {
            e.buttonNext.hover(function() {
                e.stopAuto();
            }, function() {
                e.startAuto();
            });
            e.buttonPrev.hover(function() {
                e.stopAuto();
            }, function() {
                e.startAuto();
            });
            e.clip.hover(function() {
                e.stopAuto();
            }, function() {
                e.startAuto();
            });
        }
        "use strict";
        jQuery.browser = {};
        (function() {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
        if ($(".extrabox")[0]) {
            (function($) {
                $.fn.clickToggle = function(func1, func2) {
                    var funcs = [ func1, func2 ];
                    this.data("toggleclicked", 0);
                    this.click(function() {
                        var data = $(this).data();
                        var tc = data.toggleclicked;
                        $.proxy(funcs[tc], this)();
                        data.toggleclicked = (tc + 1) % 2;
                    });
                    return this;
                };
            })(jQuery);
            var DropHeight = jQuery(".extrabox").height();
            jQuery(".extrabox").css("top", "-" + DropHeight + "px");
            jQuery(".arrow-down").clickToggle(function() {
                var DropHeight = jQuery(".extrabox").height();
                jQuery(this).addClass("opened");
                jQuery(".extrabox").animate({
                    top: 0
                }, {
                    duration: "800",
                    easing: "easeInOutExpo"
                });
                jQuery(".arrow-down i").removeClass("icon-angle-down").addClass("icon-angle-up");
                jQuery(".page-content, .sliderr, .headdown, .head, .breadcrumb, footer").animate({
                    opacity: .5
                }, {
                    duration: "2000",
                    easing: "easeInOutExpo"
                });
            }, function() {
                var DropHeight = jQuery(".extrabox").height();
                jQuery(this).removeClass("opened");
                jQuery(".extrabox").animate({
                    top: -DropHeight
                }, {
                    duration: "800",
                    easing: "easeInOutExpo"
                });
                jQuery(".arrow-down i").addClass("icon-angle-down").removeClass("icon-angle-up");
                jQuery(".page-content, .sliderr, .headdown, .head, .breadcrumb, footer").animate({
                    opacity: 1
                }, {
                    duration: "2000",
                    easing: "easeInOutExpo"
                });
            });
        }
        var tabs = jQuery("ul.tabs");
        tabs.each(function(i) {
            var tab = jQuery(this).find("> li > a");
            tab.click(function(e) {
                var contentLocation = jQuery(this).attr("href");
                if (contentLocation.charAt(0) === "#") {
                    e.preventDefault();
                    tab.removeClass("active");
                    jQuery(this).addClass("active");
                    jQuery(contentLocation).fadeIn(500).addClass("active").siblings().hide().removeClass("active");
                }
            });
        });
        jQuery("ul.tt-accordion li").each(function() {
            jQuery(this).children(".accordion-content").css("height", function() {
                return jQuery(this).height();
            });
            if (jQuery(this).index() > 0) {
                jQuery(this).children(".accordion-content").css("display", "none");
            } else {
                if ($(".faq")[0]) {
                    jQuery(this).addClass("active").find(".accordion-head-sign").append("<i class='icon-ok-sign'></i>");
                    jQuery(this).siblings("li").find(".accordion-head-sign").append("<i class='icon-question-sign'></i>");
                } else {
                    jQuery(this).addClass("active").find(".accordion-head-sign").append("<i class='icon-minus-sign'></i>");
                    jQuery(this).siblings("li").find(".accordion-head-sign").append("<i class='icon-plus-sign'></i>");
                }
            }
            jQuery(this).children(".accordion-head").bind("click", function() {
                jQuery(this).parent().addClass(function() {
                    if (jQuery(this).hasClass("active")) {
                        return;
                    }
                    {
                        return "active";
                    }
                });
                if ($(".faq")[0]) {
                    jQuery(this).siblings(".accordion-content").slideDown();
                    jQuery(this).parent().find(".accordion-head-sign i").addClass("icon-ok-sign").removeClass("icon-question-sign");
                    jQuery(this).parent().siblings("li").children(".accordion-content").slideUp();
                    jQuery(this).parent().siblings("li").removeClass("active");
                    jQuery(this).parent().siblings("li").find(".accordion-head-sign i").removeClass("icon-ok-sign").addClass("icon-question-sign");
                } else {
                    jQuery(this).siblings(".accordion-content").slideDown();
                    jQuery(this).parent().find(".accordion-head-sign i").addClass("icon-minus-sign").removeClass("icon-plus-sign");
                    jQuery(this).parent().siblings("li").children(".accordion-content").slideUp();
                    jQuery(this).parent().siblings("li").removeClass("active");
                    jQuery(this).parent().siblings("li").find(".accordion-head-sign i").removeClass("icon-minus-sign").addClass("icon-plus-sign");
                }
            });
        });
        jQuery("ul.tt-toggle").find(".toggle-content.active").siblings(".toggle-head").trigger("click");
        var snapper;
        function initSnapper() {
            if ($(window).innerWidth() <= 959) {
                if (snapper === undefined) {
                    snapper = new Snap({
                        element: document.getElementById("layout"),
                        transitionSpeed: .6,
                        disable: "right"
                    });
                } else {
                    snapper.enable();
                }
            } else if (snapper !== undefined) {
                snapper.disable();
                snapper.close("left");
            }
        }
        $(window).resize(initSnapper);
        initSnapper();
        $("#header nav").prepend('<div id="open-left"><i class="icon-reorder"></i></div>');
        $("body").prepend('<div class="snap-drawers"><div class="snap-drawer snap-drawer-left"><ul></ul></div></div>');
        var a = $(".sf-menu").html();
        $(".snap-drawer ul").html(a);
        $("#open-left").click(function() {
            if ($("body").hasClass("snapjs-left")) {
                snapper.close("left");
            } else if ($("body:not(.snapjs-left)")) {
                snapper.open("left");
            } else {}
        });
        jQuery("#toTop").click(function() {
            jQuery("body,html").animate({
                scrollTop: 0
            }, 1e3);
        });
        jQuery("#toTop").addClass("hidett");
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() < 400) {
                jQuery("#toTop").addClass("hidett").removeClass("showtt");
            } else {
                jQuery("#toTop").removeClass("hidett").addClass("showtt");
            }
        });
        $(".notification-close").click(function() {
            $(this).parent().slideUp("slow");
            return false;
        });
        if ($(".flex-slide-h")[0]) {
            jQuery(".flex-slide-h").flexslider({
                animation: "slide",
                direction: "horizontal",
                slideshowSpeed: 8e3,
                animationSpeed: 1400,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                randomize: false,
                smoothHeight: true,
                keyboardNav: true,
                start: function(slider) {
                    $(".flex-active-slide").find("h3").delay(100).addClass("effect").fadeIn(400);
                    $(".flex-active-slide").find("p").delay(100).addClass("effectt").fadeIn(400);
                },
                before: function(slider) {
                    $(".big-slider h3").removeClass("effect").addClass("Out").fadeOut("slow");
                    $(".big-slider p").removeClass("effectt").addClass("Out").fadeOut("slow");
                },
                after: function(slider) {
                    $(".flex-active-slide").find("h3").delay(100).addClass("effect").fadeIn(400).removeClass("Out");
                    $(".flex-active-slide").find("p").delay(100).addClass("effectt").fadeIn(400).removeClass("Out");
                }
            });
        }
        if ($(".flex-slide-v")[0]) {
            jQuery(".flex-slide-h").flexslider({
                animation: "slide",
                direction: "horizontal",
                slideshowSpeed: 8e3,
                animationSpeed: 1400,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                randomize: false,
                smoothHeight: true,
                keyboardNav: true,
                start: function(slider) {
                    $(".flex-active-slide").find("h3").delay(100).addClass("effect").fadeIn(400);
                    $(".flex-active-slide").find("p").delay(100).addClass("effectt").fadeIn(400);
                },
                before: function(slider) {
                    $(".big-slider h3").removeClass("effect").addClass("Out").fadeOut("slow");
                    $(".big-slider p").removeClass("effectt").addClass("Out").fadeOut("slow");
                },
                after: function(slider) {
                    $(".flex-active-slide").find("h3").delay(100).addClass("effect").fadeIn(400).removeClass("Out");
                    $(".flex-active-slide").find("p").delay(100).addClass("effectt").fadeIn(400).removeClass("Out");
                }
            });
        }
        if ($(".flex-slide-fade")[0]) {
            jQuery(".flex-slide-h").flexslider({
                animation: "slide",
                direction: "horizontal",
                slideshowSpeed: 8e3,
                animationSpeed: 1400,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                randomize: false,
                smoothHeight: true,
                keyboardNav: true,
                start: function(slider) {
                    $(".flex-active-slide").find("h3").delay(100).addClass("effect").fadeIn(400);
                    $(".flex-active-slide").find("p").delay(100).addClass("effectt").fadeIn(400);
                },
                before: function(slider) {
                    $(".big-slider h3").removeClass("effect").addClass("Out").fadeOut("slow");
                    $(".big-slider p").removeClass("effectt").addClass("Out").fadeOut("slow");
                },
                after: function(slider) {
                    $(".flex-active-slide").find("h3").delay(100).addClass("effect").fadeIn(400).removeClass("Out");
                    $(".flex-active-slide").find("p").delay(100).addClass("effectt").fadeIn(400).removeClass("Out");
                }
            });
        }
        if ($(".projectslider")[0]) {
            jQuery(".projectslider").flexslider({
                animation: "fade",
                direction: "horizontal",
                slideshowSpeed: 8e3,
                animationSpeed: 1e3,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                initDelay: 0,
                randomize: false,
                smoothHeight: true,
                keyboardNav: false
            });
        }
        if ($("[class^='product_']")[0]) {
            jQuery('[class^="product_"]').flexslider({
                slideshow: false,
                touch: true,
                animation: "slide",
                direction: "horizontal",
                animationSpeed: 1e3,
                directionNav: true,
                controlNav: false,
                randomize: false,
                smoothHeight: true
            });
        }
        if ($(".tst")[0]) {
            jQuery(".tst").flexslider({
                animation: "slide",
                direction: "horizontal",
                slideshowSpeed: 8e3,
                animationSpeed: 1e3,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                initDelay: 0,
                randomize: false,
                smoothHeight: true,
                keyboardNav: false
            });
        }
        if ($(".sec_testimonials")[0]) {
            jQuery(".sec_testimonials").flexslider({
                animation: "slide",
                direction: "horizontal",
                slideshowSpeed: 8e3,
                animationSpeed: 1e3,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                initDelay: 0,
                randomize: false,
                smoothHeight: true,
                keyboardNav: false
            });
        }
        if ($(".tstFade")[0]) {
            jQuery(".tstFade").flexslider({
                animation: "fade",
                slideshowSpeed: 8e3,
                animationSpeed: 800,
                directionNav: true,
                controlNav: false,
                pauseOnHover: true,
                initDelay: 0,
                randomize: false,
                smoothHeight: true,
                keyboardNav: false
            });
        }
        if ($("#iview")[0]) {
            jQuery("#iview").iView({
                pauseTime: 7e3,
                directionNav: false,
                controlNav: true,
                tooltipY: -15
            });
        }
        if ($(".portfolio-carousel")[0]) {
            jQuery(".portfolio-carousel").jCarouselLite({
                btnNext: ".portfolio-carousel .nexte",
                btnPrev: ".portfolio-carousel .preve",
                easing: "easeInOutExpo",
                visible: 4,
                scroll: 1,
                hoverPause: true,
                auto: 2e3,
                speed: 800
            });
        }
        if ($(".magazine-carousel")[0]) {
            jQuery(".magazine-carousel").jCarouselLite({
                btnNext: ".magazine-carousel .nexte",
                btnPrev: ".magazine-carousel .preve",
                easing: "easeInOutBack",
                scroll: 1,
                hoverPause: true,
                auto: 3e3,
                speed: 700
            });
        }
        if ($(".client-carousel")[0]) {
            jQuery(".client-carousel").jCarouselLite({
                btnNext: ".client-carousel .nexte",
                btnPrev: ".client-carousel .preve",
                easing: "easeInOutBack",
                visible: 4,
                scroll: 1,
                hoverPause: true,
                auto: 4e3,
                speed: 600
            });
        }
        if ($(".products_carousel")[0]) {
            jQuery(".products_carousel").jCarouselLite({
                btnNext: ".products_carousel .nexte",
                btnPrev: ".products_carousel .preve",
                easing: "easeInOutExpo",
                visible: 4,
                scroll: 1,
                hoverPause: true,
                auto: 2e3,
                speed: 800
            });
        }
        if ($("a[data-gal^='lightbox']")[0]) {
            $("a[data-gal^='lightbox']").prettyPhoto({
                animation_speed: "normal",
                theme: "dark_rounded",
                autoplay_slideshow: false,
                overlay_gallery: false,
                show_title: false
            });
        }
        if ($("#contactForm")[0]) {
            $("#contactForm").submit(function() {
                $("#contactForm .error").remove();
                $("#contactForm .requiredField").removeClass("fielderror");
                $("#contactForm .requiredField").addClass("fieldtrue");
                $("#contactForm span strong").remove();
                var hasError = false;
                $("#contactForm .requiredField").each(function() {
                    if (jQuery.trim($(this).val()) === "") {
                        var labelText = $(this).prev("label").text();
                        $(this).addClass("fielderror");
                        $("#contactForm span").html("<strong>*Please fill out all fields.</strong>");
                        hasError = true;
                    } else if ($(this).hasClass("email")) {
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if (!emailReg.test(jQuery.trim($(this).val()))) {
                            var labelText = $(this).prev("label").text();
                            $(this).addClass("fielderror");
                            $("#contactForm span").html("<strong>Is incorrect your email address</strong>");
                            hasError = true;
                        }
                    }
                });
                if (!hasError) {
                    $("#contactForm").slideDown("normal", function() {
                        $("#contactForm #sendMessage").addClass("load-color");
                        $("#contactForm #sendMessage").attr("disabled", "disabled").addClass("btn-success").val("Sending message. Please wait...");
                    });
                    var formInput = $(this).serialize();
                    $.post($(this).attr("action"), formInput, function(data) {
                        $("#contactForm").slideUp("normal", function() {
                            $(this).before('<div class="notification-box notification-box-success"><p><i class="icon-ok"></i>Thanks!</strong> Your email was successfully sent. You will get revert within 24 hours.</p></div>');
                        });
                    });
                }
                return false;
            });
        }
        if ($("#contactForm-widget")[0]) {
            $("#contactForm-widget").submit(function() {
                $("#contactForm-widget .error").remove();
                $("#contactForm-widget .requiredField").removeClass("fielderror");
                $("#contactForm-widget .requiredField").addClass("fieldtrue");
                $("#contactForm-widget span strong").remove();
                var hasError = false;
                $("#contactForm-widget .requiredField").each(function() {
                    if (jQuery.trim($(this).val()) === "") {
                        var labelText = $(this).prev("label").text();
                        $(this).addClass("fielderror");
                        $("#contactForm-widget span").html("<strong>*Please fill out all fields.</strong>");
                        hasError = true;
                    } else if ($(this).hasClass("email")) {
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if (!emailReg.test(jQuery.trim($(this).val()))) {
                            var labelText = $(this).prev("label").text();
                            $(this).addClass("fielderror");
                            $("#contactForm-widget span").html("<strong>Is incorrect your email address</strong>");
                            hasError = true;
                        }
                    }
                });
                if (!hasError) {
                    $("#contactForm-widget").slideDown("normal", function() {
                        $("#contactForm-widget #sendMessage").addClass("load-color");
                        $("#contactForm-widget #sendMessage").attr("disabled", "disabled").val("Sending message. Please wait...");
                        $("#contactForm-widget span").html('<i class="icon-spinner icon-spin"></i>');
                    });
                    var formInput = $(this).serialize();
                    $.post($(this).attr("action"), formInput, function(data) {
                        $("#contactForm-widget").slideUp("normal", function() {
                            $(this).before('<div class="notification-box notification-box-success"><p><i class="icon-ok"></i>Thanks!</strong> Your email was successfully sent. You will get revert within 24 hours.</p></div>');
                        });
                    });
                }
                return false;
            });
        }
        $(".toptip").tipsy({
            fade: true,
            gravity: "s"
        });
        $(".bottomtip").tipsy({
            fade: true,
            gravity: "n"
        });
        $(".righttip").tipsy({
            fade: true,
            gravity: "w"
        });
        $(".lefttip").tipsy({
            fade: true,
            gravity: "e"
        });
        var w_width = $(window).width();
        if (w_width >= 960) {
            if ($(".animated")[0]) {
                jQuery(".animated").css("opacity", "0");
            }
            jQuery(".animt").each(function() {
                var $curr = jQuery(this);
                var $currOffset = $curr.attr("data-gen-offset");
                if ($currOffset === "" || $currOffset === "undefined" || $currOffset === undefined) {
                    $currOffset = "bottom-in-view";
                }
                $curr.waypoint(function() {
                    $curr.trigger("animt");
                }, {
                    triggerOnce: true,
                    offset: $currOffset
                });
            });
            jQuery(".animated").each(function() {
                var $curr = jQuery(this);
                $curr.bind("animt", function() {
                    $curr.css("opacity", "");
                    $curr.addClass($curr.data("gen"));
                });
            });
            jQuery(".animated").each(function() {
                var $curr = jQuery(this);
                var $currOffset = $curr.attr("data-gen-offset");
                if ($currOffset === "" || $currOffset === "undefined" || $currOffset === undefined) {
                    $currOffset = "bottom-in-view";
                }
                $curr.waypoint(function() {
                    $curr.trigger("animt");
                }, {
                    triggerOnce: true,
                    offset: $currOffset
                });
            });
        }
        if ($(".progress-bar > span")[0]) {
            $(".progress-bar > span").waypoint(function() {
                $(this).each(function() {
                    $(this).animate({
                        width: $(this).attr("rel") + "%"
                    }, 800);
                });
            }, {
                triggerOnce: true,
                offset: "bottom-in-view"
            });
        }
        if ($("ul#news")[0]) {
            jQuery("ul#news").liScroll({
                travelocity: .08
            });
        }
        if ($(".my_sticky")[0]) {
            $(".my_sticky").before('<div class="Corpse_Sticky"></div>');
            $(window).scroll(function() {
                var wind_scr = $(window).scrollTop();
                var window_width = $(window).width();
                var head_w = $(".my_sticky").height();
                if (window_width >= 959) {
                    if (wind_scr < 200) {
                        if ($(".my_sticky").data("sticky") === true) {
                            $(".my_sticky").data("sticky", false);
                            $(".my_sticky").stop(true).animate({
                                opacity: 0
                            }, 300, function() {
                                $(".my_sticky").removeClass("sticky");
                                $(".my_sticky").stop(true).animate({
                                    opacity: 1
                                }, 300);
                                $(".Corpse_Sticky").css("padding-top", "");
                            });
                        }
                    } else {
                        if ($(".my_sticky").data("sticky") === false || typeof $(".my_sticky").data("sticky") === "undefined") {
                            $(".my_sticky").data("sticky", true);
                            $(".my_sticky").stop(true).animate({
                                opacity: 0
                            }, 300, function() {
                                $(".my_sticky").addClass("sticky");
                                $(".my_sticky.sticky").stop(true).animate({
                                    opacity: 1
                                }, 300);
                                $(".Corpse_Sticky").css("padding-top", head_w + "px");
                            });
                        }
                    }
                }
            });
            $(window).resize(function() {
                var window_width = $(window).width();
                if (window_width <= 959) {
                    if ($(".my_sticky").hasClass("sticky")) {
                        $(".my_sticky").removeClass("sticky");
                        $(".my_sticky").stop(true).animate({
                            opacity: 0
                        }, 300, function() {
                            $(".my_sticky").removeClass("sticky");
                            $(".my_sticky").stop(true).animate({
                                opacity: 1
                            }, 300);
                            $(".Corpse_Sticky").css("padding-top", "");
                        });
                    }
                }
            });
        }
        $.stellar({
            horizontalScrolling: false,
            verticalOffset: 0
        });
        $("html").niceScroll({
            zindex: 999,
            cursorborder: "",
            cursorborderradius: "2px",
            cursorcolor: "#191919",
            cursoropacitymin: .5
        });
        function initNice() {
            if ($(window).innerWidth() <= 960) {
                $("html").niceScroll().remove();
            } else {
                $("html").niceScroll({
                    zindex: 999,
                    cursorborder: "",
                    cursorborderradius: "2px",
                    cursorcolor: "#191919",
                    cursoropacitymin: .5
                });
            }
        }
        $(window).load(initNice);
        $(window).resize(initNice);
        var url = window.location.href;
        $(".sf-menu a").filter(function() {
            return this.href === url;
        }).parent().addClass("current");
    });
    (function() {
        var t = [].indexOf || function(t) {
            for (var e = 0, n = this.length; e < n; e++) {
                if (e in this && this[e] === t) return e;
            }
            return -1;
        }, e = [].slice;
        (function(t, e) {
            if (typeof define === "function" && define.amd) {
                return define("waypoints", [ "jquery" ], function(n) {
                    return e(n, t);
                });
            } else {
                return e(t.jQuery, t);
            }
        })(this, function(n, r) {
            var i, o, l, s, f, u, a, c, h, d, p, y, v, w, g, m;
            i = n(r);
            c = t.call(r, "ontouchstart") >= 0;
            s = {
                horizontal: {},
                vertical: {}
            };
            f = 1;
            a = {};
            u = "waypoints-context-id";
            p = "resize.waypoints";
            y = "scroll.waypoints";
            v = 1;
            w = "waypoints-waypoint-ids";
            g = "waypoint";
            m = "waypoints";
            o = function() {
                function t(t) {
                    var e = this;
                    this.$element = t;
                    this.element = t[0];
                    this.didResize = false;
                    this.didScroll = false;
                    this.id = "context" + f++;
                    this.oldScroll = {
                        x: t.scrollLeft(),
                        y: t.scrollTop()
                    };
                    this.waypoints = {
                        horizontal: {},
                        vertical: {}
                    };
                    t.data(u, this.id);
                    a[this.id] = this;
                    t.bind(y, function() {
                        var t;
                        if (!(e.didScroll || c)) {
                            e.didScroll = true;
                            t = function() {
                                e.doScroll();
                                return e.didScroll = false;
                            };
                            return r.setTimeout(t, n[m].settings.scrollThrottle);
                        }
                    });
                    t.bind(p, function() {
                        var t;
                        if (!e.didResize) {
                            e.didResize = true;
                            t = function() {
                                n[m]("refresh");
                                return e.didResize = false;
                            };
                            return r.setTimeout(t, n[m].settings.resizeThrottle);
                        }
                    });
                }
                t.prototype.doScroll = function() {
                    var t, e = this;
                    t = {
                        horizontal: {
                            newScroll: this.$element.scrollLeft(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left"
                        },
                        vertical: {
                            newScroll: this.$element.scrollTop(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up"
                        }
                    };
                    if (c && (!t.vertical.oldScroll || !t.vertical.newScroll)) {
                        n[m]("refresh");
                    }
                    n.each(t, function(t, r) {
                        var i, o, l;
                        l = [];
                        o = r.newScroll > r.oldScroll;
                        i = o ? r.forward : r.backward;
                        n.each(e.waypoints[t], function(t, e) {
                            var n, i;
                            if (r.oldScroll < (n = e.offset) && n <= r.newScroll) {
                                return l.push(e);
                            } else if (r.newScroll < (i = e.offset) && i <= r.oldScroll) {
                                return l.push(e);
                            }
                        });
                        l.sort(function(t, e) {
                            return t.offset - e.offset;
                        });
                        if (!o) {
                            l.reverse();
                        }
                        return n.each(l, function(t, e) {
                            if (e.options.continuous || t === l.length - 1) {
                                return e.trigger([ i ]);
                            }
                        });
                    });
                    return this.oldScroll = {
                        x: t.horizontal.newScroll,
                        y: t.vertical.newScroll
                    };
                };
                t.prototype.refresh = function() {
                    var t, e, r, i = this;
                    r = n.isWindow(this.element);
                    e = this.$element.offset();
                    this.doScroll();
                    t = {
                        horizontal: {
                            contextOffset: r ? 0 : e.left,
                            contextScroll: r ? 0 : this.oldScroll.x,
                            contextDimension: this.$element.width(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left",
                            offsetProp: "left"
                        },
                        vertical: {
                            contextOffset: r ? 0 : e.top,
                            contextScroll: r ? 0 : this.oldScroll.y,
                            contextDimension: r ? n[m]("viewportHeight") : this.$element.height(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up",
                            offsetProp: "top"
                        }
                    };
                    return n.each(t, function(t, e) {
                        return n.each(i.waypoints[t], function(t, r) {
                            var i, o, l, s, f;
                            i = r.options.offset;
                            l = r.offset;
                            o = n.isWindow(r.element) ? 0 : r.$element.offset()[e.offsetProp];
                            if (n.isFunction(i)) {
                                i = i.apply(r.element);
                            } else if (typeof i === "string") {
                                i = parseFloat(i);
                                if (r.options.offset.indexOf("%") > -1) {
                                    i = Math.ceil(e.contextDimension * i / 100);
                                }
                            }
                            r.offset = o - e.contextOffset + e.contextScroll - i;
                            if (r.options.onlyOnScroll && l != null || !r.enabled) {
                                return;
                            }
                            if (l !== null && l < (s = e.oldScroll) && s <= r.offset) {
                                return r.trigger([ e.backward ]);
                            } else if (l !== null && l > (f = e.oldScroll) && f >= r.offset) {
                                return r.trigger([ e.forward ]);
                            } else if (l === null && e.oldScroll >= r.offset) {
                                return r.trigger([ e.forward ]);
                            }
                        });
                    });
                };
                t.prototype.checkEmpty = function() {
                    if (n.isEmptyObject(this.waypoints.horizontal) && n.isEmptyObject(this.waypoints.vertical)) {
                        this.$element.unbind([ p, y ].join(" "));
                        return delete a[this.id];
                    }
                };
                return t;
            }();
            l = function() {
                function t(t, e, r) {
                    var i, o;
                    r = n.extend({}, n.fn[g].defaults, r);
                    if (r.offset === "bottom-in-view") {
                        r.offset = function() {
                            var t;
                            t = n[m]("viewportHeight");
                            if (!n.isWindow(e.element)) {
                                t = e.$element.height();
                            }
                            return t - n(this).outerHeight();
                        };
                    }
                    this.$element = t;
                    this.element = t[0];
                    this.axis = r.horizontal ? "horizontal" : "vertical";
                    this.callback = r.handler;
                    this.context = e;
                    this.enabled = r.enabled;
                    this.id = "waypoints" + v++;
                    this.offset = null;
                    this.options = r;
                    e.waypoints[this.axis][this.id] = this;
                    s[this.axis][this.id] = this;
                    i = (o = t.data(w)) != null ? o : [];
                    i.push(this.id);
                    t.data(w, i);
                }
                t.prototype.trigger = function(t) {
                    if (!this.enabled) {
                        return;
                    }
                    if (this.callback != null) {
                        this.callback.apply(this.element, t);
                    }
                    if (this.options.triggerOnce) {
                        return this.destroy();
                    }
                };
                t.prototype.disable = function() {
                    return this.enabled = false;
                };
                t.prototype.enable = function() {
                    this.context.refresh();
                    return this.enabled = true;
                };
                t.prototype.destroy = function() {
                    delete s[this.axis][this.id];
                    delete this.context.waypoints[this.axis][this.id];
                    return this.context.checkEmpty();
                };
                t.getWaypointsByElement = function(t) {
                    var e, r;
                    r = n(t).data(w);
                    if (!r) {
                        return [];
                    }
                    e = n.extend({}, s.horizontal, s.vertical);
                    return n.map(r, function(t) {
                        return e[t];
                    });
                };
                return t;
            }();
            d = {
                init: function(t, e) {
                    var r;
                    if (e == null) {
                        e = {};
                    }
                    if ((r = e.handler) == null) {
                        e.handler = t;
                    }
                    this.each(function() {
                        var t, r, i, s;
                        t = n(this);
                        i = (s = e.context) != null ? s : n.fn[g].defaults.context;
                        if (!n.isWindow(i)) {
                            i = t.closest(i);
                        }
                        i = n(i);
                        r = a[i.data(u)];
                        if (!r) {
                            r = new o(i);
                        }
                        return new l(t, r, e);
                    });
                    n[m]("refresh");
                    return this;
                },
                disable: function() {
                    return d._invoke(this, "disable");
                },
                enable: function() {
                    return d._invoke(this, "enable");
                },
                destroy: function() {
                    return d._invoke(this, "destroy");
                },
                prev: function(t, e) {
                    return d._traverse.call(this, t, e, function(t, e, n) {
                        if (e > 0) {
                            return t.push(n[e - 1]);
                        }
                    });
                },
                next: function(t, e) {
                    return d._traverse.call(this, t, e, function(t, e, n) {
                        if (e < n.length - 1) {
                            return t.push(n[e + 1]);
                        }
                    });
                },
                _traverse: function(t, e, i) {
                    var o, l;
                    if (t == null) {
                        t = "vertical";
                    }
                    if (e == null) {
                        e = r;
                    }
                    l = h.aggregate(e);
                    o = [];
                    this.each(function() {
                        var e;
                        e = n.inArray(this, l[t]);
                        return i(o, e, l[t]);
                    });
                    return this.pushStack(o);
                },
                _invoke: function(t, e) {
                    t.each(function() {
                        var t;
                        t = l.getWaypointsByElement(this);
                        return n.each(t, function(t, n) {
                            n[e]();
                            return true;
                        });
                    });
                    return this;
                }
            };
            n.fn[g] = function() {
                var t, r;
                r = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
                if (d[r]) {
                    return d[r].apply(this, t);
                } else if (n.isFunction(r)) {
                    return d.init.apply(this, arguments);
                } else if (n.isPlainObject(r)) {
                    return d.init.apply(this, [ null, r ]);
                } else if (!r) {
                    return n.error("jQuery Waypoints needs a callback function or handler option.");
                } else {
                    return n.error("The " + r + " method does not exist in jQuery Waypoints.");
                }
            };
            n.fn[g].defaults = {
                context: r,
                continuous: true,
                enabled: true,
                horizontal: false,
                offset: 0,
                triggerOnce: false
            };
            h = {
                refresh: function() {
                    return n.each(a, function(t, e) {
                        return e.refresh();
                    });
                },
                viewportHeight: function() {
                    var t;
                    return (t = r.innerHeight) != null ? t : i.height();
                },
                aggregate: function(t) {
                    var e, r, i;
                    e = s;
                    if (t) {
                        e = (i = a[n(t).data(u)]) != null ? i.waypoints : void 0;
                    }
                    if (!e) {
                        return [];
                    }
                    r = {
                        horizontal: [],
                        vertical: []
                    };
                    n.each(r, function(t, i) {
                        n.each(e[t], function(t, e) {
                            return i.push(e);
                        });
                        i.sort(function(t, e) {
                            return t.offset - e.offset;
                        });
                        r[t] = n.map(i, function(t) {
                            return t.element;
                        });
                        return r[t] = n.unique(r[t]);
                    });
                    return r;
                },
                above: function(t) {
                    if (t == null) {
                        t = r;
                    }
                    return h._filter(t, "vertical", function(t, e) {
                        return e.offset <= t.oldScroll.y;
                    });
                },
                below: function(t) {
                    if (t == null) {
                        t = r;
                    }
                    return h._filter(t, "vertical", function(t, e) {
                        return e.offset > t.oldScroll.y;
                    });
                },
                left: function(t) {
                    if (t == null) {
                        t = r;
                    }
                    return h._filter(t, "horizontal", function(t, e) {
                        return e.offset <= t.oldScroll.x;
                    });
                },
                right: function(t) {
                    if (t == null) {
                        t = r;
                    }
                    return h._filter(t, "horizontal", function(t, e) {
                        return e.offset > t.oldScroll.x;
                    });
                },
                enable: function() {
                    return h._invoke("enable");
                },
                disable: function() {
                    return h._invoke("disable");
                },
                destroy: function() {
                    return h._invoke("destroy");
                },
                extendFn: function(t, e) {
                    return d[t] = e;
                },
                _invoke: function(t) {
                    var e;
                    e = n.extend({}, s.vertical, s.horizontal);
                    return n.each(e, function(e, n) {
                        n[t]();
                        return true;
                    });
                },
                _filter: function(t, e, r) {
                    var i, o;
                    i = a[n(t).data(u)];
                    if (!i) {
                        return [];
                    }
                    o = [];
                    n.each(i.waypoints[e], function(t, e) {
                        if (r(i, e)) {
                            return o.push(e);
                        }
                    });
                    o.sort(function(t, e) {
                        return t.offset - e.offset;
                    });
                    return n.map(o, function(t) {
                        return t.element;
                    });
                }
            };
            n[m] = function() {
                var t, n;
                n = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
                if (h[n]) {
                    return h[n].apply(null, t);
                } else {
                    return h.aggregate.call(null, n);
                }
            };
            n[m].settings = {
                resizeThrottle: 100,
                scrollThrottle: 30
            };
            return i.load(function() {
                return n[m]("refresh");
            });
        });
    }).call(this);
    jQuery(function($) {
        function csrfSafeMethod(method) {
            return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
        }
        function sameOrigin(url) {
            var host = document.location.host;
            var protocol = document.location.protocol;
            var sr_origin = "//" + host;
            var origin = protocol + sr_origin;
            return url === origin || url.slice(0, origin.length + 1) === origin + "/" || (url === sr_origin || url.slice(0, sr_origin.length + 1) === sr_origin + "/") || !/^(\/\/|http:|https:).*/.test(url);
        }
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== "") {
                var cookies = document.cookie.split(";");
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) === name + "=") {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        var csrftoken = getCookie("csrftoken");
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });
    });
}).call(this);
//@ sourceMappingURL=main.map