<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'RWMB_Text_Field' ) )
{
	class RWMB_Text_Field
	{
        /**
         * Enqueue scripts and styles
         *
         * @return void
         */
        static function admin_enqueue_scripts()
        {
            $urlcss = plugins_url('/3d-slider/css');
            $urljs = plugins_url('/3d-slider/js');
            wp_enqueue_style( 'fontselect', "{$urlcss}/fontselect.css" );
            wp_enqueue_script( 'fontselect', "{$urljs}/jquery.fontselect.min.js", array( 'jquery' ), '1.0', true );
            wp_enqueue_script( 'fontselect-inject', "{$urljs}/fontselect.js", array( 'jquery','fontselect' ), '1.0', true );
        }

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			return sprintf(
				'<input type="text" class="rwmb-text" name="%s" id="%s" value="%s" size="%s" />',
				$field['field_name'],
				$field['id'],
				$meta,
				$field['size']
			);
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = wp_parse_args( $field, array(
				'size' => 30,
			) );
			return $field;
		}
	}
}