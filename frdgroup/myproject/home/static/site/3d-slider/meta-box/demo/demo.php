<?php
    /**
    * Registering meta boxes
    *
    * All the definitions of meta boxes are listed below with comments.
    * Please read them CAREFULLY.
    *
    * You also should read the changelog to know what has been changed before updating.
    *
    * For more information, please visit:
    * @link http://www.deluxeblogtips.com/meta-box/
    */

    /********************* META BOX DEFINITIONS ***********************/

    /**
    * Prefix of meta keys (optional)
    * Use underscore (_) at the beginning to make keys hidden
    * Alt.: You also can make prefix empty to disable it
    */
    // Better has an underscore as last sign
    $prefix = 'SD3D_';

    global $meta_boxes;

    $meta_boxes = array();

    // 1st meta box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'       => 'standard',

        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'    => 'General settings',

        // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
        'pages'    => array('threedee_slider'),

        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'  => 'normal',

        // Order of meta box: high (default), low. Optional.
        'priority' => 'high',

        // List of meta fields
        'fields'   => array(
            array(
                'name'  => 'Slider shortcode',
                'id'    => "{$prefix}shortcode",
                'desc'  => 'Use this shortcode.',
                'type'  => 'code',
                'std'   => '',
                'clone' => false,
            ),
            // THICKBOX IMAGE UPLOAD (WP 3.3+)
            array(
                'name' => 'Slideshow images',
                'id'   => "{$prefix}upload_images",
                'desc' => 'Upload images you want to display in your slideshow.',
                'type' => 'thickbox_image',
            ),            
            array(
                'name'        => 'Width of the slider (px).',
                'id'          => "{$prefix}sliderWidth",
                'description' => 'Set slider width.',
                'type'        => 'number',
                'std'         => '600',
                'min'         => 200,
                'step'        => 10,
            ),
            array(
                'name'        => 'Height of the slider (px).',
                'id'          => "{$prefix}sliderHeight",
                'description' => 'Set slider height.',
                'type'        => 'number',
                'std'         => '300',
                'min'         => 200,
                'step'        => 10,
            ),
            array(
                'name'  => 'Title font',
                'id'    => "{$prefix}googleFont",
                'desc'  => 'Choose Google font of the caption title.',
                'type'  => 'text',
                'std'   => 'Anton',
                'clone' => false,
            ),
            array(
                'name'        => 'Title font size (px).',
                'id'          => "{$prefix}googleFontsize",
                'description' => 'Set title font size.',
                'type'        => 'number',
                'std'         => '14',
                'min'         => 1,
                'step'        => 1,
            ),            
            array(
                'name'        => 'Display thumbnails navigation?',
                'id'          => "{$prefix}displayThumbs",
                'description' => 'Do you want to display thumbnails navigation?',
                'type'        => 'checkbox',
                'std'         => 1,
            ),
            array(
                'name'        => 'Display arrow navigation?',
                'id'          => "{$prefix}displayArrows",
                'description' => 'Do you want to display arrow navigation?',
                'type'        => 'checkbox',
                'std'         => 1,
            ),
            array(
                'name'        => 'Display animated circle timer?',
                'id'          => "{$prefix}displayTimer",
                'description' => 'Do you want to display animatd circle timer?',
                'type'        => 'checkbox',
                'std'         => 1,
            ),
            array(
                'name'        => 'Display animated bartimer?',
                'id'          => "{$prefix}bartimer",
                'description' => 'Do you want to display animated bartimer?',
                'type'        => 'checkbox',
                'std'         => 1,
            ),
            array(
                'name'        => 'Autoplay?',
                'id'          => "{$prefix}autoplay",
                'description' => 'Do you want start slider automatically?',
                'type'        => 'checkbox',
                'std'         => 1,
            ),
            array(
                'name'        => 'Display shadow?',
                'id'          => "{$prefix}shadow",
                'description' => 'Do you want display shadow?',
                'type'        => 'checkbox',
                'std'         => 1,
            ),            
            array(
                'name'  => 'Custom CSS',
                'id'    => "{$prefix}customCSS",
                'desc'  => 'Custom CSS. For example if you have slider named "My slider", the shortcode will be [threedee_slider name="my-slider"]. Then just take the name "my-slider" and use it for title size in the CSS:  <pre>#my-slider .slider-title { text-shadow: 1px 1px 1px #c0c0c0; }</pre>' ,
                'type'  => 'textarea',
                'std'   => 'customCSS',
                'clone' => false,
            ),
           
            
    ));

    /********************* META BOX REGISTERING ***********************/

    /**
    * Register meta boxes
    *
    * @return void
    */
    function SDES_register_meta_boxes()
    {
        // Make sure there's no errors when the plugin is deactivated or during upgrade
        if (!class_exists('RW_Meta_Box'))
            return;

        global $meta_boxes;
        foreach ($meta_boxes as $meta_box) {
            new RW_Meta_Box($meta_box);
        }
    }

    // Hook to 'admin_init' to make sure the meta box class is loaded before
    // (in case using the meta box class in another plugin)
    // This is also helpful for some conditionals like checking page template, categories, etc.
    add_action('admin_init', 'SDES_register_meta_boxes');
