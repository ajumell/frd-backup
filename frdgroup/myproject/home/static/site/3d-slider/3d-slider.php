<?php
	/*
	Plugin Name: 3D Slider
	Plugin URI: http://sramekdesign.com
	Description: 3D Slider implements powerful 3D slider script.
	Version: 1.1.2
	Author: Tom Sramek
	Author URI: http://sramekdesign.com
	*/

	$GLOBALS['csPluginPath'] = plugins_url('', __FILE__);
	include('meta-box/meta-box.php');
	include('meta-box/demo/demo.php');
	require_once('aq_resizer.php');


	// Enqueue javascript 

	function sramekdesign_threedee_slider_load_scripts()
	{
		wp_enqueue_style('slider-style', plugins_url('/templates/text-block/style/slider-style.css', __FILE__));

		wp_enqueue_script('jquery');	
		wp_enqueue_script('modernizr', 'http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js', array(), '2.6.2');	
		wp_enqueue_script('cuteslider', $GLOBALS['csPluginPath'].'/js/cute.slider.js', array(), '1.1.1');
		wp_enqueue_script('cuteslider_transitions', $GLOBALS['csPluginPath'].'/js/cute.transitions.all.js', array(), '1.1.1' );
		wp_localize_script( 'cuteslider', 'CSSettings', array( 'pluginPath' => $GLOBALS['csPluginPath'] ) );

	}

	add_action('wp_enqueue_scripts', 'sramekdesign_threedee_slider_load_scripts');

	// Create slider post type
	add_action('init', 'sramekdesign_threedee_slider_create_post_type');

	function sramekdesign_threedee_slider_create_post_type()
	{
		register_post_type('threedee_slider',
			array(
				'labels'      => array(
					'name'          => __('3D Sliders'),
					'singular_name' => __('Slider'),
				),
				'public'      => true,
				'has_archive' => true,
				'show_ui'     => true,
				'supports'    => array('title')
			)
		);
	}
if (!function_exists('get_post_id')) {
	function get_post_id($slug, $post_type)
	{
		$query = new WP_Query(
			array(
				'name'      => $slug,
				'post_type' => $post_type
			)
		);

		$query->the_post();

		return get_the_ID();
	}

}
	// shortcode
	add_action('init', 'register_shortcodes');

	function register_shortcodes()
	{
		add_shortcode('threedee_slider', 'threedee_slider_function');
	}

	function threedee_slider_function($atts)
	{
		extract(shortcode_atts(array(
			'name' => '',
			), $atts));

		$gallery_id = get_post_id($name, 'threedee_slider');	

		$custom_fields = get_post_custom($gallery_id);
		/*print "<pre>";
		foreach ( $custom_fields as $field_key => $field_values ) {
		if(!isset($field_values[0])) continue;
		if(in_array($field_key,array("_edit_lock","_edit_last"))) continue;
		print $field_key . '=>' . $field_values[0] . "\n";
		}
		print  "</pre>";*/

		$args = array(
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'post_parent'    => $gallery_id,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_status'    => null,
			'numberposts'    => -1,

		);

		$images = get_posts($args);
		$x      = 0;

		$sliderWidth = $custom_fields['SD3D_sliderWidth'][0] + 10;
		$sliderHeight = $custom_fields['SD3D_sliderHeight'][0]+5;

		$removethis = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ":bold", ":");
		$ourfont_prepare = str_replace($removethis, '', $custom_fields['SD3D_googleFont'][0]);
		$ourfont = str_replace('+', ' ', $ourfont_prepare);
		if(isset($custom_fields['SD3D_customCSS'][0])) { $customCSS = $custom_fields['SD3D_customCSS'][0]; } else { $customCSS = ''; }
		if(isset($custom_fields['sd3d_video_link'][0])) { $sd3d_video_link = $custom_fields['sd3d_video_link'][0]; } else { $sd3d_video_link = ''; }
		if(isset($custom_fields['_wp_attachment_image_alt'][0])) { $wp_attachment_image_alt = $custom_fields['_wp_attachment_image_alt'][0]; } else { $wp_attachment_image_alt = ''; }

		$return_slider = '	
		<style>
		@import url(http://fonts.googleapis.com/css?family='.$custom_fields['SD3D_googleFont'][0].');
		#'.$name.' { max-width: '.$sliderWidth.'px !important; max-height: '.$sliderHeight.'px !important; }
		#'.$name.' .slider-title { font-family: "'.$ourfont.'"; font-size: '.$custom_fields['SD3D_googleFontsize'][0].'px; }
		'.$customCSS.'
		</style>
		<div id="'.$name.'" class="sramekdesign3dslider">
		<div id="slider" class="cute-slider" data-width="'.$sliderWidth.'" data-height="'.$sliderHeight.'" data-force="" >
		<ul data-type="slides">';

		foreach ($images as $image):  $x++;
			$attachement_meta = get_post_custom($image->ID);

			$image_src = aq_resize($image->guid, $sliderWidth, $sliderHeight, true);
			$thumbnail_src = aq_resize($image->guid, 150, 150, true);

			if ($wp_attachment_image_alt!='') { $data_trans3d = $wp_attachment_image_alt; } else { $data_trans3d = 'tr63,tr64'; }
			if ($x==1) { $data_src =  'src="'.$image_src.'"'; } else { $imagepath =  plugins_url('/templates/text-block/cute-theme/blank.jpg', __FILE__); $data_src = 'src="'.$imagepath.'" ' .  'data-src="'.$image_src.'"';  }

			$return_slider .= '
			<li data-delay="4" data-trans3d="'.$data_trans3d.'" data-trans2d="tr1,tr14,tr26" style="position: relative;">
			<img '.$data_src.' data-thumb="'.$thumbnail_src.'" style="background: none !important; border: none !important; height: auto; max-width: none !important; padding: 0 0 0 0 !important;" />';		
			if ($image->post_excerpt != '') {
				$return_slider .= '<div data-type="info" class="info1" data-align="right"><div>'.$image->post_excerpt.'</div></div>';
			}
			
			if ($sd3d_video_link != '') {
				$return_slider .= '<a data-type="video" href="'.$sd3d_video_link.'"> </a>';
			}

			if (($attachement_meta['sd3d_slide_link'][0] != '') && ($image->post_excerpt == '')) {
				//if(($image->post_excerpt == '')) { $style = 'style="cursor: crosshair;"'; }
				$return_slider .= '<a data-type="link" href="'.$attachement_meta['sd3d_slide_link'][0].'" target="_blank" class="slider3d-link"> </a>';
			}			
			$return_slider .= '</li>';				

			if (($image->_wp_attachment_image_alt != '') || ($image->post_excerpt != '')) {
				$htmlcaption = '#htmlcaption-'. $name . $x;
			} else {
				$htmlcaption = '';
			}
			$return_string = '<a href="#">
			<img src="' . $image_src . '" data-thumb="' . $thumbnail_src . '" alt="" title="' . $htmlcaption . '" tyle="background: none !important; border: none !important; height: auto; max-width: none !important; padding: 0 0 0 0 !important; margin: 0 0 0 0 !important;" />
			</a>';
			
			endforeach;
		wp_reset_query();
		$return_slider .= '	</ul>
		<ul data-type="controls">	
		<li data-type="slideinfo" data-effect="fade"> </li>';	
		if ( $custom_fields['SD3D_bartimer'][0] == 1 ) {						
			$return_slider .= '		<li data-type="bartimer"> </li>';
		}	
		$return_slider .='<li data-type="link"> </li>';
		$return_slider .= '		<li data-type="video"> </li>';
		if ( $custom_fields['SD3D_displayArrows'][0] == 1 ) {
			$return_slider .= '		<li data-type="next"> </li>
			<li data-type="previous"> </li>';
		}
		if (( $custom_fields['SD3D_displayTimer'][0] == 1 ) && ( $custom_fields['SD3D_autoplay'][0] == 1 ) ) {
			$return_slider .= '		<li data-type="circletimer" data-color="#333333" data-stroke="13"> </li>';
		}
		if ( $custom_fields['SD3D_displayThumbs'][0] == 1 ) {						
			$return_slider .= '		<li data-type="slidecontrol" data-thumbalign="up"> </li>';
		}
		$return_slider .= '</ul>

		</div>';
		if ( $custom_fields['SD3D_shadow'][0] == 1 ) {				
			$return_slider .= '<div class="br-shadow"><img src="'.plugins_url('/templates/text-block/cute-theme/shadow.png', __FILE__).'" /></div>';
		}		
		$return_slider .= '</div>
		<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery(".br-slidecontrol li span").css("display", "none");
		jQuery(".br-slidecontrol li:first-child span").css("display", "block");
		});
		//<![CDATA[
		var slider = new Cute.Slider();
		slider.setup("slider" , "'.$name.'");';	
		if ( $custom_fields['SD3D_autoplay'][0] == 1 ) {					
			$return_slider .= 'slider.play();';
		} else {
			$return_slider .= 'slider.pause();';
		}

		$return_slider .= '//]]>
		</script>';

		return $return_slider;
	}

	function sd3d_video_link( $form_fields, $post ) {

		$form_fields['sd3d-video-link'] = array(
			'label' => 'Video link',
			'input' => 'text',
			'value' => get_post_meta( $post->ID, 'sd3d_video_link', true ),
			'helps' => '3D slider plugin specific field. Vimeo or Youtube.',
		);


		return $form_fields;
	}

	add_filter( 'attachment_fields_to_edit', 'sd3d_video_link', 10, 2 );

	function sd3d_video_link_save( $post, $attachment ) {
		if( isset( $attachment['sd3d-video-link'] ) )
			update_post_meta( $post['ID'], 'sd3d_video_link', $attachment['sd3d-video-link'] );

		return $post;
	}

	add_filter( 'attachment_fields_to_save', 'sd3d_slide_link_save', 10, 2 );

	function sd3d_slide_link( $form_fields, $post ) {

		$form_fields['sd3d-slide-link'] = array(
			'label' => 'Slide link',
			'input' => 'text',
			'value' => get_post_meta( $post->ID, 'sd3d_slide_link', true ),
			'helps' => '3D slider plugin specific field. URL of the slide.',
		);


		return $form_fields;
	}

	add_filter( 'attachment_fields_to_edit', 'sd3d_slide_link', 10, 2 );

	function sd3d_slide_link_save( $post, $attachment ) {
		if( isset( $attachment['sd3d-slide-link'] ) )
			update_post_meta( $post['ID'], 'sd3d_slide_link', $attachment['sd3d-slide-link'] );

		return $post;
	}

	add_filter( 'attachment_fields_to_save', 'sd3d_slide_link_save', 10, 2 );
