from django.conf.urls import patterns, url
import views

urlpatterns = patterns(
    '',
    url(r'^$', views.home, name='site-home'),
    url(r'^faq/$', views.faq, name='site-faq'),
    url(r'^products/$', views.products, name='site-products'),
    url(r'^projects/$', views.projects, name='site-projects'),
    url(r'^gallery/$', views.gallery, name='site-gallery'),
    url(r'^contacts/$', views.contact, name='site-contact'),
    url(r'^google7925afcbe258929c.html$', views.validation, name='site-validation'),
    url(r'^sitemap.xml$', views.sitemap, name='site-sitemap'),
    url(r'^robots.txt', views.robots, name='site-robots'),
)
