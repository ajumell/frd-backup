from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django_helpers.helpers import email


def home(request):
    return render(request, 'site/home.html')


def faq(request):
    return render(request, 'site/faq.html')


def products(request):
    return render(request, 'site/products.html')


def projects(request):
    return render(request, 'site/projects.html')


def gallery(request):
    return render(request, 'site/gallery.html')


@csrf_exempt
def contact(request):
    if request.method == 'POST' and request.is_ajax():
        name = request.POST.get('senderName')
        email_address = request.POST.get('senderEmail')
        message = request.POST.get('message')
        template_dict = {
            'name': name,
            'email': email_address,
            'message': message,
            "ip_address": request.META['REMOTE_ADDR']
        }
        to_addr = settings.CONTACT_EMAIL
        subject = 'Contact Us From :' + name
        email.render_template(subject, 'email/contact-us.html', to_addr, template_dict=template_dict)

        return HttpResponse('')
    return render(request, 'site/contact.html')


def validation(request):
    return HttpResponse("google-site-verification: google7925afcbe258929c.html")


def robots(request):
    t = """User-agent: *
Allow: /
sitemap: http://www.frdgroup.co.in/sitemap.xml
    """
    return HttpResponse(t.strip())


def sitemap(request):
    t = """
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url><loc>http://frdgroup.co.in/</loc></url>
<url><loc>http://frdgroup.co.in/products/</loc></url>
<url><loc>http://frdgroup.co.in/projects/</loc></url>
<url><loc>http://frdgroup.co.in/gallery/</loc></url>
<url><loc>http://frdgroup.co.in/faq/</loc></url>
<url><loc>http://frdgroup.co.in/contacts/</loc></url>
</urlset>
    """
    return HttpResponse(t.strip())