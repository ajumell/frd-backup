from django.core.urlresolvers import reverse
from django.db.models import Q

from xeo_hideo_theme.menu import MainMenuBase


class MainMenu(MainMenuBase):
    menu_id = 'main-menu'

    def setup(self):
        user = self.get_variable('user')

        self.add_item('Home', 'home')

        self.add_guest_item('Login', 'login')
        self.add_guest_item('Register', 'register')
        self.add_guest_item('Forgot Password', 'forgot-password')

        add_product_items(self)
        add_photo_items(self)

        self.add_user_item('Change Password', 'change-password')
        self.add_user_item('Logout ( %s )' % user, 'logout')


def add_photo_items(menu):
    from views import PhotoCRUDView

    list_url = PhotoCRUDView.get_url_name_for_action('list')
    item = menu.add_user_item("Photos", list_url)
    PhotoCRUDView.get_nav(item, 'list', 'add')


def add_product_items(menu):
    from views import ProductCRUDView

    list_url = ProductCRUDView.get_url_name_for_action('list')
    item = menu.add_user_item("Products", list_url)
    ProductCRUDView.get_nav(item, 'list', 'add')