# coding=utf-8
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Max

from django_helpers.apps.forms.fields import ThumbnailImageField


class OrderedModel(models.Model):
    """
    An abstract model that allows objects to be ordered relative to each other.
    Provides an ``order`` field.
    """

    order = models.PositiveIntegerField(editable=False, db_index=True)
    disabled = models.BooleanField(default=False, editable=False)

    class Meta:
        abstract = True
        ordering = ('order',)

    # noinspection PyProtectedMember
    def get_qs(self):
        qs = self.__class__._default_manager
        return qs

    @classmethod
    def clean_up_ordering(cls):
        # noinspection PyProtectedMember
        qs = cls._default_manager
        items = qs.all()
        i = 0
        for item in items:
            i += 1
            item.order = i
            item.save()

    def save(self, *args, **kwargs):
        if not self.id:
            qs = self.get_qs()
            c = qs.all().aggregate(Max('order')).get('order__max')
            if c is None:
                c = 0
            self.order = c + 1
        super(OrderedModel, self).save(*args, **kwargs)

    def _move(self, up):
        if up:
            replacement = self.get_prev()
        else:
            replacement = self.get_next()

        if replacement is None:
            return

        self.order, replacement.order = replacement.order, self.order
        self.save()
        replacement.save()

    def move(self, direction):
        self._move(direction == 'up')

    def move_down(self):
        """
        Move this object down one position.
        """
        return self._move(up=False)

    def move_up(self):
        """
        Move this object up one position.
        """
        return self._move(up=True)

    def is_first(self):
        return self.get_prev() is None

    def is_last(self):
        return self.get_next() is None

    def get_next(self):
        item = getattr(self, '__next_item', None)
        if item is not None:
            return item
        if item == -1:
            return None

        query = self.get_qs().filter(order__gt=self.order).order_by('order')
        try:
            item = query[0]
            setattr(self, '__next_item', item)
            return item
        except IndexError:
            setattr(self, '__next_item', -1)
            return None

    def get_prev(self):
        item = getattr(self, '__prev_item', None)

        if item is not None:
            return item

        if item == -1:
            return None

        query = self.get_qs().filter(order__lt=self.order).order_by('-order')
        try:
            item = query[0]
            setattr(self, '__prev_item', item)
            return item
        except IndexError:
            setattr(self, '__prev_item', -1)
            return None

    def can_move_up(self, raise_error=False):
        is_first = self.is_first()
        if is_first and raise_error:
            raise ValidationError("Cannot move up.")

        return is_first is False

    def can_move_down(self, raise_error=False):
        is_last = self.is_last()
        if is_last and raise_error:
            raise ValidationError("Cannot move down.")

        return is_last is False


class Photo(OrderedModel):
    name = models.CharField(max_length=255)
    image = ThumbnailImageField()

    class Meta:
        db_table = 'photo'

    def __unicode__(self):
        return self.name


class Product(OrderedModel):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = ThumbnailImageField()

    class Meta:
        db_table = 'product'

    def __unicode__(self):
        return self.name