from tables import *
from forms import *
from xeo_theme_base.crud import CRUDView
from xeo_theme_base.views import render, FORM_HALF_TEMPLATE, DATA_TABLE_TEMPLATE


def home(request):
    return render(request, 'home/home.html')


class PhotoCRUDView(CRUDView):
    model = Photo
    check_permissions = True

    url_prefix = "photo"

    # List
    list_permission = 'change'
    list_data_table = PhotoDataTable
    list_title = "List Photos"

    # Insert
    add_permission = 'add'
    add_form = AddPhotoForm
    add_title = "Add Photo"
    add_success_message = '"{0}" added successfully.'
    add_success_redirect = 'list'

    # Edit
    edit_permission = 'change'
    edit_form = EditPhotoForm
    edit_title = 'Edit "{0}"'
    edit_success_message = '"{0}" updated successfully.'
    edit_success_redirect = 'list'

    # Delete
    delete_permission = 'delete'
    delete_title = 'Confirm Delete "{0}"'
    delete_success_redirect = 'list'
    delete_cancel_link = 'list'

class ProductCRUDView(CRUDView):
    model = Product
    check_permissions = True

    url_prefix = "product"

    # List
    list_permission = 'change'
    list_data_table = ProductDataTable
    list_title = "List Products"

    # Insert
    add_permission = 'add'
    add_form = AddProductForm
    add_title = "Add Product"
    add_success_message = '"{0}" added successfully.'
    add_success_redirect = 'list'

    # Edit
    edit_permission = 'change'
    edit_form = EditProductForm
    edit_title = 'Edit "{0}"'
    edit_success_message = '"{0}" updated successfully.'
    edit_success_redirect = 'list'

    # Delete
    delete_permission = 'delete'
    delete_title = 'Confirm Delete "{0}"'
    delete_success_redirect = 'list'
    delete_cancel_link = 'list'