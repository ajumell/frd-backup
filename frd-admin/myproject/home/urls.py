from django.conf.urls import patterns, url

import views


urlpatterns = patterns(
    '',
    url(r'^$', views.home, name='home'),

)

urlpatterns += views.PhotoCRUDView.urls()

urlpatterns += views.ProductCRUDView.urls()