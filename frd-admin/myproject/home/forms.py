# coding=utf-8
from django import forms

from models import *


class AddPhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = (
            'name',
            'image',
        )


class EditPhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = (
            'name',
        )


class AddProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = (
            'name',
            'description',
            'image',
        )


class EditProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = (
            'name',
            'description',
        )