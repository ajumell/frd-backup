from django.db.models.signals import post_migrate

__author__ = 'ajumell'

users = [
    {
        'username': 'ajumell',
        'first_name': 'Muhammed',
        'last_name': 'K K',
        'email': 'ajumell@xeoscript.com',
        'password': 'q',
        'admin': True
    },
    {
        'username': 'admin',
        'first_name': 'Muhammed',
        'last_name': 'K K',
        'email': 'ajumell@xeoscript.com',
        'password': 'q',
        'admin': True
    }
]


def load_users():
    from django.contrib.auth import get_user_model

    User = get_user_model()

    print 'Loading user data'
    for user in users:
        username = user.get('username')
        password = user.get('password')
        u, created = User.objects.get_or_create(username=username)
        if created is True:
            u.set_password(password)
            u.first_name = user.get('first_name')
            u.last_name = user.get('last_name')
            u.email = user.get('email')
            u.is_staff = True
            u.is_superuser = user.get('admin', False)
            u.is_active = True
            u.save()


# noinspection PyUnusedLocal
def load_data(sender, **kwargs):
    if sender.label == "auth":
        load_users()


post_migrate.connect(load_data, dispatch_uid="home.fixtures")