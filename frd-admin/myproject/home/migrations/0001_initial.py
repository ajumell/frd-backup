# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_helpers.apps.forms.fields.thumbnail


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(editable=False, db_index=True)),
                ('disabled', models.BooleanField(default=False, editable=False)),
                ('name', models.CharField(max_length=255)),
                ('image', django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField(upload_to=b'')),
            ],
            options={
                'db_table': 'photo',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(editable=False, db_index=True)),
                ('disabled', models.BooleanField(default=False, editable=False)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('image', django_helpers.apps.forms.fields.thumbnail.ThumbnailImageField(upload_to=b'')),
            ],
            options={
                'db_table': 'product',
            },
            bases=(models.Model,),
        ),
    ]
