# coding=utf-8
from django_helpers.apps.data_tables.columns import ImageField
from django_helpers.apps.data_tables.columns import DataTableColumn, BootstrapButtonSetColumn, LineBreaksColumn
from xeo_theme_base.data_tables import DataTableBase

from models import *


class PhotoDataTable(DataTableBase):
    table_id = "photo-data-table"

    # noinspection PyMethodMayBeStatic
    def get_columns(self, user):
        from views import PhotoCRUDView as View

        columns = []

        name_column = DataTableColumn("name", "Name", True, True)
        columns.append(name_column)
        name_column.make_editable()

        image_column = ImageField("image", "Image")
        columns.append(image_column)

        actions = BootstrapButtonSetColumn()
        actions.add_button(View.get_edit_button(user))
        actions.add_button(View.get_delete_button(user))

        if actions.has_buttons():
            columns.append(actions)
        return columns

    def get_query(self, request, kwargs=None):
        return Photo.objects.all()


class ProductDataTable(DataTableBase):
    table_id = "product-data-table"

    # noinspection PyMethodMayBeStatic,PyArgumentEqualDefault
    def get_columns(self, user):
        from views import ProductCRUDView as View

        columns = []

        name_column = DataTableColumn("name", "Name", True, True)
        columns.append(name_column)
        name_column.make_editable()

        description_column = LineBreaksColumn("description", "Description", False, False)
        columns.append(description_column)
        description_column.make_editable()

        image_column = ImageField("image", "Image")
        columns.append(image_column)

        actions = BootstrapButtonSetColumn()
        actions.add_button(View.get_edit_button(user))
        actions.add_button(View.get_delete_button(user))

        if actions.has_buttons():
            columns.append(actions)
        return columns

    def get_query(self, request, kwargs=None):
        return Product.objects.all()