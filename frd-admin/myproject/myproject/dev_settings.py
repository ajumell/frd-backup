# noinspection PyUnresolvedReferences
from base_settings import *

INSTALLED_APPS += (
    'django_generators',
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'static', 'media')


MEDIA_URL = STATIC_URL + 'media/'
