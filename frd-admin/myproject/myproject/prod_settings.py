# coding=utf-8
# noinspection PyUnresolvedReferences
from base_settings import *

ADMINS = (
    ('Muhammed K K', 'ajumell@xeoscript.com'),
)

MANAGERS = ADMINS

DEBUG = True

TEMPLATE_DEBUG = False

HOME_FOLDER = os.path.join('/', 'home', 'ajumell')

ALLOWED_HOSTS = ['admin.frdgroup.co.in', 'frdadmin.xeoscript.com']

STATIC_ROOT = os.path.join(HOME_FOLDER, 'webapps', 'frd_app_static', 'admin')

STATIC_URL = '/static/admin/'

MEDIA_ROOT = os.path.join(STATIC_ROOT, '..', 'media')

MEDIA_URL = '/static/media/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(HOME_FOLDER, 'db', 'frd-group.sqlite3'),
    }
}