from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',

    url(r'^auth/', include('auth_app.urls')),
    url(r'^admin/', include('staff_app.urls')),
    url(r'^django-admin/', include(admin.site.urls)),

    url(r'^api/', include("django_helpers.urls")),

    url(r'^', include('home.urls')),
)
